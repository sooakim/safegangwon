package kr.edcan.lumihana.safegangwon.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import kr.edcan.lumihana.safegangwon.Adapter.PostAdapter;
import kr.edcan.lumihana.safegangwon.Model.PostModel;
import kr.edcan.lumihana.safegangwon.R;

public class PostListActivity extends AppCompatActivity {
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private ArrayList<PostModel> arrayList;
    private LinearLayoutManager linearLayoutManager;
    private PostAdapter postAdapter;
    private RecyclerView recycler_post;
    private SwipeRefreshLayout refresh_refresh;
    private LinearLayout root;
    private FloatingActionButton floating_post;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_list);

        initFirebase();
        initToolbar();
        initRefresh();
        initRecycler();
        loadNewPosts();
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.postlist_toolbar);
        toolbar.setTitle("게시판");
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_36px);
        root = (LinearLayout) findViewById(R.id.postlist_root);
        floating_post = (FloatingActionButton) findViewById(R.id.postlist_floating_post);
        floating_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(getApplicationContext(), PostActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void initRefresh() {
        refresh_refresh = (SwipeRefreshLayout) findViewById(R.id.postlist_refresh_refresh);
        refresh_refresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadNewPosts();
                refresh_refresh.setRefreshing(false);
            }
        });
    }

    private void initRecycler() {
        arrayList = new ArrayList<>();
        recycler_post = (RecyclerView) findViewById(R.id.postlist_recycler_posts);
        recycler_post.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        postAdapter = new PostAdapter(PostListActivity.this, arrayList);
        recycler_post.setLayoutManager(linearLayoutManager);
        recycler_post.setAdapter(postAdapter);
    }

    private void loadNewPosts() {
        arrayList.clear();
        databaseReference.child("posts").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() == 0)
                    Snackbar.make(root, "게시글이 없습니다.", Snackbar.LENGTH_SHORT)
                            .setAction("작성", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    final Intent intent = new Intent(getApplicationContext(), PostActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                }
                            }).show();
                else {
                    for (DataSnapshot shot : dataSnapshot.getChildren()) {
                        if (shot.getValue(PostModel.class) == null) continue;
                        else {
                            Log.e("jd", shot.getValue(PostModel.class).getEmail());
                            arrayList.add(shot.getValue(PostModel.class));
                        }
                    }
                    updateRecycler();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("PostLost", databaseError.getMessage() + "");
            }
        });
    }

    private void updateRecycler() {
        postAdapter.notifyDataSetChanged();
    }

    private void initFirebase() {
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
    }
}

