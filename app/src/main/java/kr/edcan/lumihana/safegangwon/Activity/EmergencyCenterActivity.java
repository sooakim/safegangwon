package kr.edcan.lumihana.safegangwon.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;
import kr.edcan.lumihana.safegangwon.Adapter.DataAdapter;
import kr.edcan.lumihana.safegangwon.Model.DataModel;
import kr.edcan.lumihana.safegangwon.R;
import kr.edcan.lumihana.safegangwon.RealmModel.EmergencyCenterModel;
import kr.edcan.lumihana.safegangwon.Util.NetworkManager;

public class EmergencyCenterActivity extends AppCompatActivity implements OnMapReadyCallback {
    private static final String POINT_NAME = "응급 의료 센터";
    private GoogleMap emergencycenter_map;
    private Toolbar toolbar;
    private ArrayList<DataModel> arrayList;
    private RecyclerView recycler_data;
    private RelativeLayout relative_map;
    private LinearLayoutManager linearLayoutManager;
    private DataAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_center);

        relative_map = (RelativeLayout) findViewById(R.id.emergencycenter_relative_map);
        initToolbar();
        initMap();
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.emergencycenter_toolbar);
        toolbar.setTitle("응급 의료 센터");
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_36px);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.emergencycenter_map);
        mapFragment.getMapAsync(this);
    }

    private void updateRecycler() {
        adapter.notifyDataSetChanged();
    }

    private void initRecycler() {
        arrayList = new ArrayList<>();

        linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        adapter = new DataAdapter(getApplicationContext(), arrayList);

        recycler_data = (RecyclerView) findViewById(R.id.emergencycenter_recycler_data);
        recycler_data.setLayoutManager(linearLayoutManager);
        recycler_data.setAdapter(adapter);
    }

    private void fillRecycler() {
        final Intent intent = getIntent();
        final int _id = intent.getIntExtra("_id", -1);

        if (_id >= 0) {
            Realm realm = Realm.getDefaultInstance();
            RealmResults<EmergencyCenterModel> results = realm.where(EmergencyCenterModel.class).equalTo("_id", _id).findAll();

            EmergencyCenterModel model;
            if(results.size()<=0) model = null;
            else model = results.get(0);

            if (model != null) {
                final int no = model.getNO();
                final String sigun_cd = getModelValue(model.getSIGUN_CD());
                final String sigun_nm = getModelValue(model.getSIGUN_NM());
                final String bizplc_nm = getModelValue(model.getBIZPLC_NM());
                final String locplc_lonto_addr = getModelValue(model.getLOCPLC_LOTNO_ADDR());
                final String locplc_roadnm_addr = getModelValue(model.getLOCPLC_ROADNM_ADDR());
                final String licensg_de = getModelValue(model.getLICENSG_DE());
                final String bsn_state_nm = getModelValue(model.getBSN_STATE_NM());
                final String clsbiz_de = getModelValue(model.getCLSBIZ_DE());
                final String suspnbiz_begin_de = getModelValue(model.getSUSPNBIZ_BEGIN_DE());
                final String suspnbiz_end_de = getModelValue(model.getSUSPNBIZ_END_DE());
                final String reopenbiz_de = getModelValue(model.getREOPENBIZ_DE());
                final int locplc_ar = model.getLOCPLC_AR();
                final String locplc_zip_cd = getModelValue(model.getLOCPLC_ZIP_CD());
                final int genrl_amblnc_cnt = model.getGENRL_AMBLNC_CNT();
                final int specl_amblnc_cnt = model.getSPECL_AMBLNC_CNT();
                final int sickbd_cnt = model.getSICKBD_CNT();
                final String easing_medcare_charge_dept_nm = getModelValue(model.getEASING_MEDCARE_CHARGE_DEPT_NM());
                final String easing_medcare_appont_form = getModelValue(model.getEASING_MEDCARE_APPONT_FORM());
                final String medcare_inst_asortmt_nm = getModelValue(model.getMEDCARE_INST_ASORTMT_NM());
                final int medstaf_cnt = model.getMEDSTAF_CNT();
                final int hosptlrm_cnt = model.getHOSPTLRM_CNT();
                final String treat_sbject_cd_info = getModelValue(model.getTREAT_SBJECT_CD_INFO());
                final String treat_sbject_cont = getModelValue(model.getTREAT_SBJECT_CONT());
                final int tot_ar = model.getTOT_AR();
                final int tot_psn_cnt = model.getTOT_PSN_CNT();
                final String first_appont_de = getModelValue(model.getFIRST_APPONT_DE());
                final int permisn_sickbd_cnt = model.getPERMISN_SICKBD_CNT();
                final int wgs84_logt = model.getWGS84_LOGT();
                final int wgs84_lng = model.getWGS84_LOGT();
                final int x_crdnt = model.getX_CRDNT();
                final int y_crdnt = model.getY_CRDNT();
                final String data_colct_de = getModelValue(model.getDATA_COLCT_DE());
                final String etl_ldadng_dtm = getModelValue(model.getETL_LDADNG_DTM());
                final String lat = getModelValue(model.getLAT());
                final String lng = getModelValue(model.getLNG());

                if (no != 0) arrayList.add(new DataModel("번호", no + ""));
                if (!sigun_nm.equals("")) arrayList.add(new DataModel("시 / 군", sigun_nm));
                if (!medcare_inst_asortmt_nm.equals(""))
                    arrayList.add(new DataModel("의료 기관종 별명", medcare_inst_asortmt_nm));
                if (!bizplc_nm.equals("")) arrayList.add(new DataModel("사업장명", bizplc_nm));
                if (locplc_ar != 0) arrayList.add(new DataModel("소재지 면적", locplc_ar + ""));
                if (!locplc_zip_cd.equals("")) arrayList.add(new DataModel("우편 번호", locplc_zip_cd));
                if (!locplc_lonto_addr.equals(""))
                    arrayList.add(new DataModel("지번 주소", locplc_lonto_addr));
                if (!locplc_roadnm_addr.equals(""))
                    arrayList.add(new DataModel("도로명 주소", locplc_roadnm_addr));
                if (!first_appont_de.equals(""))
                    arrayList.add(new DataModel("최초 지정 일자", first_appont_de));
                if (!licensg_de.equals("")) arrayList.add(new DataModel("허가 일자", licensg_de));
                if (!bsn_state_nm.equals("")) arrayList.add(new DataModel("영업 상태명", bsn_state_nm));
                if (!clsbiz_de.equals("")) arrayList.add(new DataModel("폐업 일자", clsbiz_de));
                if (!reopenbiz_de.equals("")) arrayList.add(new DataModel("재개업 일자", reopenbiz_de));
                if (!suspnbiz_begin_de.equals(""))
                    arrayList.add(new DataModel("휴업 시작 일자", suspnbiz_begin_de));
                if (!suspnbiz_end_de.equals(""))
                    arrayList.add(new DataModel("휴업 종료 일자", suspnbiz_end_de));
                if (genrl_amblnc_cnt != 0)
                    arrayList.add(new DataModel("일반 구급차 대수", genrl_amblnc_cnt + ""));
                if (specl_amblnc_cnt != 0)
                    arrayList.add(new DataModel("특수 구급차 대수", specl_amblnc_cnt + ""));
                if (sickbd_cnt != 0) arrayList.add(new DataModel("병상수", sickbd_cnt + ""));
                if (!easing_medcare_charge_dept_nm.equals(""))
                    arrayList.add(new DataModel("완화 의료 담당 부서명", easing_medcare_charge_dept_nm));
                if (!easing_medcare_appont_form.equals(""))
                    arrayList.add(new DataModel("완화 의료 지정 형태", easing_medcare_appont_form));
                if (medstaf_cnt != 0)
                    arrayList.add(new DataModel("의료인 수", medstaf_cnt + ""));
                if (hosptlrm_cnt != 0) arrayList.add(new DataModel("입원실 수", hosptlrm_cnt + ""));
                if (!treat_sbject_cd_info.equals(""))
                    arrayList.add(new DataModel("진료 과목 코드 정보", treat_sbject_cd_info));
                if (!treat_sbject_cont.equals(""))
                    arrayList.add(new DataModel("진료 과목 내용", treat_sbject_cont));
                if (tot_ar != 0) arrayList.add(new DataModel("연면적", tot_ar + ""));
                if (tot_psn_cnt != 0) arrayList.add(new DataModel("총 인원수", tot_psn_cnt + ""));
                if (permisn_sickbd_cnt != 0)
                    arrayList.add(new DataModel("허가 병상수", permisn_sickbd_cnt + ""));
                if (!data_colct_de.equals(""))
                    arrayList.add(new DataModel("데이터 수집 일자", data_colct_de));
                if (!etl_ldadng_dtm.equals(""))
                    arrayList.add(new DataModel("ETL 적재 일시", etl_ldadng_dtm));
                if (!lat.equals("") || !lng.equals(""))
                    pointInMap(Double.parseDouble(lat), Double.parseDouble(lng));
                else {
                    relative_map.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), "지도 정보 없음", Toast.LENGTH_SHORT).show();
                }

                updateRecycler();
            } else {

            }
        } else {

        }
    }

    private String getModelValue(String original) {
        if (original == null) return "";
        else return original.toString().trim();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        emergencycenter_map = googleMap;

        initRecycler();
        fillRecycler();
    }

    private void pointInMap(double lat, double lng) {
        if(!(125.0<=lng&&lng<=132.0) && !(32<=lat&&lat<=39)) {
            relative_map.setVisibility(View.GONE);  //잘못된 좌표
            Toast.makeText(getApplicationContext(), "지도를 표시할 수 없음", Toast.LENGTH_SHORT).show();
        }
        LatLng point = new LatLng(lat, lng);
        emergencycenter_map.addMarker(new MarkerOptions().position(point).title(POINT_NAME));
        emergencycenter_map.moveCamera(CameraUpdateFactory.newLatLngZoom(point, 16));
    }

    private boolean checkNetwork() {
        if (NetworkManager.getConnectivityStatus(getApplicationContext()) == NetworkManager.TYPE_NOT_CONNECTED) {
            RelativeLayout map = (RelativeLayout) findViewById(R.id.emergencycenter_relative_map);

            Snackbar.make(map, "네트워크에 연결해주세요.", Snackbar.LENGTH_LONG)
                    .setAction("연결", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final Intent intent = new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    }).show();
            return false;
        } else return true;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        checkNetwork();
    }

    @Override
    protected void onResume() {
        super.onResume();
        onStart();
    }
}
