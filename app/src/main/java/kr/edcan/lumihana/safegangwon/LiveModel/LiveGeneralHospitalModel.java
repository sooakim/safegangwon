package kr.edcan.lumihana.safegangwon.LiveModel;

/**
 * Created by kimok_000 on 2016-09-28.
 */
public class LiveGeneralHospitalModel extends ParentModel{
    private String bizplc_nm;
    private String locplc_lonto_addr;

    public LiveGeneralHospitalModel(String bizplc_nm, String locplc_lonto_addr) {
        super.typeOfModel = ParentModel.TYPE_GENERAL_HOSPITAL;
        this.bizplc_nm = bizplc_nm;
        this.locplc_lonto_addr = locplc_lonto_addr;
    }

    public String getBizplc_nm() {
        return bizplc_nm;
    }

    public String getLocplc_lonto_addr() {
        return locplc_lonto_addr;
    }

    public void setBizplc_nm(String bizplc_nm) {
        this.bizplc_nm = bizplc_nm;
    }

    public void setLocplc_lonto_addr(String locplc_lonto_addr) {
        this.locplc_lonto_addr = locplc_lonto_addr;
    }
}
