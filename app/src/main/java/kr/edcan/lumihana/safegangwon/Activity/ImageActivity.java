package kr.edcan.lumihana.safegangwon.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import kr.edcan.lumihana.safegangwon.R;

public class ImageActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private ImageView image_image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image);

        image_image = (ImageView) findViewById(R.id.image_image_image);
        final Intent intent = getIntent();
        toolbar = (Toolbar) findViewById(R.id.image_toolbar);
        toolbar.setTitle(intent.getStringExtra("title"));
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_36px);

        Glide.with(this).load(intent.getStringExtra("image")).into(image_image);
        Log.e("hello",intent.getStringExtra("image"));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home :{
                finish();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
