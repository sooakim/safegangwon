package kr.edcan.lumihana.safegangwon.Util;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import io.realm.Realm;
import kr.edcan.lumihana.safegangwon.Model.CivilDefenseModel;
import kr.edcan.lumihana.safegangwon.Model.EarthModel;
import kr.edcan.lumihana.safegangwon.Model.EmergencyCenterModel;
import kr.edcan.lumihana.safegangwon.Model.EmergencyNoticeModel;
import kr.edcan.lumihana.safegangwon.Model.GeneralHospitalModel;
import kr.edcan.lumihana.safegangwon.Model.HospitalModel;
import kr.edcan.lumihana.safegangwon.Model.SnowModel;
import kr.edcan.lumihana.safegangwon.Model.WaterSupplyModel;
import kr.edcan.lumihana.safegangwon.Model.WeatherModel;

/**
 * Created by kimok_000 on 2016-09-23.
 */
public class DBManager implements
        GangwonManager.OnGangwonChangedListener, DataManager.OnEmergencyNoticeChangedListener, WeatherManager.OnWeatherChangedListener {
    private static final String CLASS_NAME = "DBManager";
    private static final String API_DATA = "HWAAD423720160922064840LYSKB";
    private static final String API_WEATHER = "daa93524-10c6-34e8-b7ef-c0c702e65362";
    private static final String API_GANGWON = "4e666c76506b696d38326543494a70";
    private static final String COUNT_CIVIL_DEFENSE = "CivilDefenseModelCount";
    private static final String COUNT_SNOW = "SnowModelCount";
    private static final String COUNT_EARTH = "EarthModelCount";
    private static final String COUNT_WATER_SUPPLY = "WaterSupplyCount";
    private static final String COUNT_GENERAL_HOSPITAL = "GeneralHospitalCount";
    private static final String COUNT_EMERGENCY_CENTER = "EmergencyCenterCount";
    private static final String COUNT_HOSPITAL = "HospitalCount";
    private static final String COUNT_EMERGENCY_NOTICE = "EmergencyNoticeCount";
    private static final String COUNT_WEATHER = "WeatherCount";
    private Context context;
    private Realm realm;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private GangwonManager gangwonManager;
    private DataManager dataManager;
    private WeatherManager weatherManager;

    public DBManager(Context context) {
        this.context = context;


        initRealm();
        initSharedPreferences();

        initManager();
    }

    private void initRealm() {
        realm = Realm.getDefaultInstance();
    }

    private void initSharedPreferences() {
        sharedPreferences = context.getSharedPreferences(CLASS_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    private void initManager() {
        gangwonManager = new GangwonManager(context, API_GANGWON);
        gangwonManager.setOnGangwonChangedListener(this);
        dataManager = new DataManager(context, API_DATA);
        dataManager.setOnEmergencyNoticeChangedListener(this);
        weatherManager = new WeatherManager(context, WeatherManager.VERSION, API_WEATHER);
        weatherManager.setOnWeatherChangedListener(this);
    }

    public void getSnow(int startIndex, int endIndex) {
        gangwonManager.getSnow(GangwonManager.SERVICE_SNOW_GANGWON, startIndex, endIndex);
    }

    public void getEarth(String service, int startIndex, int endIndex) {
        gangwonManager.getEarth(service, startIndex, endIndex);
    }

    public void getCivilDefense(int startIndex, int endIndex) {
        gangwonManager.getCivilDefense(GangwonManager.SERVICE_CIVIL_DEFENSE, startIndex, endIndex);
    }

    public void getWaterSupply(int startIndex, int endIndex) {
        gangwonManager.getWaterSupply(GangwonManager.SERVICE_WATER_SUPPLY, startIndex, endIndex);
    }

    public void getEmergencyCenter(int startIndex, int endIndex) {
        gangwonManager.getEmergencyCenter(GangwonManager.SERVICE_EMERGENCY_CENTER, startIndex, endIndex);
    }

    public void getHospital(int startIndex, int endIndex) {
        gangwonManager.getHospital(GangwonManager.SERVICE_HOSPITAL, startIndex, endIndex);
    }

    public void getWeatherWithAddress(String city, String county, String village) {
        weatherManager.getWeather(city, county, village);
    }

    public void getWeatherWithLocation(String lat, String lon) {
        weatherManager.getWeather(lat, lon);
    }

    public void getWeatherWithId(int stnId) {
        weatherManager.getWeather(stnId);
    }

    public void getEmergencyNotice(int startIndex, int endIndex) {
        dataManager.getEmergencyNotice(startIndex, endIndex);
    }

    public void getGeneralHospital(int startIndex, int endIndex){
        gangwonManager.getGeneralHospital(GangwonManager.SERVICE_GENERAL_HOSPITAL, startIndex, endIndex);
    }

    public boolean isAlive() {
        if (realm == null || !realm.isClosed()) return true;
        else return false;
    }

    public boolean isEmpty() {
        if (realm.isEmpty()) return true;
        else return false;
    }

    public boolean isBusy() {
        if (realm.isInTransaction()) return true;
        else return false;
    }

    public void pauseDB() {
        if (isAlive()) {
            realm.close();
        }
    }

    public void resumeDB() {
        if (isAlive()) {
            initRealm();
        }
    }

    public void closeDB() {
        if (realm != null) {
            realm.close();
            realm = null;
        }
    }

    @Override
    public void onSnowChanged(SnowModel snowModel) {
        initRealm();
        final int pastCount = sharedPreferences.getInt(COUNT_SNOW, 0);
        final int currentCount = snowModel.getData().getList_total_count();

        if (pastCount != 0 && pastCount < currentCount) {                       //이전에 저장된 데이터가 존재하고, 새로운 데이터의 양이 더 많을 때
            Log.e(CLASS_NAME, COUNT_SNOW + " : data changed");
            int cnt = 0;
            for (final SnowModel.DataBean.RowBean row : snowModel.getData().getRow()) {
                final int _id = cnt++;

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        kr.edcan.lumihana.safegangwon.RealmModel.SnowModel newModel = new kr.edcan.lumihana.safegangwon.RealmModel.SnowModel(
                                _id, row.getSIDO(), row.getSIGUNGU(), row.getCANAME(), row.getPROD(), row.getMODELN(), row.getCTYPE(), row.getADDR(), row.getSTREAMADDR());
                        realm.insertOrUpdate(newModel);
                    }
                });
            }

            editor.putInt(COUNT_SNOW, currentCount);
            editor.commit();
//            for (int i = 0; i < pastCount; i++) {                             //이전에 저장된 부분까지는 데이터를 업데이트
//                final SnowModel.DataBean.RowBean row = snowModel.getData().getRow().get(i);
//                final int _id = i;
//
//                realm.executeTransaction(new Realm.Transaction() {
//                    @Override
//                    public void execute(Realm realm) {
//                        realm.insertOrUpdate();
//                    }
//                });
//            }
//
//            for (int i = pastCount; i < currentCount; i++) {
//                final SnowModel.DataBean.RowBean row = snowModel.getData().getRow().get(i);
//                final int _id = i;
//
//                realm.executeTransaction(new Realm.Transaction() {
//                    @Override
//                    public void execute(Realm realm) {
//                        kr.edcan.lumihana.safegangwon.RealmModel.SnowModel newModel = realm.createObject(kr.edcan.lumihana.safegangwon.RealmModel.SnowModel.class);
//                        newModel.set_id(_id);
//                        newModel.setADDR(row.getADDR());
//                        newModel.setCANAME(row.getCANAME());
//                        newModel.setCTYPE(row.getCTYPE());
//                        newModel.setMODELN(row.getMODELN());
//                        newModel.setPROD(row.getPROD());
//                        newModel.setSIDO(row.getSIDO());
//                        newModel.setSIGUNGU(row.getSIGUNGU());
//                        newModel.setSTREAMADDR(row.getSTREAMADDR());
//                    }
//                });
//            }
            Log.e(CLASS_NAME, COUNT_SNOW + " : changed data saved");
        } else if (pastCount == 0 && currentCount != 0) {                        //이전에 저장된 데이터가 없고, 새로운 데이터가 존재할 때
            Log.e(CLASS_NAME, COUNT_SNOW + " : first data reveived");
            int cnt = 0;
            for (final SnowModel.DataBean.RowBean row : snowModel.getData().getRow()) {
                final int _id = cnt++;

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        kr.edcan.lumihana.safegangwon.RealmModel.SnowModel newModel = new kr.edcan.lumihana.safegangwon.RealmModel.SnowModel(
                                _id, row.getSIDO(), row.getSIGUNGU(), row.getCANAME(), row.getPROD(), row.getMODELN(), row.getCTYPE(), row.getADDR(), row.getSTREAMADDR());
                        realm.insertOrUpdate(newModel);
                    }
                });
            }

            editor.putInt(COUNT_SNOW, currentCount);
            editor.commit();
            Log.e(CLASS_NAME, COUNT_SNOW + " : first data saved");
        } else if (currentCount == 0) {                                   //새로운 데이터가 존재하지 않을 때
            Log.e(CLASS_NAME, COUNT_SNOW + " : No data received.");
        } else if (pastCount == currentCount) {                            //이전에 저장된 데이터의 양과 새로운 데이터의 양이 같을 때
            Log.e(CLASS_NAME, COUNT_SNOW + " : No data changed");
        }
        pauseDB();
    }

    @Override
    public void onEarthChanged(EarthModel earthModel) {
        initRealm();
        final int pastCount = sharedPreferences.getInt(COUNT_EARTH, 0);
        final int currentCount = earthModel.getData().getList_total_count();

        if (pastCount != 0 && pastCount < currentCount) {                       //이전에 저장된 데이터가 존재하고, 새로운 데이터의 양이 더 많을 때
            Log.e(CLASS_NAME, COUNT_EARTH + " : data changed");
            int cnt = 0;
            for (final EarthModel.DataBean.RowBean row : earthModel.getData().getRow()) {
                final int _id = cnt++;

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        kr.edcan.lumihana.safegangwon.RealmModel.EarthModel newModel = new kr.edcan.lumihana.safegangwon.RealmModel.EarthModel(
                                _id, row.getSIDO(), row.getGUGUN_NM(), row.getOBS_NM(), row.getLNG(), row.getLAT(), row.getOBS_ORG_NM(), row.getOBS_ORG(),
                                row.getOBS_SDATE(), row.getOBS_EDATE(), row.getOBS_TYPE(), row.getOBS_STATUS(), row.getLOCPLC_LOTNO_ADDR(), row.getOBS_ETC(),
                                row.getOBS_EQUP(), row.getOBS_CONTENTS(), row.getOBS_TIME(), row.getDATA_BASE_DT()
                        );
                        realm.insertOrUpdate(newModel);
                    }
                });
            }

            editor.putInt(COUNT_EARTH, currentCount);
            editor.commit();
            Log.e(CLASS_NAME, COUNT_EARTH + " : changed data saved");
        } else if (pastCount == 0 && currentCount != 0) {                        //이전에 저장된 데이터가 없고, 새로운 데이터가 존재할 때
            Log.e(CLASS_NAME, COUNT_EARTH + " : first data reveived");
            int cnt = 0;
            for (final EarthModel.DataBean.RowBean row : earthModel.getData().getRow()) {
                final int _id = cnt++;

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        kr.edcan.lumihana.safegangwon.RealmModel.EarthModel newModel = new kr.edcan.lumihana.safegangwon.RealmModel.EarthModel(
                                _id, row.getSIDO(), row.getGUGUN_NM(), row.getOBS_NM(), row.getLNG(), row.getLAT(), row.getOBS_ORG_NM(), row.getOBS_ORG(),
                                row.getOBS_SDATE(), row.getOBS_EDATE(), row.getOBS_TYPE(), row.getOBS_STATUS(), row.getLOCPLC_LOTNO_ADDR(), row.getOBS_ETC(),
                                row.getOBS_EQUP(), row.getOBS_CONTENTS(), row.getOBS_TIME(), row.getDATA_BASE_DT()
                        );
                        realm.insertOrUpdate(newModel);
                    }
                });
            }

            editor.putInt(COUNT_EARTH, currentCount);
            editor.commit();
            Log.e(CLASS_NAME, COUNT_EARTH + " : first data saved");
        } else if (currentCount == 0) {                                   //새로운 데이터가 존재하지 않을 때
            Log.e(CLASS_NAME, COUNT_EARTH + " : No data received.");
        } else if (pastCount == currentCount) {                            //이전에 저장된 데이터의 양과 새로운 데이터의 양이 같을 때
            Log.e(CLASS_NAME, COUNT_EARTH + " : No data changed");
        }
        pauseDB();
    }

    @Override
    public void onCivilDefenseChanged(CivilDefenseModel civilDefenseModel) {
        initRealm();
        final int pastCount = sharedPreferences.getInt(COUNT_CIVIL_DEFENSE, 0);
        final int currentCount = civilDefenseModel.getData().getList_total_count();

        if (pastCount != 0 && pastCount < currentCount) {                       //이전에 저장된 데이터가 존재하고, 새로운 데이터의 양이 더 많을 때
            Log.e(CLASS_NAME, COUNT_CIVIL_DEFENSE + " : data changed");
            int cnt = 0;
            for (final CivilDefenseModel.DataBean.RowBean row : civilDefenseModel.getData().getRow()) {
                final int _id = cnt++;

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        kr.edcan.lumihana.safegangwon.RealmModel.CivilDefenseModel newModel = new kr.edcan.lumihana.safegangwon.RealmModel.CivilDefenseModel(
                                _id, row.getNO(), row.getSIGUN_CD(), row.getSIGUN_NM(), row.getBIZPLC_NM(), row.getLOCPLC_LOTNO_ADDR(), row.getLOCPLC_ROADNM_ADDR(),
                                row.getLICENSG_DE(), row.getBSN_STATE_NM(), row.getCLSBIZ_DE(), row.getSUSPNBIZ_BEGIN_DE(), row.getSUSPNBIZ_END_DE(), row.getREOPENBIZ_DE(),
                                row.getLOCPLC_AR(), row.getLOCPLC_ZIP_CD(), row.getFACLT_BULDNG_NM(), row.getRELEASE_DE(), row.getEMGNCY_FACLT_DIV_NM(), row.getWGS84_LOGT(),
                                row.getWGS84_LAT(), row.getX_CRDNT(), row.getY_CRDNT(), row.getDATA_COLCT_DE(), row.getETL_LDADNG_DTM(), row.getLAT(), row.getLNG()
                        );
                        realm.insertOrUpdate(newModel);
                    }
                });
            }

            editor.putInt(COUNT_CIVIL_DEFENSE, currentCount);
            editor.commit();
            Log.e(CLASS_NAME, COUNT_CIVIL_DEFENSE + " : changed data saved");
        } else if (pastCount == 0 && currentCount != 0) {                        //이전에 저장된 데이터가 없고, 새로운 데이터가 존재할 때
            Log.e(CLASS_NAME, COUNT_CIVIL_DEFENSE + " : first data reveived");
            int cnt = 0;
            for (final CivilDefenseModel.DataBean.RowBean row : civilDefenseModel.getData().getRow()) {
                final int _id = cnt++;

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        kr.edcan.lumihana.safegangwon.RealmModel.CivilDefenseModel newModel = new kr.edcan.lumihana.safegangwon.RealmModel.CivilDefenseModel(
                                _id, row.getNO(), row.getSIGUN_CD(), row.getSIGUN_NM(), row.getBIZPLC_NM(), row.getLOCPLC_LOTNO_ADDR(), row.getLOCPLC_ROADNM_ADDR(),
                                row.getLICENSG_DE(), row.getBSN_STATE_NM(), row.getCLSBIZ_DE(), row.getSUSPNBIZ_BEGIN_DE(), row.getSUSPNBIZ_END_DE(), row.getREOPENBIZ_DE(),
                                row.getLOCPLC_AR(), row.getLOCPLC_ZIP_CD(), row.getFACLT_BULDNG_NM(), row.getRELEASE_DE(), row.getEMGNCY_FACLT_DIV_NM(), row.getWGS84_LOGT(),
                                row.getWGS84_LAT(), row.getX_CRDNT(), row.getY_CRDNT(), row.getDATA_COLCT_DE(), row.getETL_LDADNG_DTM(), row.getLAT(), row.getLNG()
                        );
                        realm.insertOrUpdate(newModel);
                    }
                });
            }

            editor.putInt(COUNT_CIVIL_DEFENSE, currentCount);
            editor.commit();
            Log.e(CLASS_NAME, COUNT_CIVIL_DEFENSE + " : first data saved");
        } else if (currentCount == 0) {                                   //새로운 데이터가 존재하지 않을 때
            Log.e(CLASS_NAME, COUNT_CIVIL_DEFENSE + " : No data received.");
        } else if (pastCount == currentCount) {                            //이전에 저장된 데이터의 양과 새로운 데이터의 양이 같을 때
            Log.e(CLASS_NAME, COUNT_CIVIL_DEFENSE + " : No data changed");
        }
        pauseDB();
    }

    @Override
    public void onWaterSupplyChanged(WaterSupplyModel waterSupplyModel) {
        initRealm();
        final int pastCount = sharedPreferences.getInt(COUNT_WATER_SUPPLY, 0);
        final int currentCount = waterSupplyModel.getData().getList_total_count();

        if (pastCount != 0 && pastCount < currentCount) {                       //이전에 저장된 데이터가 존재하고, 새로운 데이터의 양이 더 많을 때
            Log.e(CLASS_NAME, COUNT_WATER_SUPPLY + " : data changed");
            int cnt = 0;
            for (final WaterSupplyModel.DataBean.RowBean row : waterSupplyModel.getData().getRow()) {
                final int _id = cnt++;

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        kr.edcan.lumihana.safegangwon.RealmModel.WaterSupplyModel newModel = new kr.edcan.lumihana.safegangwon.RealmModel.WaterSupplyModel(
                                _id, row.getNO(), row.getSIGUN_CD(), row.getSIGUN_NM(), row.getBIZPLC_NM(), row.getLOCPLC_LOTNO_ADDR(), row.getLOCPLC_ROADNM_ADDR(),
                                row.getLICENSG_DE(), row.getBSN_STATE_NM(), row.getCLSBIZ_DE(), row.getSUSPNBIZ_BEGIN_DE(), row.getSUSPNBIZ_END_DE(), row.getREOPENBIZ_DE(),
                                row.getLOCPLC_AR(), row.getLOCPLC_ZIP_CD(), row.getFACLT_BULDNG_NM(), row.getRELEASE_DE(), row.getEMGNCY_FACLT_DIV_NM(), row.getWGS84_LOGT(),
                                row.getWGS84_LAT(), row.getX_CRDNT(), row.getY_CRDNT(), row.getDATA_COLCT_DE(), row.getETL_LDADNG_DTM(), row.getLAT(), row.getLNG()
                        );
                        realm.insertOrUpdate(newModel);
                    }
                });
            }

            editor.putInt(COUNT_WATER_SUPPLY, currentCount);
            editor.commit();
            Log.e(CLASS_NAME, COUNT_WATER_SUPPLY + " : changed data saved");
        } else if (pastCount == 0 && currentCount != 0) {                        //이전에 저장된 데이터가 없고, 새로운 데이터가 존재할 때
            Log.e(CLASS_NAME, COUNT_WATER_SUPPLY + " : first data reveived");
            int cnt = 0;
            for (final WaterSupplyModel.DataBean.RowBean row : waterSupplyModel.getData().getRow()) {
                final int _id = cnt++;

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        kr.edcan.lumihana.safegangwon.RealmModel.WaterSupplyModel newModel = new kr.edcan.lumihana.safegangwon.RealmModel.WaterSupplyModel(
                                _id, row.getNO(), row.getSIGUN_CD(), row.getSIGUN_NM(), row.getBIZPLC_NM(), row.getLOCPLC_LOTNO_ADDR(), row.getLOCPLC_ROADNM_ADDR(),
                                row.getLICENSG_DE(), row.getBSN_STATE_NM(), row.getCLSBIZ_DE(), row.getSUSPNBIZ_BEGIN_DE(), row.getSUSPNBIZ_END_DE(), row.getREOPENBIZ_DE(),
                                row.getLOCPLC_AR(), row.getLOCPLC_ZIP_CD(), row.getFACLT_BULDNG_NM(), row.getRELEASE_DE(), row.getEMGNCY_FACLT_DIV_NM(), row.getWGS84_LOGT(),
                                row.getWGS84_LAT(), row.getX_CRDNT(), row.getY_CRDNT(), row.getDATA_COLCT_DE(), row.getETL_LDADNG_DTM(), row.getLAT(), row.getLNG()
                        );
                        realm.insertOrUpdate(newModel);
                    }
                });
            }

            editor.putInt(COUNT_WATER_SUPPLY, currentCount);
            editor.commit();
            Log.e(CLASS_NAME, COUNT_WATER_SUPPLY + " : first data saved");
        } else if (currentCount == 0) {                                   //새로운 데이터가 존재하지 않을 때
            Log.e(CLASS_NAME, COUNT_WATER_SUPPLY + " : No data received.");
        } else if (pastCount == currentCount) {                            //이전에 저장된 데이터의 양과 새로운 데이터의 양이 같을 때
            Log.e(CLASS_NAME, COUNT_WATER_SUPPLY + " : No data changed");
        }
        pauseDB();
    }

    @Override
    public void onGeneralHospitalChanged(GeneralHospitalModel generalHospitalModel) {
        initRealm();
        final int pastCount = sharedPreferences.getInt(COUNT_GENERAL_HOSPITAL, 0);
        final int currentCount = generalHospitalModel.getData().getList_total_count();

        if (pastCount != 0 && pastCount < currentCount) {                       //이전에 저장된 데이터가 존재하고, 새로운 데이터의 양이 더 많을 때
            Log.e(CLASS_NAME, COUNT_GENERAL_HOSPITAL + " : data changed");
            int cnt = 0;
            for (final GeneralHospitalModel.DataBean.RowBean row : generalHospitalModel.getData().getRow()) {
                final int _id = cnt++;

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        kr.edcan.lumihana.safegangwon.RealmModel.GeneralHospitalModel newModel = new kr.edcan.lumihana.safegangwon.RealmModel.GeneralHospitalModel(
                                _id, row.getNO(), row.getSIGUN_CD(), row.getSIGUN_NM(), row.getBIZPLC_NM(), row.getLOCPLC_LOTNO_ADDR(), row.getLOCPLC_ROADNM_ADDR(),
                                row.getLICENSG_DE(), row.getBSN_STATE_NM(), row.getCLSBIZ_DE(), row.getSUSPNBIZ_BEGIN_DE(), row.getSUSPNBIZ_END_DE(), row.getREOPENBIZ_DE(),
                                row.getLOCPLC_AR(), row.getLOCPLC_ZIP_CD(), row.getGENRL_AMBLNC_CNT(), row.getSPECL_AMBLNC_CNT(), row.getRESCUPSN_CNT(), row.getSICKBD_CNT(),
                                row.getEASING_MEDCARE_CHARGE_DEPT_NM(), row.getEASING_MEDCARE_APPONT_FORM(), row.getMEDCARE_INST_ASORTMT_NM(), row.getMEDSTAF_CNT(), row.getHOSPTLRM_CNT(),
                                row.getTREAT_SBJECT_CD_INFO(), row.getTREAT_SBJECT_CONT(), row.getTOT_AR(), row.getTOT_PSN_CNT(), row.getFIRST_APPONT_DE(), row.getPERMISN_SICKBD_CNT(),
                                row.getWGS84_LOGT(), row.getWGS84_LAT(), row.getX_CRDNT(), row.getY_CRDNT(), row.getDATA_COLCT_DE(), row.getETL_LDADNG_DTM(), row.getLAT(), row.getLNG()
                        );
                        realm.insertOrUpdate(newModel);
                    }
                });
            }

            editor.putInt(COUNT_GENERAL_HOSPITAL, currentCount);
            editor.commit();
            Log.e(CLASS_NAME, COUNT_GENERAL_HOSPITAL + " : changed data saved");
        } else if (pastCount == 0 && currentCount != 0) {                        //이전에 저장된 데이터가 없고, 새로운 데이터가 존재할 때
            Log.e(CLASS_NAME, COUNT_GENERAL_HOSPITAL + " : first data reveived");
            int cnt = 0;
            for (final GeneralHospitalModel.DataBean.RowBean row : generalHospitalModel.getData().getRow()) {
                final int _id = cnt++;

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        kr.edcan.lumihana.safegangwon.RealmModel.GeneralHospitalModel newModel = new kr.edcan.lumihana.safegangwon.RealmModel.GeneralHospitalModel(
                                _id, row.getNO(), row.getSIGUN_CD(), row.getSIGUN_NM(), row.getBIZPLC_NM(), row.getLOCPLC_LOTNO_ADDR(), row.getLOCPLC_ROADNM_ADDR(),
                                row.getLICENSG_DE(), row.getBSN_STATE_NM(), row.getCLSBIZ_DE(), row.getSUSPNBIZ_BEGIN_DE(), row.getSUSPNBIZ_END_DE(), row.getREOPENBIZ_DE(),
                                row.getLOCPLC_AR(), row.getLOCPLC_ZIP_CD(), row.getGENRL_AMBLNC_CNT(), row.getSPECL_AMBLNC_CNT(), row.getRESCUPSN_CNT(), row.getSICKBD_CNT(),
                                row.getEASING_MEDCARE_CHARGE_DEPT_NM(), row.getEASING_MEDCARE_APPONT_FORM(), row.getMEDCARE_INST_ASORTMT_NM(), row.getMEDSTAF_CNT(), row.getHOSPTLRM_CNT(),
                                row.getTREAT_SBJECT_CD_INFO(), row.getTREAT_SBJECT_CONT(), row.getTOT_AR(), row.getTOT_PSN_CNT(), row.getFIRST_APPONT_DE(), row.getPERMISN_SICKBD_CNT(),
                                row.getWGS84_LOGT(), row.getWGS84_LAT(), row.getX_CRDNT(), row.getY_CRDNT(), row.getDATA_COLCT_DE(), row.getETL_LDADNG_DTM(), row.getLAT(), row.getLNG()
                        );
                        realm.insertOrUpdate(newModel);
                    }
                });
            }

            editor.putInt(COUNT_GENERAL_HOSPITAL, currentCount);
            editor.commit();
            Log.e(CLASS_NAME, COUNT_GENERAL_HOSPITAL + " : first data saved");
        } else if (currentCount == 0) {                                   //새로운 데이터가 존재하지 않을 때
            Log.e(CLASS_NAME, COUNT_GENERAL_HOSPITAL + " : No data received.");
        } else if (pastCount == currentCount) {                            //이전에 저장된 데이터의 양과 새로운 데이터의 양이 같을 때
            Log.e(CLASS_NAME, COUNT_GENERAL_HOSPITAL + " : No data changed");
        }
        pauseDB();
    }

    @Override
    public void onEmergencyCenterChanged(EmergencyCenterModel emergencyCenterModel) {
        initRealm();
        final int pastCount = sharedPreferences.getInt(COUNT_EMERGENCY_CENTER, 0);
        final int currentCount = emergencyCenterModel.getData().getList_total_count();

        if (pastCount != 0 && pastCount < currentCount) {                       //이전에 저장된 데이터가 존재하고, 새로운 데이터의 양이 더 많을 때
            Log.e(CLASS_NAME, COUNT_EMERGENCY_CENTER + " : data changed");
            int cnt = 0;
            for (final EmergencyCenterModel.DataBean.RowBean row : emergencyCenterModel.getData().getRow()) {
                final int _id = cnt++;

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        kr.edcan.lumihana.safegangwon.RealmModel.EmergencyCenterModel newModel = new kr.edcan.lumihana.safegangwon.RealmModel.EmergencyCenterModel(
                                _id, row.getNO(), row.getSIGUN_CD(), row.getSIGUN_NM(), row.getBIZPLC_NM(), row.getLOCPLC_LOTNO_ADDR(), row.getLOCPLC_ROADNM_ADDR(),
                                row.getLICENSG_DE(), row.getBSN_STATE_NM(), row.getCLSBIZ_DE(), row.getSUSPNBIZ_BEGIN_DE(), row.getSUSPNBIZ_END_DE(), row.getREOPENBIZ_DE(),
                                row.getLOCPLC_AR(), row.getLOCPLC_ZIP_CD(), row.getGENRL_AMBLNC_CNT(), row.getSPECL_AMBLNC_CNT(), row.getSICKBD_CNT(), row.getEASING_MEDCARE_CHARGE_DEPT_NM(),
                                row.getEASING_MEDCARE_APPONT_FORM(), row.getMEDCARE_INST_ASORTMT_NM(), row.getMEDSTAF_CNT(), row.getHOSPTLRM_CNT(), row.getTREAT_SBJECT_CD_INFO(),
                                row.getTREAT_SBJECT_CONT(), row.getTOT_AR(), row.getTOT_PSN_CNT(), row.getFIRST_APPONT_DE(), row.getPERMISN_SICKBD_CNT(), row.getWGS84_LOGT(), row.getWGS84_LAT(),
                                row.getX_CRDNT(), row.getY_CRDNT(), row.getDATA_COLCT_DE(), row.getETL_LDADNG_DTM(), row.getLAT(), row.getLNG()
                        );
                        realm.insertOrUpdate(newModel);
                    }
                });
            }

            editor.putInt(COUNT_EMERGENCY_CENTER, currentCount);
            editor.commit();
            Log.e(CLASS_NAME, COUNT_EMERGENCY_CENTER + " : changed data saved");
        } else if (pastCount == 0 && currentCount != 0) {                        //이전에 저장된 데이터가 없고, 새로운 데이터가 존재할 때
            Log.e(CLASS_NAME, COUNT_EMERGENCY_CENTER + " : first data reveived");
            int cnt = 0;
            for (final EmergencyCenterModel.DataBean.RowBean row : emergencyCenterModel.getData().getRow()) {
                final int _id = cnt++;

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        kr.edcan.lumihana.safegangwon.RealmModel.EmergencyCenterModel newModel = new kr.edcan.lumihana.safegangwon.RealmModel.EmergencyCenterModel(
                                _id, row.getNO(), row.getSIGUN_CD(), row.getSIGUN_NM(), row.getBIZPLC_NM(), row.getLOCPLC_LOTNO_ADDR(), row.getLOCPLC_ROADNM_ADDR(),
                                row.getLICENSG_DE(), row.getBSN_STATE_NM(), row.getCLSBIZ_DE(), row.getSUSPNBIZ_BEGIN_DE(), row.getSUSPNBIZ_END_DE(), row.getREOPENBIZ_DE(),
                                row.getLOCPLC_AR(), row.getLOCPLC_ZIP_CD(), row.getGENRL_AMBLNC_CNT(), row.getSPECL_AMBLNC_CNT(), row.getSICKBD_CNT(), row.getEASING_MEDCARE_CHARGE_DEPT_NM(),
                                row.getEASING_MEDCARE_APPONT_FORM(), row.getMEDCARE_INST_ASORTMT_NM(), row.getMEDSTAF_CNT(), row.getHOSPTLRM_CNT(), row.getTREAT_SBJECT_CD_INFO(),
                                row.getTREAT_SBJECT_CONT(), row.getTOT_AR(), row.getTOT_PSN_CNT(), row.getFIRST_APPONT_DE(), row.getPERMISN_SICKBD_CNT(), row.getWGS84_LOGT(), row.getWGS84_LAT(),
                                row.getX_CRDNT(), row.getY_CRDNT(), row.getDATA_COLCT_DE(), row.getETL_LDADNG_DTM(), row.getLAT(), row.getLNG()
                        );
                        realm.insertOrUpdate(newModel);
                    }
                });
            }

            editor.putInt(COUNT_EMERGENCY_CENTER, currentCount);
            editor.commit();
            Log.e(CLASS_NAME, COUNT_EMERGENCY_CENTER + " : first data saved");
        } else if (currentCount == 0) {                                   //새로운 데이터가 존재하지 않을 때
            Log.e(CLASS_NAME, COUNT_EMERGENCY_CENTER + " : No data received.");
        } else if (pastCount == currentCount) {                            //이전에 저장된 데이터의 양과 새로운 데이터의 양이 같을 때
            Log.e(CLASS_NAME, COUNT_EMERGENCY_CENTER + " : No data changed");
        }
        pauseDB();
    }

    @Override
    public void onHospitalChanged(HospitalModel hospitalModel) {
        initRealm();
        final int pastCount = sharedPreferences.getInt(COUNT_HOSPITAL, 0);
        final int currentCount = hospitalModel.getData().getList_total_count();

        if (pastCount != 0 && pastCount < currentCount) {                       //이전에 저장된 데이터가 존재하고, 새로운 데이터의 양이 더 많을 때
            Log.e(CLASS_NAME, COUNT_HOSPITAL + " : data changed");
            int cnt = 0;
            for (final HospitalModel.DataBean.RowBean row : hospitalModel.getData().getRow()) {
                final int _id = cnt++;

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        kr.edcan.lumihana.safegangwon.RealmModel.HospitalModel newModel = new kr.edcan.lumihana.safegangwon.RealmModel.HospitalModel(
                                _id, row.getNO(), row.getSIGUN_CD(), row.getSIGUN_NM(), row.getBIZPLC_NM(), row.getLOCPLC_LOTNO_ADDR(), row.getLOCPLC_ROADNM_ADDR(),
                                row.getLICENSG_DE(), row.getBSN_STATE_NM(), row.getCLSBIZ_DE(), row.getSUSPNBIZ_BEGIN_DE(), row.getSUSPNBIZ_END_DE(), row.getREOPENBIZ_DE(),
                                row.getLOCPLC_AR(), row.getLOCPLC_ZIP_CD(), row.getGENRL_AMBLNC_CNT(), row.getSPECL_AMBLNC_CNT(), row.getSICKBD_CNT(), row.getEASING_MEDCARE_CHARGE_DEPT_NM(),
                                row.getEASING_MEDCARE_APPONT_FORM(), row.getMEDCARE_INST_ASORTMT_NM(), row.getMEDSTAF_CNT(), row.getHOSPTLRM_CNT(), row.getTREAT_SBJECT_CD_INFO(),
                                row.getTREAT_SBJECT_CONT(), row.getTOT_AR(), row.getTOT_PSN_CNT(), row.getFIRST_APPONT_DE(), row.getPERMISN_SICKBD_CNT(), row.getWGS84_LOGT(), row.getWGS84_LAT(),
                                row.getX_CRDNT(), row.getY_CRDNT(), row.getDATA_COLCT_DE(), row.getETL_LDADNG_DTM(), row.getLAT(), row.getLNG()
                        );
                        realm.insertOrUpdate(newModel);
                    }
                });
            }

            editor.putInt(COUNT_HOSPITAL, currentCount);
            editor.commit();
            Log.e(CLASS_NAME, COUNT_HOSPITAL + " : changed data saved");
        } else if (pastCount == 0 && currentCount != 0) {                        //이전에 저장된 데이터가 없고, 새로운 데이터가 존재할 때
            Log.e(CLASS_NAME, COUNT_HOSPITAL + " : first data reveived");
            int cnt = 0;
            for (final HospitalModel.DataBean.RowBean row : hospitalModel.getData().getRow()) {
                final int _id = cnt++;

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        kr.edcan.lumihana.safegangwon.RealmModel.HospitalModel newModel = new kr.edcan.lumihana.safegangwon.RealmModel.HospitalModel(
                                _id, row.getNO(), row.getSIGUN_CD(), row.getSIGUN_NM(), row.getBIZPLC_NM(), row.getLOCPLC_LOTNO_ADDR(), row.getLOCPLC_ROADNM_ADDR(),
                                row.getLICENSG_DE(), row.getBSN_STATE_NM(), row.getCLSBIZ_DE(), row.getSUSPNBIZ_BEGIN_DE(), row.getSUSPNBIZ_END_DE(), row.getREOPENBIZ_DE(),
                                row.getLOCPLC_AR(), row.getLOCPLC_ZIP_CD(), row.getGENRL_AMBLNC_CNT(), row.getSPECL_AMBLNC_CNT(), row.getSICKBD_CNT(), row.getEASING_MEDCARE_CHARGE_DEPT_NM(),
                                row.getEASING_MEDCARE_APPONT_FORM(), row.getMEDCARE_INST_ASORTMT_NM(), row.getMEDSTAF_CNT(), row.getHOSPTLRM_CNT(), row.getTREAT_SBJECT_CD_INFO(),
                                row.getTREAT_SBJECT_CONT(), row.getTOT_AR(), row.getTOT_PSN_CNT(), row.getFIRST_APPONT_DE(), row.getPERMISN_SICKBD_CNT(), row.getWGS84_LOGT(), row.getWGS84_LAT(),
                                row.getX_CRDNT(), row.getY_CRDNT(), row.getDATA_COLCT_DE(), row.getETL_LDADNG_DTM(), row.getLAT(), row.getLNG()
                        );
                        realm.insertOrUpdate(newModel);
                    }
                });
            }

            editor.putInt(COUNT_HOSPITAL, currentCount);
            editor.commit();
            Log.e(CLASS_NAME, COUNT_HOSPITAL + " : first data saved");
        } else if (currentCount == 0) {                                   //새로운 데이터가 존재하지 않을 때
            Log.e(CLASS_NAME, COUNT_HOSPITAL + " : No data received.");
        } else if (pastCount == currentCount) {                            //이전에 저장된 데이터의 양과 새로운 데이터의 양이 같을 때
            Log.e(CLASS_NAME, COUNT_HOSPITAL + " : No data changed");
        }
        pauseDB();
    }

    @Override
    public void emergenctNoticeChanged(EmergencyNoticeModel emergencyNoticeModel) {
        initRealm();
        final int pastCount = sharedPreferences.getInt(COUNT_EMERGENCY_NOTICE, 0);
        final int currentCount = emergencyNoticeModel.getData().get(0).getHead().get(0).getList_total_count();

        if (pastCount != 0 && pastCount < currentCount) {                       //이전에 저장된 데이터가 존재하고, 새로운 데이터의 양이 더 많을 때
            Log.e(CLASS_NAME, COUNT_EMERGENCY_NOTICE + " : data changed");
            int cnt = 0;
            for (final EmergencyNoticeModel.DataBean.RowBean row : emergencyNoticeModel.getData().get(1).getRow()) {
                final int _id = cnt++;

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        kr.edcan.lumihana.safegangwon.RealmModel.EmergencyNoticeModel newModel = new kr.edcan.lumihana.safegangwon.RealmModel.EmergencyNoticeModel(
                                _id, row.getMsg_id(), row.getMsg_seq(), row.getClmy_pttn_cd(), row.getClmy_pttn_nm(), row.getTitl(), row.getCnts1(), row.getInpt_date()
                        );
                        realm.insertOrUpdate(newModel);
                    }
                });
            }

            editor.putInt(COUNT_EMERGENCY_NOTICE, currentCount);
            editor.commit();
            Log.e(CLASS_NAME, COUNT_EMERGENCY_NOTICE + " : changed data saved");
        } else if (pastCount == 0 && currentCount != 0) {                        //이전에 저장된 데이터가 없고, 새로운 데이터가 존재할 때
            Log.e(CLASS_NAME, COUNT_EMERGENCY_NOTICE + " : first data reveived");
            if(emergencyNoticeModel.getData().get(1).getRow()==null) Log.e("hello", "Null");
            Log.e("hello", emergencyNoticeModel.getData().get(0).getHead().get(0).getList_total_count()+"");
            int cnt = 0;
            for (final EmergencyNoticeModel.DataBean.RowBean row : emergencyNoticeModel.getData().get(1).getRow()) {
                final int _id = cnt++;

                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        kr.edcan.lumihana.safegangwon.RealmModel.EmergencyNoticeModel newModel = new kr.edcan.lumihana.safegangwon.RealmModel.EmergencyNoticeModel(
                                _id, row.getMsg_id(), row.getMsg_seq(), row.getClmy_pttn_cd(), row.getClmy_pttn_nm(), row.getTitl(), row.getCnts1(), row.getInpt_date()
                        );
                        realm.insertOrUpdate(newModel);
                    }
                });
            }

            editor.putInt(COUNT_EMERGENCY_NOTICE, currentCount);
            editor.commit();
            Log.e(CLASS_NAME, COUNT_EMERGENCY_NOTICE + " : first data saved");
        } else if (currentCount == 0) {                                   //새로운 데이터가 존재하지 않을 때
            Log.e(CLASS_NAME, COUNT_EMERGENCY_NOTICE + " : No data received.");
        } else if (pastCount == currentCount) {                            //이전에 저장된 데이터의 양과 새로운 데이터의 양이 같을 때
            Log.e(CLASS_NAME, COUNT_EMERGENCY_NOTICE + " : No data changed");
        }
        pauseDB();
    }

    @Deprecated()
    @Override
    public void weatherWithAddress(WeatherModel weather) {

    }

    @Deprecated
    @Override
    public void weatherWithLocation(WeatherModel weather) {

    }

    @Deprecated
    @Override
    public void weatherWithId(WeatherModel weather) {

    }

    @Override
    public void onChanged(WeatherModel weather) {
        initRealm();
        Log.e(CLASS_NAME, COUNT_WEATHER + " : weather data received");
        final WeatherModel.WeatherBean.MinutelyBean minutely = weather.getWeather().getMinutely().get(0);

        final int _id = 0;
        final kr.edcan.lumihana.safegangwon.RealmModel.WeatherModel newModel = new kr.edcan.lumihana.safegangwon.RealmModel.WeatherModel(
                _id,
                weather.getCommon().getAlertYn(), weather.getCommon().getStormYn(),
                minutely.getHumidity(),
                minutely.getLightning(),
                minutely.getTimeObservation(),
                minutely.getStation().getName(), minutely.getStation().getId(), minutely.getStation().getType(), minutely.getStation().getLatitude(), minutely.getStation().getLongitude(),
                minutely.getWind().getWdir(), minutely.getWind().getWspd(),
                minutely.getPrecipitation().getType(), minutely.getPrecipitation().getSinceOntime(),
                minutely.getSky().getName(), minutely.getSky().getCode(),
                minutely.getRain().getSinceOntime(), minutely.getRain().getSinceMidnight(), minutely.getRain().getLast10min(), minutely.getRain().getLast15min(), minutely.getRain().getLast30min(), minutely.getRain().getLast1hour(), minutely.getRain().getLast6hour(), minutely.getRain().getLast12hour(), minutely.getRain().getLast24hour(),
                minutely.getPressure().getSurface(), minutely.getPressure().getSeaLevel(),
                minutely.getTemperature().getTc(), minutely.getTemperature().getTmax(), minutely.getTemperature().getTmin()
        );

        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insertOrUpdate(newModel);
            }
        });
        Log.e(CLASS_NAME, COUNT_WEATHER + " : weather data saved");
        pauseDB();
    }
}
