package kr.edcan.lumihana.safegangwon.Util;

import android.content.Context;
import android.util.Log;

import com.squareup.okhttp.ResponseBody;

import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by kimok_000 on 2016-09-23.
 */
public class FireMessageManager {
    private static final String CLASS_NAME = "FireMessageManager";
    private Context context;
    private Retrofit retrofit;
    private IFireMessageService fireMessageService;
    private Call<ResponseBody> registerToken;

    public FireMessageManager(Context context) {
        this.context = context;

        initRetrofit();
    }

    private void initRetrofit() {
        retrofit = new Retrofit.Builder()
                .baseUrl("http://milkgun.kr:3000/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        fireMessageService = retrofit.create(IFireMessageService.class);
    }

    public void registerToken(String token, String id){
        registerToken = fireMessageService.registerToken(token, id);
        registerToken.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Response<ResponseBody> response, Retrofit retrofit) {
                if(response.code() == 200){
                    Log.e(CLASS_NAME, "successfully sended");
                }else Log.e(CLASS_NAME, "Milkgun Server return !200");
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e(CLASS_NAME, "Failed to send token");
                Log.e(CLASS_NAME, t.getMessage() + "");
            }
        });
    }
}
