package kr.edcan.lumihana.safegangwon.LiveModel;

/**
 * Created by kimok_000 on 2016-09-28.
 */
public class LiveSnowModel extends ParentModel{
    private String caname;
    private String addr;

    public LiveSnowModel(String caname, String addr) {
        super.typeOfModel = ParentModel.TYPE_SNOW;
        this.caname = caname;
        this.addr = addr;
    }

    public void setCaname(String caname) {
        this.caname = caname;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getCaname() {
        return caname;
    }

    public String getAddr() {
        return addr;
    }
}
