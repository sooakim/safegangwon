package kr.edcan.lumihana.safegangwon.Util;

import kr.edcan.lumihana.safegangwon.Model.CivilDefenseModel;
import kr.edcan.lumihana.safegangwon.Model.EarthModel;
import kr.edcan.lumihana.safegangwon.Model.EmergencyCenterModel;
import kr.edcan.lumihana.safegangwon.Model.GeneralHospitalModel;
import kr.edcan.lumihana.safegangwon.Model.HospitalModel;
import kr.edcan.lumihana.safegangwon.Model.SnowModel;
import kr.edcan.lumihana.safegangwon.Model.WaterSupplyModel;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by kimok_000 on 2016-09-22.
 */
public interface IGangwonService {
    @GET("{KEY}/{TYPE}/{SERVICE}/{START_INDEX}/{END_INDEX}/")
    Call<SnowModel> snow(@Path("KEY") String key, @Path("TYPE") String type, @Path("SERVICE") String service,
                                @Path("START_INDEX") int startIndex, @Path("END_INDEX") int endIndex);

    @GET("{KEY}/{TYPE}/{SERVICE}/{START_INDEX}/{END_INDEX}/")
    Call<EarthModel> earth(@Path("KEY") String key, @Path("TYPE") String type, @Path("SERVICE") String service,
                           @Path("START_INDEX") int startIndex, @Path("END_INDEX") int endIndex);

    @GET("{KEY}/{TYPE}/{SERVICE}/{START_INDEX}/{END_INDEX}/")
    Call<CivilDefenseModel> civilDefense(@Path("KEY") String key, @Path("TYPE") String type, @Path("SERVICE") String service,
                                        @Path("START_INDEX") int startIndex, @Path("END_INDEX") int endIndex);

    @GET("{KEY}/{TYPE}/{SERVICE}/{START_INDEX}/{END_INDEX}/")
    Call<WaterSupplyModel> waterSupply(@Path("KEY") String key, @Path("TYPE") String type, @Path("SERVICE") String service,
                                      @Path("START_INDEX") int startIndex, @Path("END_INDEX") int endIndex);

    @GET("{KEY}/{TYPE}/{SERVICE}/{START_INDEX}/{END_INDEX}/")
    Call<GeneralHospitalModel> generalHospital(@Path("KEY") String key, @Path("TYPE") String type, @Path("SERVICE") String service,
                                           @Path("START_INDEX") int startIndex, @Path("END_INDEX") int endIndex);

    @GET("{KEY}/{TYPE}/{SERVICE}/{START_INDEX}/{END_INDEX}/")
    Call<EmergencyCenterModel> emergencyCenter(@Path("KEY") String key, @Path("TYPE") String type, @Path("SERVICE") String service,
                                           @Path("START_INDEX") int startIndex, @Path("END_INDEX") int endIndex);

    @GET("{KEY}/{TYPE}/{SERVICE}/{START_INDEX}/{END_INDEX}/")
    Call<HospitalModel> hospital(@Path("KEY") String key, @Path("TYPE") String type, @Path("SERVICE") String service,
                                    @Path("START_INDEX") int startIndex, @Path("END_INDEX") int endIndex);
}
