package kr.edcan.lumihana.safegangwon.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import kr.edcan.lumihana.safegangwon.R;

public class HelpActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private TextView text_desc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        setToolbar();
        setHelp();
    }

    private void setToolbar(){
        toolbar = (Toolbar) findViewById(R.id.help_toolbar);
        toolbar.setTitle(getTitleFromIntent());
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        setSupportActionBar(toolbar);
    }

    private String getTitleFromIntent(){
        final Intent intent = getIntent();
        return intent.getStringExtra("title");
    }

    private String getDescFromIntent(){
        final Intent intent = getIntent();
        return intent.getStringExtra("desc");
    }

    private void setHelp(){
        text_desc = (TextView) findViewById(R.id.help_text_desc);
        text_desc.setText(getDescFromIntent());
    }
}
