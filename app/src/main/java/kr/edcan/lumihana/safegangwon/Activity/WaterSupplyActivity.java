package kr.edcan.lumihana.safegangwon.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;
import kr.edcan.lumihana.safegangwon.Adapter.DataAdapter;
import kr.edcan.lumihana.safegangwon.Model.DataModel;
import kr.edcan.lumihana.safegangwon.R;
import kr.edcan.lumihana.safegangwon.RealmModel.WaterSupplyModel;
import kr.edcan.lumihana.safegangwon.Util.NetworkManager;

public class WaterSupplyActivity extends AppCompatActivity implements OnMapReadyCallback {
    private static final String POINT_NAME = "급수 시설";
    private GoogleMap watersupply_map;
    private Toolbar toolbar;
    private ArrayList<DataModel> arrayList;
    private RecyclerView recycler_data;
    private RelativeLayout relative_map;
    private LinearLayoutManager linearLayoutManager;
    private DataAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_water_supply);

        relative_map = (RelativeLayout) findViewById(R.id.watersupply_relative_map);
        initToolbar();
        initMap();
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.watersupply_toolbar);
        toolbar.setTitle("급수 시설");
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_36px);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.watersupply_map);
        mapFragment.getMapAsync(this);
    }

    private void updateRecycler() {
        adapter.notifyDataSetChanged();
    }

    private void initRecycler() {
        arrayList = new ArrayList<>();

        linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        adapter = new DataAdapter(getApplicationContext(), arrayList);

        recycler_data = (RecyclerView) findViewById(R.id.watersupply_recycler_data);
        recycler_data.setLayoutManager(linearLayoutManager);
        recycler_data.setAdapter(adapter);
    }

    private void fillRecycler() {
        final Intent intent = getIntent();
        final int _id = intent.getIntExtra("_id", -1);

        if (_id >= 0) {
            Realm realm = Realm.getDefaultInstance();
            RealmResults<WaterSupplyModel> results = realm.where(WaterSupplyModel.class).equalTo("_id", _id).findAll();

            WaterSupplyModel model;
            if (results.size() <= 0) model = null;
            else model = results.get(0);

            if (model != null) {
                final int no = model.getNO();
                final String sigun_cd = getModelValue(model.getSIGUN_CD());
                final String sigun_nm = getModelValue(model.getSIGUN_NM());
                final String bizplc_nm = getModelValue(model.getBIZPLC_NM());
                final String locplc_lonto_addr = getModelValue(model.getLOCPLC_LOTNO_ADDR());
                final String locplc_roadnm_addr = getModelValue(model.getLOCPLC_ROADNM_ADDR());
                final String licensg_de = getModelValue(model.getLICENSG_DE());
                final String bsn_state_nm = getModelValue(model.getBSN_STATE_NM());
                final String clsbiz_de = getModelValue(model.getCLSBIZ_DE());
                final String suspnbiz_begin_de = getModelValue(model.getSUSPNBIZ_BEGIN_DE());
                final String suspnbiz_end_de = getModelValue(model.getSUSPNBIZ_END_DE());
                final String reopenbiz_de = getModelValue(model.getREOPENBIZ_DE());
                final int locplc_ar = model.getLOCPLC_AR();
                final String locplc_zip_cd = getModelValue(model.getLOCPLC_ZIP_CD());
                final String faclt_buldng_nm = getModelValue(model.getFACLT_BULDNG_NM());
                final String release_de = getModelValue(model.getRELEASE_DE());
                final String emgncy_faclt_div_nm = getModelValue(model.getEMGNCY_FACLT_DIV_NM());
                final int wgs84_logt = model.getWGS84_LOGT();
                final int wgs84_lat = model.getWGS84_LAT();
                final int x_crdnt = model.getX_CRDNT();
                final int y_crdnt = model.getY_CRDNT();
                final String data_colct_de = getModelValue(model.getDATA_COLCT_DE());
                final String etl_ldadng_dtm = getModelValue(model.getETL_LDADNG_DTM());
                final String lat = getModelValue(model.getLAT());
                final String lng = getModelValue(model.getLNG());

                if (no != 0) arrayList.add(new DataModel("번호", no + ""));
                if (!emgncy_faclt_div_nm.equals(""))
                    arrayList.add(new DataModel("비상 시설 구분명", emgncy_faclt_div_nm));
                if (locplc_ar != 0) arrayList.add(new DataModel("소재지 면적", locplc_ar + ""));
                if (!faclt_buldng_nm.equals(""))
                    arrayList.add(new DataModel("건물 명", faclt_buldng_nm));
                if (!bizplc_nm.equals("")) arrayList.add(new DataModel("사업장명", bizplc_nm));
                if (!sigun_nm.equals("")) arrayList.add(new DataModel("시/ 군", sigun_nm));
                if (!locplc_zip_cd.equals("")) arrayList.add(new DataModel("우편 번호", locplc_zip_cd));
                if (!locplc_lonto_addr.equals(""))
                    arrayList.add(new DataModel("지번 주소", locplc_lonto_addr));
                if (!locplc_roadnm_addr.equals(""))
                    arrayList.add(new DataModel("도로명 주소", locplc_roadnm_addr));
                if (!licensg_de.equals("")) arrayList.add(new DataModel("허가 일자", licensg_de));
                if (!release_de.equals("")) arrayList.add(new DataModel("해제 일자", release_de));
                if (!bsn_state_nm.equals("")) arrayList.add(new DataModel("영업 상태", bsn_state_nm));
                if (!clsbiz_de.equals("")) arrayList.add(new DataModel("폐업 일자", clsbiz_de));
                if (!suspnbiz_begin_de.equals(""))
                    arrayList.add(new DataModel("휴업 시작 일자", suspnbiz_begin_de));
                if (!suspnbiz_end_de.equals(""))
                    arrayList.add(new DataModel("휴업 종료 일자", suspnbiz_end_de));
                if (!reopenbiz_de.equals("")) arrayList.add(new DataModel("재개업 일자", reopenbiz_de));
                if (!data_colct_de.equals(""))
                    arrayList.add(new DataModel("데이터 수집 일자", data_colct_de));
                if (!lat.equals("") || !lng.equals(""))
                    pointInMap(Double.parseDouble(lat), Double.parseDouble(lng));
                else {
                    relative_map.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), "지도 정보 없음", Toast.LENGTH_SHORT).show();
                }

                updateRecycler();
            } else {

            }
        } else {

        }
    }

    private String getModelValue(String original) {
        if (original == null) return "";
        else return original.toString().trim();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        watersupply_map = googleMap;

        initRecycler();
        fillRecycler();
    }

    private void pointInMap(double lat, double lng) {
        if(!(125.0<=lng&&lng<=132.0) && !(32<=lat&&lat<=39)) {
            relative_map.setVisibility(View.GONE);  //잘못된 좌표
            Toast.makeText(getApplicationContext(), "지도를 표시할 수 없음", Toast.LENGTH_SHORT).show();
        }
        LatLng point = new LatLng(lng, lat);
        watersupply_map.addMarker(new MarkerOptions().position(point).title(POINT_NAME));
        watersupply_map.moveCamera(CameraUpdateFactory.newLatLngZoom(point, 16));
    }

    private boolean checkNetwork() {
        if (NetworkManager.getConnectivityStatus(getApplicationContext()) == NetworkManager.TYPE_NOT_CONNECTED) {
            RelativeLayout map = (RelativeLayout) findViewById(R.id.watersupply_relative_map);

            Snackbar.make(map, "네트워크에 연결해주세요.", Snackbar.LENGTH_LONG)
                    .setAction("연결", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final Intent intent = new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    }).show();
            return false;
        } else return true;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        checkNetwork();
    }

    @Override
    protected void onResume() {
        super.onResume();
        onStart();
    }
}
