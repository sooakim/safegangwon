package kr.edcan.lumihana.safegangwon.Util;

import kr.edcan.lumihana.safegangwon.Model.EmergencyNoticeModel;
import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by kimok_000 on 2016-09-23.
 */
public interface IDataService {
    @GET("MisfortuneSituationNoticeMsg/")
    Call<EmergencyNoticeModel> emergencyNotice(@Query("KEY") String key, @Query("Type") String type, @Query("pIndex") int startIndex, @Query("pSize") int endIndex);
}
