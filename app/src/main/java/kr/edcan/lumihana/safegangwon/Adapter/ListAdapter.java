package kr.edcan.lumihana.safegangwon.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import kr.edcan.lumihana.safegangwon.Activity.CivilDefenseActivity;
import kr.edcan.lumihana.safegangwon.Activity.EarthActivity;
import kr.edcan.lumihana.safegangwon.Activity.EmergencyCenterActivity;
import kr.edcan.lumihana.safegangwon.Activity.EmergencyNoticeActivity;
import kr.edcan.lumihana.safegangwon.Activity.GeneralHospitalActivity;
import kr.edcan.lumihana.safegangwon.Activity.HospitalActivity;
import kr.edcan.lumihana.safegangwon.Activity.SnowActivity;
import kr.edcan.lumihana.safegangwon.Activity.WaterSupplyActivity;
import kr.edcan.lumihana.safegangwon.LiveModel.ParentModel;
import kr.edcan.lumihana.safegangwon.Model.ListDataModel;
import kr.edcan.lumihana.safegangwon.R;

/**
 * Created by kimok_000 on 2016-09-26.
 */
public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {
    private Context context;
    private ArrayList<ListDataModel> arrayList;

    public ListAdapter(Context context, ArrayList<ListDataModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_list, parent, false);
        view.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ListDataModel model = arrayList.get(position);
        final int _id = model.get_id();
        final int type = model.getType();
        final String number = getString(model.getNumber()).toString().trim();
        final String title = getString(model.getTitle()).toString().trim();
        final String content = getString(model.getContent()).toString().trim();
        final String date = getString(model.getDate()).toString().trim();

        if (!number.equals("")) holder.text_number.setText(number + "");
        if (!title.equals("")) holder.text_title.setText(title + "");
        if (!content.equals("")) holder.text_content.setText(content + "");
        if (!date.equals("")) holder.text_date.setText(date + "");

        holder.card_root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                switch (type) {
                    case ParentModel.TYPE_CIVIL_DEFENSE:
                        intent = new Intent(context, CivilDefenseActivity.class);
                        break;
                    case ParentModel.TYPE_EARTH:
                        intent = new Intent(context, EarthActivity.class);
                        break;
                    case ParentModel.TYPE_EMERGENCY_CENTER:
                        intent = new Intent(context, EmergencyCenterActivity.class);
                        break;
                    case ParentModel.TYPE_EMERGENCY_NOTICE:
                        intent = new Intent(context, EmergencyNoticeActivity.class);
                        break;
                    case ParentModel.TYPE_GENERAL_HOSPITAL:
                        intent = new Intent(context, GeneralHospitalActivity.class);
                        break;
                    case ParentModel.TYPE_HOSPITAL:
                        intent = new Intent(context, HospitalActivity.class);
                        break;
                    case ParentModel.TYPE_SNOW:
                        intent = new Intent(context, SnowActivity.class);
                        break;
                    case ParentModel.TYPE_WATER_SUPPLY:
                        intent = new Intent(context, WaterSupplyActivity.class);
                        break;
                    case ParentModel.TYPE_WEATHER:
                        intent = null;
                        break;
                    case ParentModel.TYPE_PARENT:
                        intent = null;
                        break;
                    default:
                        intent = null;
                        break;
                }
                if (intent != null) {
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK).putExtra("_id", _id);
                    context.startActivity(intent);
                }
            }
        });
    }

    private String getString(String nullable) {
        if (nullable == null) return "";
        else return nullable;
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView text_number, text_title, text_content, text_date;
        private CardView card_root;

        public ViewHolder(View itemView) {
            super(itemView);

            card_root = (CardView) itemView.findViewById(R.id.list_card_root);
            text_number = (TextView) itemView.findViewById(R.id.list_text_number);
            text_title = (TextView) itemView.findViewById(R.id.list_text_title);
            text_content = (TextView) itemView.findViewById(R.id.list_text_content);
            text_date = (TextView) itemView.findViewById(R.id.list_text_date);
        }
    }
}
