package kr.edcan.lumihana.safegangwon.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

import kr.edcan.lumihana.safegangwon.R;
import kr.edcan.lumihana.safegangwon.Util.NetworkManager;

public class ForgotActivity extends AppCompatActivity {
    private static final String CLASS_NAME = "ForgotActivity";
    private FrameLayout frame_root;
    private EditText edit_email;
    private FloatingActionButton floating_next;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot);

        init();
        initFirebase();
        checkNetwork();
    }

    private boolean checkNetwork() {
        if (NetworkManager.getConnectivityStatus(getApplicationContext()) == NetworkManager.TYPE_NOT_CONNECTED) {
            floating_next.setEnabled(false);

            Snackbar.make(frame_root, "네트워크 연결을 확인하세요.", Snackbar.LENGTH_LONG)
                    .setAction("연결", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final Intent intent = new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    })
                    .show();
            return false;
        } else {
            floating_next.setEnabled(true);
            return true;
        }
    }

    private void initFirebase() {
        firebaseAuth = FirebaseAuth.getInstance();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        checkNetwork();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkNetwork();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        firebaseAuth = null;
    }

    private void init() {
        frame_root = (FrameLayout) findViewById(R.id.forgot_frame_root);
        edit_email = (EditText) findViewById(R.id.forgot_edit_email);
        floating_next = (FloatingActionButton) findViewById(R.id.forgot_floating_next);

        floating_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email = edit_email.getText().toString().trim();

                if (email.equals("")) {
                    Snackbar.make(frame_root, "이메일을 입력해주세요", Snackbar.LENGTH_SHORT).show();
                    return;
                }

                firebaseAuth.sendPasswordResetEmail(email)
                        .addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    Snackbar.make(frame_root, email + "로 재설정 메일이 발송되었습니다.", Snackbar.LENGTH_SHORT).show();
                                } else {
                                    Snackbar.make(frame_root, task.getException().getLocalizedMessage() + "", Snackbar.LENGTH_SHORT).show();
                                    return;
                                }
                            }
                        });
            }
        });
    }
}
