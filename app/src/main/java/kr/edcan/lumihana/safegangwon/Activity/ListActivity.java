package kr.edcan.lumihana.safegangwon.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import kr.edcan.lumihana.safegangwon.Adapter.ListAdapter;
import kr.edcan.lumihana.safegangwon.LiveModel.ParentModel;
import kr.edcan.lumihana.safegangwon.Model.ListDataModel;
import kr.edcan.lumihana.safegangwon.R;
import kr.edcan.lumihana.safegangwon.RealmModel.CivilDefenseModel;
import kr.edcan.lumihana.safegangwon.RealmModel.EarthModel;
import kr.edcan.lumihana.safegangwon.RealmModel.EmergencyCenterModel;
import kr.edcan.lumihana.safegangwon.RealmModel.EmergencyNoticeModel;
import kr.edcan.lumihana.safegangwon.RealmModel.GeneralHospitalModel;
import kr.edcan.lumihana.safegangwon.RealmModel.HospitalModel;
import kr.edcan.lumihana.safegangwon.RealmModel.SnowModel;
import kr.edcan.lumihana.safegangwon.RealmModel.WaterSupplyModel;

public class ListActivity extends AppCompatActivity {
    private static final String CLASS_NAME = "ListActivity";
    private Toolbar toolbar;
    private EditText edit_search;
    private ImageView image_clear;
    private TextView text_search;
    private RecyclerView recycler_list;
    private LinearLayoutManager linearLayoutManager;
    private ListAdapter adapter;
    private ArrayList<ListDataModel> arrayList;
    private String search, what;
    private int classType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        getIntentData();
        initToolbar();
        initRecycler();
        init();
        onSearch("");
    }

    private void initRecycler() {
        arrayList = new ArrayList<>();

        linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        adapter = new ListAdapter(getApplicationContext(), arrayList);

        recycler_list = (RecyclerView) findViewById(R.id.list_recycler_list);
        recycler_list.setLayoutManager(linearLayoutManager);
        recycler_list.setAdapter(adapter);
    }

    private void updateRecycler() {
        adapter.notifyDataSetChanged();
    }

    private void init() {
        edit_search = (EditText) findViewById(R.id.list_edit_search);
        text_search = (TextView) findViewById(R.id.list_text_search);
        image_clear = (ImageView) findViewById(R.id.list_image_clear);

        image_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edit_search.setText("");
            }
        });

        text_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String data = edit_search.getText().toString();
                if (!data.equals("")) onSearch(data);
                else
                    Toast.makeText(getApplicationContext(), "검색어를 입력해주세요", Toast.LENGTH_SHORT).show();
            }
        });


        edit_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                arrayList.clear();
                updateRecycler();
                search = s.toString();
                onSearch(search);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    private void onSearch(String search) {
        Realm realm = Realm.getDefaultInstance();
        switch (classType) {
            case ParentModel.TYPE_EMERGENCY_NOTICE: {
                if (search.equals("")) {
                    RealmResults<EmergencyNoticeModel> results = realm.where(EmergencyNoticeModel.class).findAll().sort("inpt_date",Sort.DESCENDING);
                    if (results.size() <= 0) Log.e(CLASS_NAME, "No data");
                    else {
                        for (EmergencyNoticeModel model : results) {
                            final int _id = model.get_id();
                            final int number = (int) model.getMsg_id();
                            final String title = model.getTitl();
                            final String content = model.getClmy_pttn_nm();
                            final String date = model.getInpt_date();

                            arrayList.add(new ListDataModel(_id, classType, number + "", title, content, date));
                        }

                        updateRecycler();
                    }
                } else {
                    RealmResults<EmergencyNoticeModel> results = realm.where(EmergencyNoticeModel.class)
                            .contains("titl", search)
                            .or()
                            .contains("cnts1", search)
                            .findAll()
                            .sort("inpt_date", Sort.DESCENDING);

                    if (results.size() <= 0) Log.e(CLASS_NAME, "::No data");
                    else {
                        for (EmergencyNoticeModel model : results) {
                            final int _id = model.get_id();
                            final int number = (int) model.getMsg_id();
                            final String title = model.getTitl();
                            final String content = model.getClmy_pttn_nm();
                            final String date = model.getInpt_date();

                            arrayList.add(new ListDataModel(_id, classType, number + "", title, content, date));
                        }

                        updateRecycler();
                    }
                }
                break;
            }

            case ParentModel.TYPE_CIVIL_DEFENSE: {
                if (search.equals("")) {
                    RealmResults<CivilDefenseModel> results = realm.where(CivilDefenseModel.class).findAll();
                    if (results.size() <= 0) Log.e(CLASS_NAME, "No data");
                    else {
                        for (CivilDefenseModel model : results) {
                            final int _id = model.get_id();
                            final int number = model.getNO();
                            final String title = model.getBIZPLC_NM();
                            final String content = model.getLOCPLC_LOTNO_ADDR();
                            final String date = model.getDATA_COLCT_DE();

                            arrayList.add(new ListDataModel(_id, classType, number + "", title, content, date));
                        }

                        updateRecycler();
                    }
                } else {
                    RealmResults<CivilDefenseModel> results = realm.where(CivilDefenseModel.class)
                            .contains("LOCPLC_LOTNO_ADDR", search)
                            .or()
                            .contains("BIZPLC_NM", search)
                            .findAll();

                    if (results.size() <= 0) Log.e(CLASS_NAME, "No Data");
                    else {
                        for (CivilDefenseModel model : results) {
                            final int _id = model.get_id();
                            final int number = model.getNO();
                            final String title = model.getBIZPLC_NM();
                            final String content = model.getLOCPLC_LOTNO_ADDR();
                            final String date = model.getDATA_COLCT_DE();

                            arrayList.add(new ListDataModel(_id, classType, number + "", title, content, date));
                        }
                        updateRecycler();
                    }
                }

                break;
            }

            case ParentModel.TYPE_EARTH: {
                if (search.equals("")) {
                    RealmResults<EarthModel> results = realm.where(EarthModel.class).findAll();
                    if (results.size() <= 0) Log.e(CLASS_NAME, "No data");
                    else {
                        for (EarthModel model : results) {
                            final int _id = model.get_id();
                            final int number = model.get_id() + 1;
                            final String title = model.getOBS_NM();
                            final String content = model.getLOCPLC_LOTNO_ADDR();
                            final String date = model.getOBS_STATUS();

                            arrayList.add(new ListDataModel(_id, classType, number + "", title, content, date));
                        }

                        updateRecycler();
                    }
                } else {
                    RealmResults<EarthModel> results = realm.where(EarthModel.class)
                            .contains("LOCPLC_LOTNO_ADDR", search)
                            .or()
                            .contains("OBS_NM", search)
                            .findAll();

                    if (results.size() <= 0) Log.e(CLASS_NAME, "No data");
                    else {
                        for (EarthModel model : results) {
                            final int _id = model.get_id();
                            final int number = model.get_id() + 1;
                            final String title = model.getOBS_NM();
                            final String content = model.getLOCPLC_LOTNO_ADDR();
                            final String date = model.getOBS_STATUS();

                            arrayList.add(new ListDataModel(_id, classType, number + "", title, content, date));
                        }

                        updateRecycler();
                    }
                }
                break;
            }

            case ParentModel.TYPE_EMERGENCY_CENTER: {
                if (search.equals("")) {
                    RealmResults<EmergencyCenterModel> results = realm.where(EmergencyCenterModel.class).findAll();
                    if (results.size() <= 0) Log.e(CLASS_NAME, "No data");
                    else {
                        for (EmergencyCenterModel model : results) {
                            final int _id = model.get_id();
                            final int number = model.get_id() + 1;
                            final String title = model.getBIZPLC_NM();
                            final String content = model.getLOCPLC_LOTNO_ADDR();
                            final String date = model.getDATA_COLCT_DE();

                            arrayList.add(new ListDataModel(_id, classType, number + "", title, content, date));
                        }

                        updateRecycler();
                    }
                } else {
                    RealmResults<EmergencyCenterModel> results = realm.where(EmergencyCenterModel.class)
                            .contains("LOCPLC_LOTNO_ADDR", search)
                            .or()
                            .contains("BIZPLC_NM", search)
                            .findAll();

                    if (results.size() <= 0) Log.e(CLASS_NAME, "No data");
                    else {
                        for (EmergencyCenterModel model : results) {
                            final int _id = model.get_id();
                            final int number = model.get_id() + 1;
                            final String title = model.getBIZPLC_NM();
                            final String content = model.getLOCPLC_LOTNO_ADDR();
                            final String date = model.getDATA_COLCT_DE();

                            arrayList.add(new ListDataModel(_id, classType, number + "", title, content, date));
                        }

                        updateRecycler();
                    }
                }
                break;
            }

            case ParentModel.TYPE_GENERAL_HOSPITAL: {
                if (search.equals("")) {
                    RealmResults<GeneralHospitalModel> results = realm.where(GeneralHospitalModel.class).findAll();
                    if (results.size() <= 0) Log.e(CLASS_NAME, "No data");
                    else {
                        for (GeneralHospitalModel model : results) {
                            final int _id = model.get_id();
                            final int number = model.get_id() + 1;
                            final String title = model.getBIZPLC_NM();
                            final String content = model.getLOCPLC_LOTNO_ADDR();
                            final String date = model.getDATA_COLCT_DE();

                            arrayList.add(new ListDataModel(_id, classType, number + "", title, content, date));
                        }

                        updateRecycler();
                    }
                } else {
                    RealmResults<GeneralHospitalModel> results = realm.where(GeneralHospitalModel.class)
                            .contains("LOCPLC_LOTNO_ADDR", search)
                            .or()
                            .contains("BIZPLC_NM", search)
                            .findAll();

                    if (results.size() <= 0) Log.e(CLASS_NAME, "No data");
                    else {
                        for (GeneralHospitalModel model : results) {
                            final int _id = model.get_id();
                            final int number = model.get_id() + 1;
                            final String title = model.getBIZPLC_NM();
                            final String content = model.getLOCPLC_LOTNO_ADDR();
                            final String date = model.getDATA_COLCT_DE();

                            arrayList.add(new ListDataModel(_id, classType, number + "", title, content, date));
                        }

                        updateRecycler();
                    }
                }
                break;
            }

            case ParentModel.TYPE_HOSPITAL: {
                if (search.equals("")) {
                    RealmResults<HospitalModel> results = realm.where(HospitalModel.class).findAll();
                    if (results.size() <= 0) Log.e(CLASS_NAME, "No data");
                    else {
                        for (HospitalModel model : results) {
                            final int _id = model.get_id();
                            final int number = model.get_id() + 1;
                            final String title = model.getBIZPLC_NM();
                            final String content = model.getLOCPLC_LOTNO_ADDR();
                            final String date = model.getDATA_COLCT_DE();

                            arrayList.add(new ListDataModel(_id, classType, number + "", title, content, date));
                        }

                        updateRecycler();
                    }
                } else {
                    RealmResults<HospitalModel> results = realm.where(HospitalModel.class)
                            .contains("LOCPLC_LOTNO_ADDR", search)
                            .or()
                            .contains("BIZPLC_NM", search)
                            .findAll();

                    if (results.size() <= 0) Log.e(CLASS_NAME, "No data");
                    else {
                        for (HospitalModel model : results) {
                            final int _id = model.get_id();
                            final int number = model.get_id() + 1;
                            final String title = model.getBIZPLC_NM();
                            final String content = model.getLOCPLC_LOTNO_ADDR();
                            final String date = model.getDATA_COLCT_DE();

                            arrayList.add(new ListDataModel(_id, classType, number + "", title, content, date));
                        }

                        updateRecycler();
                    }
                }
                break;
            }

            case ParentModel.TYPE_SNOW: {
                if (search.equals("")) {
                    RealmResults<SnowModel> results = realm.where(SnowModel.class).findAll();
                    if (results.size() <= 0) Log.e(CLASS_NAME, "No data");
                    else {
                        for (SnowModel model : results) {
                            final int _id = model.get_id();
                            final int number = model.get_id() + 1;
                            final String title = model.getCANAME();
                            final String content = model.getADDR();
                            final String date = model.getPROD();

                            arrayList.add(new ListDataModel(_id, classType, number + "", title, content, date));
                        }

                        updateRecycler();
                    }
                } else {
                    RealmResults<SnowModel> results = realm.where(SnowModel.class)
                            .contains("ADDR", search)
                            .or()
                            .contains("CANAME", search)
                            .findAll();

                    if (results.size() <= 0) Log.e(CLASS_NAME, "No data");
                    else {
                        for (SnowModel model : results) {
                            final int _id = model.get_id();
                            final int number = model.get_id() + 1;
                            final String title = model.getCANAME();
                            final String content = model.getADDR();
                            final String date = model.getPROD();

                            arrayList.add(new ListDataModel(_id, classType, number + "", title, content, date));
                        }

                        updateRecycler();
                    }
                }
                break;
            }

            case ParentModel.TYPE_WATER_SUPPLY: {
                if (search.equals("")) {
                    RealmResults<WaterSupplyModel> results = realm.where(WaterSupplyModel.class).findAll();
                    if (results.size() <= 0) Log.e(CLASS_NAME, "No data");
                    else {
                        for (WaterSupplyModel model : results) {
                            final int _id = model.get_id();
                            final int number = model.get_id() + 1;
                            final String title = model.getBIZPLC_NM();
                            final String content = model.getLOCPLC_LOTNO_ADDR();
                            final String date = model.getDATA_COLCT_DE();

                            arrayList.add(new ListDataModel(_id, classType, number + "", title, content, date));
                        }

                        updateRecycler();
                    }
                } else {
                    RealmResults<WaterSupplyModel> results = realm.where(WaterSupplyModel.class)
                            .contains("LOCPLC_LOTNO_ADDR", search)
                            .or()
                            .contains("BIZPLC_NM", search)
                            .findAll();

                    if (results.size() <= 0) Log.e(CLASS_NAME, "No data");
                    else {
                        for (WaterSupplyModel model : results) {
                            final int _id = model.get_id();
                            final int number = model.get_id() + 1;
                            final String title = model.getBIZPLC_NM();
                            final String content = model.getLOCPLC_LOTNO_ADDR();
                            final String date = model.getDATA_COLCT_DE();

                            arrayList.add(new ListDataModel(_id, classType, number + "", title, content, date));
                        }

                        updateRecycler();
                    }
                }
                break;
            }

            default:
                finish();
        }
        realm.close();
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.list_toolbar);
        toolbar.setTitle(what + " 검색");
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_36px);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    public void getIntentData() {
        final Intent intent = getIntent();
        classType = intent.getIntExtra("CLASS_TYPE", -1);
        what = intent.getStringExtra("what");
    }
}
