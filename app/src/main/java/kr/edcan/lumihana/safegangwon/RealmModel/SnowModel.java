package kr.edcan.lumihana.safegangwon.RealmModel;

import io.realm.RealmModel;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by kimok_000 on 2016-09-24.
 */
@RealmClass
public class SnowModel extends RealmObject implements RealmModel {
    @PrimaryKey
    private int _id;

    private String SIDO;
    private String SIGUNGU;
    private String CANAME;
    private String PROD;
    private String MODELN;
    private String CTYPE;
    private String ADDR;
    private String STREAMADDR;

    public SnowModel(){}

    public SnowModel(int _id, String SIDO, String SIGUNGU, String CANAME, String PROD, String MODELN, String CTYPE, String ADDR, String STREAMADDR) {
        this._id = _id;
        this.SIDO = SIDO;
        this.SIGUNGU = SIGUNGU;
        this.CANAME = CANAME;
        this.PROD = PROD;
        this.MODELN = MODELN;
        this.CTYPE = CTYPE;
        this.ADDR = ADDR;
        this.STREAMADDR = STREAMADDR;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getSIDO() {
        return SIDO;
    }

    public void setSIDO(String SIDO) {
        this.SIDO = SIDO;
    }

    public String getSIGUNGU() {
        return SIGUNGU;
    }

    public void setSIGUNGU(String SIGUNGU) {
        this.SIGUNGU = SIGUNGU;
    }

    public String getCANAME() {
        return CANAME;
    }

    public void setCANAME(String CANAME) {
        this.CANAME = CANAME;
    }

    public String getPROD() {
        return PROD;
    }

    public void setPROD(String PROD) {
        this.PROD = PROD;
    }

    public String getMODELN() {
        return MODELN;
    }

    public void setMODELN(String MODELN) {
        this.MODELN = MODELN;
    }

    public String getCTYPE() {
        return CTYPE;
    }

    public void setCTYPE(String CTYPE) {
        this.CTYPE = CTYPE;
    }

    public String getADDR() {
        return ADDR;
    }

    public void setADDR(String ADDR) {
        this.ADDR = ADDR;
    }

    public String getSTREAMADDR() {
        return STREAMADDR;
    }

    public void setSTREAMADDR(String STREAMADDR) {
        this.STREAMADDR = STREAMADDR;
    }
}
