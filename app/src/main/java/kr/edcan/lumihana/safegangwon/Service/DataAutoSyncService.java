package kr.edcan.lumihana.safegangwon.Service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import kr.edcan.lumihana.safegangwon.Util.DBManager;
import kr.edcan.lumihana.safegangwon.Util.GangwonManager;

/**
 * Created by kimok_000 on 2016-09-30.
 */
public class DataAutoSyncService extends Service {
    private DBManager dbManager;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        dbManager = new DBManager(DataAutoSyncService.this);
        dbManager.getSnow(1, 100);
        dbManager.getEarth(GangwonManager.SERVICE_EARTH_YANGYANG, 1, 100);
        dbManager.getCivilDefense(1, 93);
        dbManager.getEmergencyCenter(1, 100);
        dbManager.getHospital(1, 100);
        dbManager.getWaterSupply(1, 100);
        dbManager.getWeatherWithAddress("강원", "속초시", "금호동");
        dbManager.getEmergencyNotice(1, 100);
        dbManager.getGeneralHospital(1, 100);
    }
}
