package kr.edcan.lumihana.safegangwon.LiveModel;

/**
 * Created by kimok_000 on 2016-09-23.
 */
public class ParentModel {
    public static final int TYPE_PARENT = -1;
    public static final int TYPE_WEATHER = 1;
    public static final int TYPE_CIVIL_DEFENSE = 2;
    public static final int TYPE_EARTH = 3;
    public static final int TYPE_EMERGENCY_CENTER = 4;
    public static final int TYPE_EMERGENCY_NOTICE = 5;
    public static final int TYPE_GENERAL_HOSPITAL = 6;
    public static final int TYPE_HOSPITAL = 7;
    public static final int TYPE_SNOW = 8;
    public static final int TYPE_WATER_SUPPLY = 9;

    protected int typeOfModel= TYPE_PARENT;

    public int getType(){
        return this.typeOfModel;
    }
}
