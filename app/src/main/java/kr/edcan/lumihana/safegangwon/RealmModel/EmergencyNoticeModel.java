package kr.edcan.lumihana.safegangwon.RealmModel;

import io.realm.RealmModel;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by kimok_000 on 2016-09-24.
 */
@RealmClass
public class EmergencyNoticeModel extends RealmObject implements RealmModel {
    @PrimaryKey
    private int _id;

    private long msg_id;
    private int msg_seq;
    private int clmy_pttn_cd;
    private String clmy_pttn_nm;
    private String titl;
    private String cnts1;
    private String inpt_date;

    public EmergencyNoticeModel(){}

    public EmergencyNoticeModel(int _id, long msg_id, int msg_seq, int clmy_pttn_cd, String clmy_pttn_nm, String titl, String cnts1, String inpt_date) {
        this._id = _id;
        this.msg_id = msg_id;
        this.msg_seq = msg_seq;
        this.clmy_pttn_cd = clmy_pttn_cd;
        this.clmy_pttn_nm = clmy_pttn_nm;
        this.titl = titl;
        this.cnts1 = cnts1;
        this.inpt_date = inpt_date;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public long getMsg_id() {
        return msg_id;
    }

    public int getMsg_seq() {
        return msg_seq;
    }

    public int getClmy_pttn_cd() {
        return clmy_pttn_cd;
    }

    public String getClmy_pttn_nm() {
        return clmy_pttn_nm;
    }

    public String getTitl() {
        return titl;
    }

    public String getCnts1() {
        return cnts1;
    }

    public String getInpt_date() {
        return inpt_date;
    }

    public void setMsg_id(long msg_id) {
        this.msg_id = msg_id;
    }

    public void setMsg_seq(int msg_seq) {
        this.msg_seq = msg_seq;
    }

    public void setClmy_pttn_cd(int clmy_pttn_cd) {
        this.clmy_pttn_cd = clmy_pttn_cd;
    }

    public void setClmy_pttn_nm(String clmy_pttn_nm) {
        this.clmy_pttn_nm = clmy_pttn_nm;
    }

    public void setTitl(String titl) {
        this.titl = titl;
    }

    public void setCnts1(String cnts1) {
        this.cnts1 = cnts1;
    }

    public void setInpt_date(String inpt_date) {
        this.inpt_date = inpt_date;
    }
}
