package kr.edcan.lumihana.safegangwon.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import kr.edcan.lumihana.safegangwon.R;
import kr.edcan.lumihana.safegangwon.Util.NetworkManager;

public class SignInActivity extends AppCompatActivity {
    private static final String CLASS_NAME = "SiginInActivity";
    private static final String SHAREDPREFERENCE_USER = "User";
    private FrameLayout frame_root;
    private FloatingActionButton floating_next;
    private TextView text_lost, text_register;
    private EditText edit_email, edit_password;
    private FirebaseAuth firebaseAuth;
    private FirebaseAuth.AuthStateListener authStateListener;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sing_in);

        init();
        initSharedPreference();
        initFirebase();
        checkNetwork();
        userCheck();
    }


    private void userCheck() {
        final String email = sharedPreferences.getString("Email", "");
        final String uuid = sharedPreferences.getString("Uid", "");

        if (email.equals("") || uuid.equals("")) {
        }else{
            finish();
        }
    }


    private void initSharedPreference() {
        sharedPreferences = getSharedPreferences(SHAREDPREFERENCE_USER, MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    @Override
    protected void onPause() {
        super.onPause();
        firebaseAuth.removeAuthStateListener(authStateListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkNetwork();
        firebaseAuth.addAuthStateListener(authStateListener);
        userCheck();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        firebaseAuth.removeAuthStateListener(authStateListener);
        firebaseAuth = null;
    }

    @Override
    public void onBackPressed() {
        setResult(RESULT_CANCELED);
        super.onBackPressed();
    }

    private void onSign(final String email, final String password) {
        if (email.toString().trim().equals("") || password.toString().trim().equals("")) {
            Snackbar.make(frame_root, "제대로 입력했는지 다시 확인해주세요", Snackbar.LENGTH_SHORT).show();
            Log.e(CLASS_NAME, "email or password is empty");
            return;
        }

        firebaseAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            final FirebaseUser user = task.getResult().getUser();
                            editor.putString("Email", user.getEmail());
                            editor.putString("Uid", user.getUid());
                            editor.commit();
                            finish();
                            Log.e(CLASS_NAME, "sign in success");
                        } else {
                            Snackbar.make(frame_root, task.getException().getLocalizedMessage(), Snackbar.LENGTH_SHORT).show();
                            Log.e(CLASS_NAME, "sign in failed");
                        }
                    }
                });
    }

    private void initFirebase() {
        firebaseAuth = FirebaseAuth.getInstance();
        authStateListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                final FirebaseUser firebaseUser = firebaseAuth.getCurrentUser();
                if (firebaseUser != null) {
                    Toast.makeText(getApplicationContext(), firebaseUser.getEmail() + "님 환영합니다.", Toast.LENGTH_SHORT).show();
                }
            }
        };
    }

    private boolean checkNetwork() {
        if (NetworkManager.getConnectivityStatus(getApplicationContext()) == NetworkManager.TYPE_NOT_CONNECTED) {
//            final Animation vibrate = AnimationUtils.loadAnimation(this, R.anim.anim_vibrate);
//            floating_next.startAnimation(vibrate);

            Snackbar.make(frame_root, "네트워크에 연결되지 않아 로그인을 건너뜁니다.", Snackbar.LENGTH_LONG)
                    .setAction("건너뛰기", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            setResult(RESULT_CANCELED);
                            finish();
                        }
                    })
                    .show();
            return false;
        } else return true;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        checkNetwork();
        userCheck();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            final String email = data.getStringExtra("Email");
            final String password = data.getStringExtra("Password");
            if (email.equals("") || password.equals("")) return;

            edit_email.setText(email + "");
            edit_password.setText(password + "");
            onSign(email, password);
        }
    }

    private void init() {
        frame_root = (FrameLayout) findViewById(R.id.signin_frame_root);
        text_lost = (TextView) findViewById(R.id.sigin_text_lost);
        text_register = (TextView) findViewById(R.id.sigin_text_register);
        edit_email = (EditText) findViewById(R.id.signin_edit_email);
        edit_password = (EditText) findViewById(R.id.signin_edit_password);
        floating_next = (FloatingActionButton) findViewById(R.id.signin_floating_next);

        text_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivityForResult(intent, 1, null);
            }
        });

        text_lost.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(getApplicationContext(), ForgotActivity.class);
                startActivity(intent);
            }
        });

        floating_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email = edit_email.getText().toString();
                final String password = edit_password.getText().toString();

                onSign(email, password);
            }
        });
    }
}
