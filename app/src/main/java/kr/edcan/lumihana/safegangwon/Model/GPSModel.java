package kr.edcan.lumihana.safegangwon.Model;

import android.location.Address;
import android.location.Location;

import kr.edcan.lumihana.safegangwon.LiveModel.ParentModel;

/**
 * Created by kimok_000 on 2016-09-23.
 */
public class GPSModel extends ParentModel {
    private Location location;
    private Address address;

    public GPSModel(Location location, Address address) {
        this.location = location;
        this.address = address;
    }

    public Location getLocation() {
        return location;
    }

    public Address getAddress() {
        return address;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public void setAddress(Address address) {
        this.address = address;
    }
}
