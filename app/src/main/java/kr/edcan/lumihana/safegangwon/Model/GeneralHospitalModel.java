package kr.edcan.lumihana.safegangwon.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import kr.edcan.lumihana.safegangwon.LiveModel.ParentModel;

/**
 * Created by kimok_000 on 2016-09-23.
 */
public class GeneralHospitalModel extends ParentModel {

    /**
     * list_total_count : 32
     * RESULT : {"CODE":"INFO-000","MESSAGE":"정상 처리되었습니다."}
     * row : [{"NO":1,"SIGUN_CD":"","SIGUN_NM":"","BIZPLC_NM":"속초보광병원","LOCPLC_LOTNO_ADDR":"강원도 속초시 교동  669번지 1호 1호","LOCPLC_ROADNM_ADDR":"강원도 속초시 중앙로 11 (교동)","LICENSG_DE":"20100405","BSN_STATE_NM":"운영중","CLSBIZ_DE":"","SUSPNBIZ_BEGIN_DE":"","SUSPNBIZ_END_DE":"","REOPENBIZ_DE":"","LOCPLC_AR":0,"LOCPLC_ZIP_CD":"","GENRL_AMBLNC_CNT":0,"SPECL_AMBLNC_CNT":0,"RESCUPSN_CNT":0,"SICKBD_CNT":343,"EASING_MEDCARE_CHARGE_DEPT_NM":"","EASING_MEDCARE_APPONT_FORM":"","MEDCARE_INST_ASORTMT_NM":"","MEDSTAF_CNT":106,"HOSPTLRM_CNT":75,"TREAT_SBJECT_CD_INFO":"12과목(내과, 외과, 정형외과, 신경외과, 신경과, 정신건강의학과, 소아청소년과, 마취통증의학과, 영상의학과, 산부인과,진단검사의학과, 비뇨기과","TREAT_SBJECT_CONT":"12과목(내과, 외과, 정형외과, 신경외과, 신경과, 정신건강의학과, 소아청소년과, 마취통증의학과, 영상의학과, 산부인과,진단검사의학과, 비뇨기과","TOT_AR":0,"TOT_PSN_CNT":0,"FIRST_APPONT_DE":"","PERMISN_SICKBD_CNT":0,"WGS84_LOGT":0,"WGS84_LAT":0,"X_CRDNT":0,"Y_CRDNT":0,"DATA_COLCT_DE":"20151208","ETL_LDADNG_DTM":"","LAT":"38.19803714342801","LNG":"128.57835811821286"},{"NO":2,"SIGUN_CD":"","SIGUN_NM":"","BIZPLC_NM":"강릉아산병원","LOCPLC_LOTNO_ADDR":"강원도 강릉시 사천면 방동리 415번지","LOCPLC_ROADNM_ADDR":"강원도 강릉시 사천면 방동길 38","LICENSG_DE":"19960730","BSN_STATE_NM":"운영중","CLSBIZ_DE":"","SUSPNBIZ_BEGIN_DE":"","SUSPNBIZ_END_DE":"","REOPENBIZ_DE":"","LOCPLC_AR":0,"LOCPLC_ZIP_CD":"","GENRL_AMBLNC_CNT":0,"SPECL_AMBLNC_CNT":0,"RESCUPSN_CNT":0,"SICKBD_CNT":751,"EASING_MEDCARE_CHARGE_DEPT_NM":"","EASING_MEDCARE_APPONT_FORM":"","MEDCARE_INST_ASORTMT_NM":"","MEDSTAF_CNT":654,"HOSPTLRM_CNT":205,"TREAT_SBJECT_CD_INFO":"내과, 외과,흉부외과,신경외과,정형외과,성형외과,산부인과,안과,치과,이비인후과,소아청소년과,피부과,비뇨기과,정신건강의학과,신경과,가정의학과,마취통증의학과,영상의학과,진단검사의학과,재활의학과,핵의학과,응급의학과,병리과,방사선종양학과(24개과)","TREAT_SBJECT_CONT":"내과, 외과,흉부외과,신경외과,정형외과,성형외과,산부인과,안과,치과,이비인후과,소아청소년과,피부과,비뇨기과,정신건강의학과,신경과,가정의학과,마취통증의학과,영상의학과,진단검사의학과,재활의학과,핵의학과,응급의학과,병리과,방사선종양학과(24개과)","TOT_AR":0,"TOT_PSN_CNT":0,"FIRST_APPONT_DE":"","PERMISN_SICKBD_CNT":0,"WGS84_LOGT":0,"WGS84_LAT":0,"X_CRDNT":0,"Y_CRDNT":0,"DATA_COLCT_DE":"20151208","ETL_LDADNG_DTM":"","LAT":"37.81835197079703","LNG":"128.85774551626025"},{"NO":3,"SIGUN_CD":"","SIGUN_NM":"","BIZPLC_NM":"의산의료재단 강릉고려병원","LOCPLC_LOTNO_ADDR":"강원도 강릉시 옥천동  286번지 6호 6호","LOCPLC_ROADNM_ADDR":"강원도 강릉시 옥가로 30 (옥천동)","LICENSG_DE":"19910813","BSN_STATE_NM":"운영중","CLSBIZ_DE":"","SUSPNBIZ_BEGIN_DE":"","SUSPNBIZ_END_DE":"","REOPENBIZ_DE":"","LOCPLC_AR":0,"LOCPLC_ZIP_CD":"","GENRL_AMBLNC_CNT":0,"SPECL_AMBLNC_CNT":0,"RESCUPSN_CNT":0,"SICKBD_CNT":238,"EASING_MEDCARE_CHARGE_DEPT_NM":"","EASING_MEDCARE_APPONT_FORM":"","MEDCARE_INST_ASORTMT_NM":"","MEDSTAF_CNT":57,"HOSPTLRM_CNT":63,"TREAT_SBJECT_CD_INFO":"내과,외과,소아청소년과,정형외과,신경외과,마취통증의학과,영상의학과,진단검사의학과,치과, 성형외과(10개과목)","TREAT_SBJECT_CONT":"내과,외과,소아청소년과,정형외과,신경외과,마취통증의학과,영상의학과,진단검사의학과,치과, 성형외과(10개과목)","TOT_AR":0,"TOT_PSN_CNT":0,"FIRST_APPONT_DE":"","PERMISN_SICKBD_CNT":0,"WGS84_LOGT":0,"WGS84_LAT":0,"X_CRDNT":0,"Y_CRDNT":0,"DATA_COLCT_DE":"20151208","ETL_LDADNG_DTM":"","LAT":"37.75901021462088","LNG":"128.89986321707713"},{"NO":4,"SIGUN_CD":"","SIGUN_NM":"","BIZPLC_NM":"홍천아산병원","LOCPLC_LOTNO_ADDR":"강원도 홍천군 홍천읍 갈마곡리 466번지 1호 1호","LOCPLC_ROADNM_ADDR":"강원도 홍천군 홍천읍 산림공원1길 17","LICENSG_DE":"19891106","BSN_STATE_NM":"운영중","CLSBIZ_DE":"","SUSPNBIZ_BEGIN_DE":"","SUSPNBIZ_END_DE":"","REOPENBIZ_DE":"","LOCPLC_AR":0,"LOCPLC_ZIP_CD":"","GENRL_AMBLNC_CNT":0,"SPECL_AMBLNC_CNT":0,"RESCUPSN_CNT":0,"SICKBD_CNT":153,"EASING_MEDCARE_CHARGE_DEPT_NM":"","EASING_MEDCARE_APPONT_FORM":"","MEDCARE_INST_ASORTMT_NM":"","MEDSTAF_CNT":56,"HOSPTLRM_CNT":38,"TREAT_SBJECT_CD_INFO":"내과, 외과, 재활의학과, 소아청소년과, 정형외과, 영상의학과, 진단검사의학과, 마취통증의학과, 신경외과, 치과","TREAT_SBJECT_CONT":"내과, 외과, 재활의학과, 소아청소년과, 정형외과, 영상의학과, 진단검사의학과, 마취통증의학과, 신경외과, 치과","TOT_AR":0,"TOT_PSN_CNT":0,"FIRST_APPONT_DE":"","PERMISN_SICKBD_CNT":0,"WGS84_LOGT":0,"WGS84_LAT":0,"X_CRDNT":0,"Y_CRDNT":0,"DATA_COLCT_DE":"20151208","ETL_LDADNG_DTM":"","LAT":"37.691105134171536","LNG":"127.89543245792154"},{"NO":5,"SIGUN_CD":"","SIGUN_NM":"","BIZPLC_NM":"한림대학교부속 춘천성심병원","LOCPLC_LOTNO_ADDR":"강원도 춘천시 교동  153번지","LOCPLC_ROADNM_ADDR":"강원도 춘천시 삭주로 77 (교동)","LICENSG_DE":"19841210","BSN_STATE_NM":"운영중","CLSBIZ_DE":"","SUSPNBIZ_BEGIN_DE":"","SUSPNBIZ_END_DE":"","REOPENBIZ_DE":"","LOCPLC_AR":0,"LOCPLC_ZIP_CD":"","GENRL_AMBLNC_CNT":0,"SPECL_AMBLNC_CNT":0,"RESCUPSN_CNT":0,"SICKBD_CNT":455,"EASING_MEDCARE_CHARGE_DEPT_NM":"","EASING_MEDCARE_APPONT_FORM":"","MEDCARE_INST_ASORTMT_NM":"의사","MEDSTAF_CNT":495,"HOSPTLRM_CNT":94,"TREAT_SBJECT_CD_INFO":"외과, 내과, 소아청소년과, 산부인과, 정형외과, 신경외과, 정신과, 안과, 성형외과, 이비인후과, 비뇨기과, 마취통증의학과, 병리과, 치과, 영상의학과, 재활의학과, 진단검사의학과, 신경과, 흉부외과, 응급의학과, 가정의학과, 피부과, 방사선종양학과(23개과)","TREAT_SBJECT_CONT":"외과, 내과, 소아청소년과, 산부인과, 정형외과, 신경외과, 정신과, 안과, 성형외과, 이비인후과, 비뇨기과, 마취통증의학과, 병리과, 치과, 영상의학과, 재활의학과, 진단검사의학과, 신경과, 흉부외과, 응급의학과, 가정의학과, 피부과, 방사선종양학과(23개과)","TOT_AR":0,"TOT_PSN_CNT":0,"FIRST_APPONT_DE":"","PERMISN_SICKBD_CNT":0,"WGS84_LOGT":0,"WGS84_LAT":0,"X_CRDNT":0,"Y_CRDNT":0,"DATA_COLCT_DE":"20151208","ETL_LDADNG_DTM":"","LAT":"37.88376397643148","LNG":"127.73929380850946"}]
     */

    @SerializedName("localdata-health_medica-general_hospital")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private int list_total_count;
        /**
         * CODE : INFO-000
         * MESSAGE : 정상 처리되었습니다.
         */

        @SerializedName("RESULT")
        private RESULTBean RESULT;
        /**
         * NO : 1
         * SIGUN_CD :
         * SIGUN_NM :
         * BIZPLC_NM : 속초보광병원
         * LOCPLC_LOTNO_ADDR : 강원도 속초시 교동  669번지 1호 1호
         * LOCPLC_ROADNM_ADDR : 강원도 속초시 중앙로 11 (교동)
         * LICENSG_DE : 20100405
         * BSN_STATE_NM : 운영중
         * CLSBIZ_DE :
         * SUSPNBIZ_BEGIN_DE :
         * SUSPNBIZ_END_DE :
         * REOPENBIZ_DE :
         * LOCPLC_AR : 0
         * LOCPLC_ZIP_CD :
         * GENRL_AMBLNC_CNT : 0
         * SPECL_AMBLNC_CNT : 0
         * RESCUPSN_CNT : 0
         * SICKBD_CNT : 343
         * EASING_MEDCARE_CHARGE_DEPT_NM :
         * EASING_MEDCARE_APPONT_FORM :
         * MEDCARE_INST_ASORTMT_NM :
         * MEDSTAF_CNT : 106
         * HOSPTLRM_CNT : 75
         * TREAT_SBJECT_CD_INFO : 12과목(내과, 외과, 정형외과, 신경외과, 신경과, 정신건강의학과, 소아청소년과, 마취통증의학과, 영상의학과, 산부인과,진단검사의학과, 비뇨기과
         * TREAT_SBJECT_CONT : 12과목(내과, 외과, 정형외과, 신경외과, 신경과, 정신건강의학과, 소아청소년과, 마취통증의학과, 영상의학과, 산부인과,진단검사의학과, 비뇨기과
         * TOT_AR : 0
         * TOT_PSN_CNT : 0
         * FIRST_APPONT_DE :
         * PERMISN_SICKBD_CNT : 0
         * WGS84_LOGT : 0
         * WGS84_LAT : 0
         * X_CRDNT : 0
         * Y_CRDNT : 0
         * DATA_COLCT_DE : 20151208
         * ETL_LDADNG_DTM :
         * LAT : 38.19803714342801
         * LNG : 128.57835811821286
         */

        @SerializedName("row")
        private List<RowBean> row;

        public int getList_total_count() {
            return list_total_count;
        }

        public void setList_total_count(int list_total_count) {
            this.list_total_count = list_total_count;
        }

        public RESULTBean getRESULT() {
            return RESULT;
        }

        public void setRESULT(RESULTBean RESULT) {
            this.RESULT = RESULT;
        }

        public List<RowBean> getRow() {
            return row;
        }

        public void setRow(List<RowBean> row) {
            this.row = row;
        }

        public static class RESULTBean {
            @SerializedName("CODE")
            private String CODE;
            @SerializedName("MESSAGE")
            private String MESSAGE;

            public String getCODE() {
                return CODE;
            }

            public void setCODE(String CODE) {
                this.CODE = CODE;
            }

            public String getMESSAGE() {
                return MESSAGE;
            }

            public void setMESSAGE(String MESSAGE) {
                this.MESSAGE = MESSAGE;
            }
        }

        public static class RowBean {
            @SerializedName("NO")
            private int NO;
            @SerializedName("SIGUN_CD")
            private String SIGUN_CD;
            @SerializedName("SIGUN_NM")
            private String SIGUN_NM;
            @SerializedName("BIZPLC_NM")
            private String BIZPLC_NM;
            @SerializedName("LOCPLC_LOTNO_ADDR")
            private String LOCPLC_LOTNO_ADDR;
            @SerializedName("LOCPLC_ROADNM_ADDR")
            private String LOCPLC_ROADNM_ADDR;
            @SerializedName("LICENSG_DE")
            private String LICENSG_DE;
            @SerializedName("BSN_STATE_NM")
            private String BSN_STATE_NM;
            @SerializedName("CLSBIZ_DE")
            private String CLSBIZ_DE;
            @SerializedName("SUSPNBIZ_BEGIN_DE")
            private String SUSPNBIZ_BEGIN_DE;
            @SerializedName("SUSPNBIZ_END_DE")
            private String SUSPNBIZ_END_DE;
            @SerializedName("REOPENBIZ_DE")
            private String REOPENBIZ_DE;
            @SerializedName("LOCPLC_AR")
            private int LOCPLC_AR;
            @SerializedName("LOCPLC_ZIP_CD")
            private String LOCPLC_ZIP_CD;
            @SerializedName("GENRL_AMBLNC_CNT")
            private int GENRL_AMBLNC_CNT;
            @SerializedName("SPECL_AMBLNC_CNT")
            private int SPECL_AMBLNC_CNT;
            @SerializedName("RESCUPSN_CNT")
            private int RESCUPSN_CNT;
            @SerializedName("SICKBD_CNT")
            private int SICKBD_CNT;
            @SerializedName("EASING_MEDCARE_CHARGE_DEPT_NM")
            private String EASING_MEDCARE_CHARGE_DEPT_NM;
            @SerializedName("EASING_MEDCARE_APPONT_FORM")
            private String EASING_MEDCARE_APPONT_FORM;
            @SerializedName("MEDCARE_INST_ASORTMT_NM")
            private String MEDCARE_INST_ASORTMT_NM;
            @SerializedName("MEDSTAF_CNT")
            private int MEDSTAF_CNT;
            @SerializedName("HOSPTLRM_CNT")
            private int HOSPTLRM_CNT;
            @SerializedName("TREAT_SBJECT_CD_INFO")
            private String TREAT_SBJECT_CD_INFO;
            @SerializedName("TREAT_SBJECT_CONT")
            private String TREAT_SBJECT_CONT;
            @SerializedName("TOT_AR")
            private float TOT_AR;
            @SerializedName("TOT_PSN_CNT")
            private int TOT_PSN_CNT;
            @SerializedName("FIRST_APPONT_DE")
            private String FIRST_APPONT_DE;
            @SerializedName("PERMISN_SICKBD_CNT")
            private int PERMISN_SICKBD_CNT;
            @SerializedName("WGS84_LOGT")
            private int WGS84_LOGT;
            @SerializedName("WGS84_LAT")
            private int WGS84_LAT;
            @SerializedName("X_CRDNT")
            private int X_CRDNT;
            @SerializedName("Y_CRDNT")
            private int Y_CRDNT;
            @SerializedName("DATA_COLCT_DE")
            private String DATA_COLCT_DE;
            @SerializedName("ETL_LDADNG_DTM")
            private String ETL_LDADNG_DTM;
            @SerializedName("LAT")
            private String LAT;
            @SerializedName("LNG")
            private String LNG;

            public int getNO() {
                return NO;
            }

            public void setNO(int NO) {
                this.NO = NO;
            }

            public String getSIGUN_CD() {
                return SIGUN_CD;
            }

            public void setSIGUN_CD(String SIGUN_CD) {
                this.SIGUN_CD = SIGUN_CD;
            }

            public String getSIGUN_NM() {
                return SIGUN_NM;
            }

            public void setSIGUN_NM(String SIGUN_NM) {
                this.SIGUN_NM = SIGUN_NM;
            }

            public String getBIZPLC_NM() {
                return BIZPLC_NM;
            }

            public void setBIZPLC_NM(String BIZPLC_NM) {
                this.BIZPLC_NM = BIZPLC_NM;
            }

            public String getLOCPLC_LOTNO_ADDR() {
                return LOCPLC_LOTNO_ADDR;
            }

            public void setLOCPLC_LOTNO_ADDR(String LOCPLC_LOTNO_ADDR) {
                this.LOCPLC_LOTNO_ADDR = LOCPLC_LOTNO_ADDR;
            }

            public String getLOCPLC_ROADNM_ADDR() {
                return LOCPLC_ROADNM_ADDR;
            }

            public void setLOCPLC_ROADNM_ADDR(String LOCPLC_ROADNM_ADDR) {
                this.LOCPLC_ROADNM_ADDR = LOCPLC_ROADNM_ADDR;
            }

            public String getLICENSG_DE() {
                return LICENSG_DE;
            }

            public void setLICENSG_DE(String LICENSG_DE) {
                this.LICENSG_DE = LICENSG_DE;
            }

            public String getBSN_STATE_NM() {
                return BSN_STATE_NM;
            }

            public void setBSN_STATE_NM(String BSN_STATE_NM) {
                this.BSN_STATE_NM = BSN_STATE_NM;
            }

            public String getCLSBIZ_DE() {
                return CLSBIZ_DE;
            }

            public void setCLSBIZ_DE(String CLSBIZ_DE) {
                this.CLSBIZ_DE = CLSBIZ_DE;
            }

            public String getSUSPNBIZ_BEGIN_DE() {
                return SUSPNBIZ_BEGIN_DE;
            }

            public void setSUSPNBIZ_BEGIN_DE(String SUSPNBIZ_BEGIN_DE) {
                this.SUSPNBIZ_BEGIN_DE = SUSPNBIZ_BEGIN_DE;
            }

            public String getSUSPNBIZ_END_DE() {
                return SUSPNBIZ_END_DE;
            }

            public void setSUSPNBIZ_END_DE(String SUSPNBIZ_END_DE) {
                this.SUSPNBIZ_END_DE = SUSPNBIZ_END_DE;
            }

            public String getREOPENBIZ_DE() {
                return REOPENBIZ_DE;
            }

            public void setREOPENBIZ_DE(String REOPENBIZ_DE) {
                this.REOPENBIZ_DE = REOPENBIZ_DE;
            }

            public int getLOCPLC_AR() {
                return LOCPLC_AR;
            }

            public void setLOCPLC_AR(int LOCPLC_AR) {
                this.LOCPLC_AR = LOCPLC_AR;
            }

            public String getLOCPLC_ZIP_CD() {
                return LOCPLC_ZIP_CD;
            }

            public void setLOCPLC_ZIP_CD(String LOCPLC_ZIP_CD) {
                this.LOCPLC_ZIP_CD = LOCPLC_ZIP_CD;
            }

            public int getGENRL_AMBLNC_CNT() {
                return GENRL_AMBLNC_CNT;
            }

            public void setGENRL_AMBLNC_CNT(int GENRL_AMBLNC_CNT) {
                this.GENRL_AMBLNC_CNT = GENRL_AMBLNC_CNT;
            }

            public int getSPECL_AMBLNC_CNT() {
                return SPECL_AMBLNC_CNT;
            }

            public void setSPECL_AMBLNC_CNT(int SPECL_AMBLNC_CNT) {
                this.SPECL_AMBLNC_CNT = SPECL_AMBLNC_CNT;
            }

            public int getRESCUPSN_CNT() {
                return RESCUPSN_CNT;
            }

            public void setRESCUPSN_CNT(int RESCUPSN_CNT) {
                this.RESCUPSN_CNT = RESCUPSN_CNT;
            }

            public int getSICKBD_CNT() {
                return SICKBD_CNT;
            }

            public void setSICKBD_CNT(int SICKBD_CNT) {
                this.SICKBD_CNT = SICKBD_CNT;
            }

            public String getEASING_MEDCARE_CHARGE_DEPT_NM() {
                return EASING_MEDCARE_CHARGE_DEPT_NM;
            }

            public void setEASING_MEDCARE_CHARGE_DEPT_NM(String EASING_MEDCARE_CHARGE_DEPT_NM) {
                this.EASING_MEDCARE_CHARGE_DEPT_NM = EASING_MEDCARE_CHARGE_DEPT_NM;
            }

            public String getEASING_MEDCARE_APPONT_FORM() {
                return EASING_MEDCARE_APPONT_FORM;
            }

            public void setEASING_MEDCARE_APPONT_FORM(String EASING_MEDCARE_APPONT_FORM) {
                this.EASING_MEDCARE_APPONT_FORM = EASING_MEDCARE_APPONT_FORM;
            }

            public String getMEDCARE_INST_ASORTMT_NM() {
                return MEDCARE_INST_ASORTMT_NM;
            }

            public void setMEDCARE_INST_ASORTMT_NM(String MEDCARE_INST_ASORTMT_NM) {
                this.MEDCARE_INST_ASORTMT_NM = MEDCARE_INST_ASORTMT_NM;
            }

            public int getMEDSTAF_CNT() {
                return MEDSTAF_CNT;
            }

            public void setMEDSTAF_CNT(int MEDSTAF_CNT) {
                this.MEDSTAF_CNT = MEDSTAF_CNT;
            }

            public int getHOSPTLRM_CNT() {
                return HOSPTLRM_CNT;
            }

            public void setHOSPTLRM_CNT(int HOSPTLRM_CNT) {
                this.HOSPTLRM_CNT = HOSPTLRM_CNT;
            }

            public String getTREAT_SBJECT_CD_INFO() {
                return TREAT_SBJECT_CD_INFO;
            }

            public void setTREAT_SBJECT_CD_INFO(String TREAT_SBJECT_CD_INFO) {
                this.TREAT_SBJECT_CD_INFO = TREAT_SBJECT_CD_INFO;
            }

            public String getTREAT_SBJECT_CONT() {
                return TREAT_SBJECT_CONT;
            }

            public void setTREAT_SBJECT_CONT(String TREAT_SBJECT_CONT) {
                this.TREAT_SBJECT_CONT = TREAT_SBJECT_CONT;
            }

            public float getTOT_AR() {
                return TOT_AR;
            }

            public void setTOT_AR(float TOT_AR) {
                this.TOT_AR = TOT_AR;
            }

            public int getTOT_PSN_CNT() {
                return TOT_PSN_CNT;
            }

            public void setTOT_PSN_CNT(int TOT_PSN_CNT) {
                this.TOT_PSN_CNT = TOT_PSN_CNT;
            }

            public String getFIRST_APPONT_DE() {
                return FIRST_APPONT_DE;
            }

            public void setFIRST_APPONT_DE(String FIRST_APPONT_DE) {
                this.FIRST_APPONT_DE = FIRST_APPONT_DE;
            }

            public int getPERMISN_SICKBD_CNT() {
                return PERMISN_SICKBD_CNT;
            }

            public void setPERMISN_SICKBD_CNT(int PERMISN_SICKBD_CNT) {
                this.PERMISN_SICKBD_CNT = PERMISN_SICKBD_CNT;
            }

            public int getWGS84_LOGT() {
                return WGS84_LOGT;
            }

            public void setWGS84_LOGT(int WGS84_LOGT) {
                this.WGS84_LOGT = WGS84_LOGT;
            }

            public int getWGS84_LAT() {
                return WGS84_LAT;
            }

            public void setWGS84_LAT(int WGS84_LAT) {
                this.WGS84_LAT = WGS84_LAT;
            }

            public int getX_CRDNT() {
                return X_CRDNT;
            }

            public void setX_CRDNT(int X_CRDNT) {
                this.X_CRDNT = X_CRDNT;
            }

            public int getY_CRDNT() {
                return Y_CRDNT;
            }

            public void setY_CRDNT(int Y_CRDNT) {
                this.Y_CRDNT = Y_CRDNT;
            }

            public String getDATA_COLCT_DE() {
                return DATA_COLCT_DE;
            }

            public void setDATA_COLCT_DE(String DATA_COLCT_DE) {
                this.DATA_COLCT_DE = DATA_COLCT_DE;
            }

            public String getETL_LDADNG_DTM() {
                return ETL_LDADNG_DTM;
            }

            public void setETL_LDADNG_DTM(String ETL_LDADNG_DTM) {
                this.ETL_LDADNG_DTM = ETL_LDADNG_DTM;
            }

            public String getLAT() {
                return LAT;
            }

            public void setLAT(String LAT) {
                this.LAT = LAT;
            }

            public String getLNG() {
                return LNG;
            }

            public void setLNG(String LNG) {
                this.LNG = LNG;
            }
        }
    }
}
