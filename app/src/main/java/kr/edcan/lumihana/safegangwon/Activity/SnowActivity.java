package kr.edcan.lumihana.safegangwon.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.MediaController;
import android.widget.VideoView;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;
import kr.edcan.lumihana.safegangwon.Adapter.DataAdapter;
import kr.edcan.lumihana.safegangwon.Model.DataModel;
import kr.edcan.lumihana.safegangwon.R;
import kr.edcan.lumihana.safegangwon.RealmModel.SnowModel;
import kr.edcan.lumihana.safegangwon.Util.NetworkManager;

public class SnowActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private VideoView video_cctv;
    private ArrayList<DataModel> arrayList;
    private LinearLayoutManager linearLayoutManager;
    private RecyclerView recycler_data;
    private DataAdapter dataAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snow);


        initToolbar();
        initRecycler();
        fillRecycler();
        checkNetwork();
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.snow_toolbar);
        toolbar.setTitle("적설 감시");
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_36px);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void fillRecycler() {
        final Intent intent = getIntent();
        final int _id = intent.getIntExtra("_id", -1);

        if (_id >= 0) {
            Realm realm = Realm.getDefaultInstance();
            RealmResults<SnowModel> results = realm.where(SnowModel.class).equalTo("_id", _id).findAll();

            SnowModel model;
            if (results.size() <= 0) model = null;
            else model = results.get(0);

            if (model != null) {
                final String sido = getModelValue(model.getSIDO());
                final String sigungu = getModelValue(model.getSIGUNGU());
                final String caname = getModelValue(model.getCANAME());
                final String prod = getModelValue(model.getPROD());
                final String modeln = getModelValue(model.getMODELN());
                final String ctype = getModelValue(model.getCTYPE());
                final String addr = getModelValue(model.getADDR());
                final String streamaddr = getModelValue(model.getSTREAMADDR());

                if (!sido.equals("")) arrayList.add(new DataModel("시/도", "강원도"));
                if (!sigungu.equals("")) arrayList.add(new DataModel("시/군/구", sigungu));
                if (!caname.equals("")) arrayList.add(new DataModel("설치 위치", caname));
                if (!prod.equals("")) arrayList.add(new DataModel("공급사", prod));
                if (!modeln.equals("")) arrayList.add(new DataModel("모델 넘버", modeln));
                if (!ctype.equals("")) arrayList.add(new DataModel("설치 목적", ctype));
                if (!addr.equals("")) arrayList.add(new DataModel("설치 주소", addr));
                if (!streamaddr.equals("")) initVideo(streamaddr);
            } else {

            }
            updateRecycler();
        }
    }

    private String getModelValue(String original) {
        if (original == null) return "";
        else return original.toString().trim();
    }


    private void updateRecycler() {
        dataAdapter.notifyDataSetChanged();
    }

    private void initRecycler() {
        arrayList = new ArrayList<>();

        linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dataAdapter = new DataAdapter(getApplicationContext(), arrayList);

        recycler_data = (RecyclerView) findViewById(R.id.snow_recycler_data);
        recycler_data.setLayoutManager(linearLayoutManager);
        recycler_data.setAdapter(dataAdapter);
    }

    private void initVideo(String streamAddr) {
        video_cctv = (VideoView) findViewById(R.id.snow_video_cctv);

        final MediaController mediaController = new MediaController(this);
        mediaController.setEnabled(false);

        video_cctv.setVideoPath(streamAddr);
        video_cctv.setMediaController(mediaController);
    }

    private boolean checkNetwork() {
        if (NetworkManager.getConnectivityStatus(getApplicationContext()) == NetworkManager.TYPE_NOT_CONNECTED) {

            Snackbar.make(video_cctv, "네트워크에 연결해주세요.", Snackbar.LENGTH_LONG)
                    .setAction("연결", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final Intent intent = new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    })
                    .show();
            return false;
        } else return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkNetwork();
    }

    @Override
    protected void onStart() {
        super.onStart();
        checkNetwork();
    }
}
