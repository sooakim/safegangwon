package kr.edcan.lumihana.safegangwon.Util;

import android.content.Context;
import android.util.Log;

import kr.edcan.lumihana.safegangwon.Model.CivilDefenseModel;
import kr.edcan.lumihana.safegangwon.Model.EarthModel;
import kr.edcan.lumihana.safegangwon.Model.EmergencyCenterModel;
import kr.edcan.lumihana.safegangwon.Model.GeneralHospitalModel;
import kr.edcan.lumihana.safegangwon.Model.HospitalModel;
import kr.edcan.lumihana.safegangwon.Model.SnowModel;
import kr.edcan.lumihana.safegangwon.Model.WaterSupplyModel;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by kimok_000 on 2016-09-22.
 */
public class GangwonManager {
    private static final String CLASS_NAME= "GangwonManager";
    private String API_KEY;
    private Retrofit retrofit;
    private IGangwonService gangwonService;
    private Context context;
    private Call<SnowModel> snow;
    private Call<EarthModel> earth;
    private Call<CivilDefenseModel> civilDefense;
    private Call<WaterSupplyModel> waterSupply;
    private Call<GeneralHospitalModel> generalHospital;
    private Call<EmergencyCenterModel> emergencyCenter;
    private Call<HospitalModel> hospital;
    private OnGangwonChangedListener onGangwonChangedListener;

    public static final String TYPE_XML = "xml";
    public static final String TYPE_XML_FILE = "xmlf";
    public static final String TYPE_XLS = "xls";
    public static final String TYPE_JSON = "json";

    private static final String SERVICE = "gwcgcom";
    private static final String SERVICE_SNOW = "-disaster_snow_image";
    private static final String SERVICE_EARTH = "-earthquake_monitor";
    private static final String SERVICE_LOCAL_HEALTH = "localdata-health_medica";

    public static final String SERVICE_SNOW_GANGWON = SERVICE + SERVICE_SNOW;
    public static final String SERVICE_SNOW_YANGYANG = SERVICE + SERVICE_SNOW + "_yangyang";
    public static final String SERVICE_SNOW_GOSEONG = SERVICE + SERVICE_SNOW + "_goseong";
    public static final String SERVICE_SNOW_INJE = SERVICE + SERVICE_SNOW + "_inje";
    public static final String SERVICE_SNOW_YANGGU = SERVICE + SERVICE_SNOW + "_yanggu";
    public static final String SERVICE_SNOW_HWACHEON = SERVICE + SERVICE_SNOW + "_hwacheon";
    public static final String SERVICE_SNOW_CHEORWON = SERVICE + SERVICE_SNOW + "_cheorwon";
    public static final String SERVICE_SNOW_JEONGSEON = SERVICE + SERVICE_SNOW + "_jeongseon";
    public static final String SERVICE_SNOW_PYEONGCHANG = SERVICE + SERVICE_SNOW + "_pyeongchang";
    public static final String SERVICE_SNOW_YEONGWOL = SERVICE + SERVICE_SNOW + "_yeongwol";
    public static final String SERVICE_SNOW_HOENGSEONG = SERVICE + SERVICE_SNOW + "_hoengseong";
    public static final String SERVICE_SNOW_HONGCHEON = SERVICE + SERVICE_SNOW + "_hongcheon";
    public static final String SERVICE_SNOW_SAMCHEOK = SERVICE + SERVICE_SNOW + "_samcheok";
    public static final String SERVICE_SNOW_SOKCHO = SERVICE + SERVICE_SNOW + "_sokcho";
    public static final String SERVICE_SNOW_TAEBAEK = SERVICE + SERVICE_SNOW + "_taebaek";
    public static final String SERVICE_SNOW_DONGHAE = SERVICE + SERVICE_SNOW + "_donghae";
    public static final String SERVICE_SNOW_GANGNEUNG = SERVICE + SERVICE_SNOW + "_gangneung";
    public static final String SERVICE_SNOW_WONJU = SERVICE + SERVICE_SNOW + "_wonju";
    public static final String SERVICE_SNOW_CHUNCHEON = SERVICE + SERVICE_SNOW + "_chuncheon";

    public static final String SERVICE_EARTH_YANGYANG = SERVICE + SERVICE_EARTH + "-yangyang";
    public static final String SERVICE_EARTH_GOSEONG = SERVICE + SERVICE_EARTH + "-goseong";
    public static final String SERVICE_EARTH_INJE = SERVICE + SERVICE_EARTH + "-inje";
    public static final String SERVICE_EARTH_YANGGU = SERVICE + SERVICE_EARTH + "-yanggu";
    public static final String SERVICE_EARTH_HWACHEON = SERVICE + SERVICE_EARTH + "-hwacheon";
    public static final String SERVICE_EARTH_CHEORWON = SERVICE + SERVICE_EARTH + "-cheorwon";
    public static final String SERVICE_EARTH_JEONGSEON = SERVICE + SERVICE_EARTH + "-jeongseon";
    public static final String SERVICE_EARTH_PYEONGCHANG = SERVICE + SERVICE_EARTH + "-pyeongchang";
    public static final String SERVICE_EARTH_HOENGSEONG = SERVICE + SERVICE_EARTH + "-hoengseong";
    public static final String SERVICE_EARTH_SAMCHEOK = SERVICE + SERVICE_EARTH + "-samcheok";
    public static final String SERVICE_EARTH_SOKCHO = SERVICE + SERVICE_EARTH + "-sokcho";
    public static final String SERVICE_EARTH_TAEBAEK = SERVICE + SERVICE_EARTH + "-taebaek";
    public static final String SERVICE_EARTH_DONGHAE = SERVICE + SERVICE_EARTH + "-donghae";
    public static final String SERVICE_EARTH_GANGNEUNG = SERVICE + SERVICE_EARTH + "-gangneung";
    public static final String SERVICE_EARTH_WONJU = SERVICE + SERVICE_EARTH + "-wonju";

    public static final String SERVICE_CIVIL_DEFENSE = SERVICE_LOCAL_HEALTH + "-civil_defense_evacuate_facility";
    public static final String SERVICE_WATER_SUPPLY = SERVICE_LOCAL_HEALTH + "-civil_defense_water_supply_facility";
    public static final String SERVICE_GENERAL_HOSPITAL = SERVICE_LOCAL_HEALTH + "-general_hospital";
    public static final String SERVICE_EMERGENCY_CENTER = SERVICE_LOCAL_HEALTH + "-emergency_medical_care_center";
    public static final String SERVICE_HOSPITAL = SERVICE_LOCAL_HEALTH + "-hospital";

    public GangwonManager(Context context, String apiKey) {
        this.API_KEY = apiKey;
        this.context = context;

        initRetrofit();
    }

    private void initRetrofit() {
        retrofit = new Retrofit.Builder()
                .baseUrl("http://data.gwd.go.kr/apiservice/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        gangwonService = retrofit.create(IGangwonService.class);
    }

    public void setOnGangwonChangedListener(OnGangwonChangedListener onGangwonChangedListener) {
        this.onGangwonChangedListener = onGangwonChangedListener;
    }

    public void getSnow(String service, int startIndex, int endIndex) {
        snow = gangwonService.snow(API_KEY, TYPE_JSON, service, startIndex, endIndex);
        snow.enqueue(new Callback<SnowModel>() {
            @Override
            public void onResponse(Response<SnowModel> response, Retrofit retrofit) {
                if (response.code() == 200) {
                    onGangwonChangedListener.onSnowChanged(response.body());
                } else Log.e(CLASS_NAME, "Gangwon Server return !200");
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e(CLASS_NAME, "Failed to get snowInfo");
                Log.e(CLASS_NAME, t.getMessage() + "");
            }
        });
    }

    public void getEarth(String service, int startIndex, int endIndex) {
        earth = gangwonService.earth(API_KEY, TYPE_JSON, service, startIndex, endIndex);
        earth.enqueue(new Callback<EarthModel>() {
            @Override
            public void onResponse(Response<EarthModel> response, Retrofit retrofit) {
                if (response.code() == 200) {
                    onGangwonChangedListener.onEarthChanged(response.body());
                } else Log.e(CLASS_NAME, "Gangwon Server return !200");
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e(CLASS_NAME, "Failed to get earthInfo");
                Log.e(CLASS_NAME, t.getMessage() + "");
            }
        });
    }

    public void getCivilDefense(String service, int startIndex, int endIndex) {
        civilDefense = gangwonService.civilDefense(API_KEY, TYPE_JSON, service, startIndex, endIndex);
        civilDefense.enqueue(new Callback<CivilDefenseModel>() {
            @Override
            public void onResponse(Response<CivilDefenseModel> response, Retrofit retrofit) {
                if (response.code() == 200) {
                    onGangwonChangedListener.onCivilDefenseChanged(response.body());
                } else Log.e(CLASS_NAME, "Gangwon Server return !200");
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e(CLASS_NAME, "Failed to get civilDefenseInfo");
                Log.e(CLASS_NAME, t.getMessage() + "");
            }
        });
    }

    public void getWaterSupply(String service, int startIndex, int endIndex) {
        waterSupply = gangwonService.waterSupply(API_KEY, TYPE_JSON, service, startIndex, endIndex);
        waterSupply.enqueue(new Callback<WaterSupplyModel>() {
            @Override
            public void onResponse(Response<WaterSupplyModel> response, Retrofit retrofit) {
                if (response.code() == 200) {
                    onGangwonChangedListener.onWaterSupplyChanged(response.body());
                } else Log.e(CLASS_NAME, "Gangwon Server return !200");
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e(CLASS_NAME, "Failed to get waterSupplyInfo");
                Log.e(CLASS_NAME, t.getMessage() + "");
            }
        });
    }

    public void getEmergencyCenter(String service, int startIndex, int endIndex) {
        emergencyCenter = gangwonService.emergencyCenter(API_KEY, TYPE_JSON, service, startIndex, endIndex);
        emergencyCenter.enqueue(new Callback<EmergencyCenterModel>() {
            @Override
            public void onResponse(Response<EmergencyCenterModel> response, Retrofit retrofit) {
                if (response.code() == 200) {
                    onGangwonChangedListener.onEmergencyCenterChanged(response.body());
                } else Log.e(CLASS_NAME, "Gangwon Server return !200");
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e(CLASS_NAME, "Failed to get EmergencyCenterInfo");
                Log.e(CLASS_NAME, t.getMessage() + "");
            }
        });
    }

    public void getHospital(String service, int startIndex, int endIndex) {
        hospital = gangwonService.hospital(API_KEY, TYPE_JSON, service, startIndex, endIndex);
        hospital.enqueue(new Callback<HospitalModel>() {
            @Override
            public void onResponse(Response<HospitalModel> response, Retrofit retrofit) {
                if (response.code() == 200) {
                    onGangwonChangedListener.onHospitalChanged(response.body());
                } else Log.e(CLASS_NAME, "Gangwon Server return !200");
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e(CLASS_NAME, "Failed to get hospitalInfo");
                Log.e(CLASS_NAME, t.getMessage() + "");
            }
        });
    }

    public void getGeneralHospital(String service, int startIndex, int endIndex){
        generalHospital = gangwonService.generalHospital(API_KEY, TYPE_JSON, service, startIndex, endIndex);
        generalHospital.enqueue(new Callback<GeneralHospitalModel>() {
            @Override
            public void onResponse(Response<GeneralHospitalModel> response, Retrofit retrofit) {
                if (response.code() == 200) {
                    onGangwonChangedListener.onGeneralHospitalChanged(response.body());
                } else Log.e(CLASS_NAME, "Gangwon Server return !200");
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e(CLASS_NAME, "Failed to get generalHospitalInfo");
                Log.e(CLASS_NAME, t.getMessage() + "");
            }
        });
    }

    public interface OnGangwonChangedListener {
        void onSnowChanged(SnowModel snowModel);

        void onEarthChanged(EarthModel earthModel);

        void onCivilDefenseChanged(CivilDefenseModel civilDefenseModel);

        void onWaterSupplyChanged(WaterSupplyModel waterSupplyModel);

        void onGeneralHospitalChanged(GeneralHospitalModel generalHospitalModel);

        void onEmergencyCenterChanged(EmergencyCenterModel emergencyCenterModel);

        void onHospitalChanged(HospitalModel hospitalModel);
    }
}
