package kr.edcan.lumihana.safegangwon.LiveModel;

/**
 * Created by kimok_000 on 2016-09-28.
 */
public class LiveEarthModel extends ParentModel {
    private String obs_nm;
    private String locplc_lonto_addr;

    public LiveEarthModel(String obs_nm, String locplc_lonto_addr) {
        super.typeOfModel = ParentModel.TYPE_EARTH;
        this.obs_nm = obs_nm;
        this.locplc_lonto_addr = locplc_lonto_addr;
    }

    public String getObs_nm() {
        return obs_nm;
    }

    public String getLocplc_lonto_addr() {
        return locplc_lonto_addr;
    }

    public void setObs_nm(String obs_nm) {
        this.obs_nm = obs_nm;
    }

    public void setLocplc_lonto_addr(String locplc_lonto_addr) {
        this.locplc_lonto_addr = locplc_lonto_addr;
    }
}
