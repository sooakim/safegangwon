package kr.edcan.lumihana.safegangwon.Model;

/**
 * Created by kimok_000 on 2016-09-26.
 */
public class PostModel {
    private String email;
    private String uuid;
    private String title;
    private String content;
    private String image;
    private long time;

    public PostModel(){
    }

    public PostModel(String email, String uuid, String title, String content, String image, long time) {
        this.email = email;
        this.uuid = uuid;
        this.title = title;
        this.content = content;
        this.image = image;
        this.time = time;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getEmail() {
        return email;
    }

    public String getUuid() {
        return uuid;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public String getImage() {
        return image;
    }
}
