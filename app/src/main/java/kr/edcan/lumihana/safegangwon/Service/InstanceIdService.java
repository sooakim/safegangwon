package kr.edcan.lumihana.safegangwon.Service;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import kr.edcan.lumihana.safegangwon.Util.FireMessageManager;

/**
 * Created by kimok_000 on 2016-09-18.
 */
public class InstanceIdService extends FirebaseInstanceIdService {
    private FireMessageManager fireMessageManager;

    @Override
    public void onTokenRefresh() {
        final String refreshedtoken = FirebaseInstanceId.getInstance().getToken();
        sendRegistrationToServer(refreshedtoken);
    }

    private void sendRegistrationToServer(String token){
        fireMessageManager = new FireMessageManager(getApplicationContext());
        fireMessageManager.registerToken(token, "test");
    }
}
