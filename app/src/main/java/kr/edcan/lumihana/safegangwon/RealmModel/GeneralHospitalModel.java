package kr.edcan.lumihana.safegangwon.RealmModel;

import io.realm.RealmModel;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by kimok_000 on 2016-09-24.
 */
@RealmClass
public class GeneralHospitalModel extends RealmObject implements RealmModel{
    @PrimaryKey
    private int _id;

    private int NO;
    private String SIGUN_CD;
    private String SIGUN_NM;
    private String BIZPLC_NM;
    private String LOCPLC_LOTNO_ADDR;
    private String LOCPLC_ROADNM_ADDR;
    private String LICENSG_DE;
    private String BSN_STATE_NM;
    private String CLSBIZ_DE;
    private String SUSPNBIZ_BEGIN_DE;
    private String SUSPNBIZ_END_DE;
    private String REOPENBIZ_DE;
    private int LOCPLC_AR;
    private String LOCPLC_ZIP_CD;
    private int GENRL_AMBLNC_CNT;
    private int SPECL_AMBLNC_CNT;
    private int RESCUPSN_CNT;
    private int SICKBD_CNT;
    private String EASING_MEDCARE_CHARGE_DEPT_NM;
    private String EASING_MEDCARE_APPONT_FORM;
    private String MEDCARE_INST_ASORTMT_NM;
    private int MEDSTAF_CNT;
    private int HOSPTLRM_CNT;
    private String TREAT_SBJECT_CD_INFO;
    private String TREAT_SBJECT_CONT;
    private float TOT_AR;
    private int TOT_PSN_CNT;
    private String FIRST_APPONT_DE;
    private int PERMISN_SICKBD_CNT;
    private int WGS84_LOGT;
    private int WGS84_LAT;
    private int X_CRDNT;
    private int Y_CRDNT;
    private String DATA_COLCT_DE;
    private String ETL_LDADNG_DTM;
    private String LAT;
    private String LNG;

    public GeneralHospitalModel(){}

    public GeneralHospitalModel(int _id, int NO, String SIGUN_CD, String SIGUN_NM, String BIZPLC_NM, String LOCPLC_LOTNO_ADDR, String LOCPLC_ROADNM_ADDR, String LICENSG_DE, String BSN_STATE_NM, String CLSBIZ_DE, String SUSPNBIZ_BEGIN_DE, String SUSPNBIZ_END_DE, String REOPENBIZ_DE, int LOCPLC_AR, String LOCPLC_ZIP_CD, int GENRL_AMBLNC_CNT, int SPECL_AMBLNC_CNT, int RESCUPSN_CNT, int SICKBD_CNT, String EASING_MEDCARE_CHARGE_DEPT_NM, String EASING_MEDCARE_APPONT_FORM, String MEDCARE_INST_ASORTMT_NM, int MEDSTAF_CNT, int HOSPTLRM_CNT, String TREAT_SBJECT_CD_INFO, String TREAT_SBJECT_CONT, float TOT_AR, int TOT_PSN_CNT, String FIRST_APPONT_DE, int PERMISN_SICKBD_CNT, int WGS84_LOGT, int WGS84_LAT, int x_CRDNT, int y_CRDNT, String DATA_COLCT_DE, String ETL_LDADNG_DTM, String LAT, String LNG) {
        this._id = _id;
        this.NO = NO;
        this.SIGUN_CD = SIGUN_CD;
        this.SIGUN_NM = SIGUN_NM;
        this.BIZPLC_NM = BIZPLC_NM;
        this.LOCPLC_LOTNO_ADDR = LOCPLC_LOTNO_ADDR;
        this.LOCPLC_ROADNM_ADDR = LOCPLC_ROADNM_ADDR;
        this.LICENSG_DE = LICENSG_DE;
        this.BSN_STATE_NM = BSN_STATE_NM;
        this.CLSBIZ_DE = CLSBIZ_DE;
        this.SUSPNBIZ_BEGIN_DE = SUSPNBIZ_BEGIN_DE;
        this.SUSPNBIZ_END_DE = SUSPNBIZ_END_DE;
        this.REOPENBIZ_DE = REOPENBIZ_DE;
        this.LOCPLC_AR = LOCPLC_AR;
        this.LOCPLC_ZIP_CD = LOCPLC_ZIP_CD;
        this.GENRL_AMBLNC_CNT = GENRL_AMBLNC_CNT;
        this.SPECL_AMBLNC_CNT = SPECL_AMBLNC_CNT;
        this.RESCUPSN_CNT = RESCUPSN_CNT;
        this.SICKBD_CNT = SICKBD_CNT;
        this.EASING_MEDCARE_CHARGE_DEPT_NM = EASING_MEDCARE_CHARGE_DEPT_NM;
        this.EASING_MEDCARE_APPONT_FORM = EASING_MEDCARE_APPONT_FORM;
        this.MEDCARE_INST_ASORTMT_NM = MEDCARE_INST_ASORTMT_NM;
        this.MEDSTAF_CNT = MEDSTAF_CNT;
        this.HOSPTLRM_CNT = HOSPTLRM_CNT;
        this.TREAT_SBJECT_CD_INFO = TREAT_SBJECT_CD_INFO;
        this.TREAT_SBJECT_CONT = TREAT_SBJECT_CONT;
        this.TOT_AR = TOT_AR;
        this.TOT_PSN_CNT = TOT_PSN_CNT;
        this.FIRST_APPONT_DE = FIRST_APPONT_DE;
        this.PERMISN_SICKBD_CNT = PERMISN_SICKBD_CNT;
        this.WGS84_LOGT = WGS84_LOGT;
        this.WGS84_LAT = WGS84_LAT;
        X_CRDNT = x_CRDNT;
        Y_CRDNT = y_CRDNT;
        this.DATA_COLCT_DE = DATA_COLCT_DE;
        this.ETL_LDADNG_DTM = ETL_LDADNG_DTM;
        this.LAT = LAT;
        this.LNG = LNG;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public int getNO() {
        return NO;
    }

    public void setNO(int NO) {
        this.NO = NO;
    }

    public String getSIGUN_CD() {
        return SIGUN_CD;
    }

    public void setSIGUN_CD(String SIGUN_CD) {
        this.SIGUN_CD = SIGUN_CD;
    }

    public String getSIGUN_NM() {
        return SIGUN_NM;
    }

    public void setSIGUN_NM(String SIGUN_NM) {
        this.SIGUN_NM = SIGUN_NM;
    }

    public String getBIZPLC_NM() {
        return BIZPLC_NM;
    }

    public void setBIZPLC_NM(String BIZPLC_NM) {
        this.BIZPLC_NM = BIZPLC_NM;
    }

    public String getLOCPLC_LOTNO_ADDR() {
        return LOCPLC_LOTNO_ADDR;
    }

    public void setLOCPLC_LOTNO_ADDR(String LOCPLC_LOTNO_ADDR) {
        this.LOCPLC_LOTNO_ADDR = LOCPLC_LOTNO_ADDR;
    }

    public String getLOCPLC_ROADNM_ADDR() {
        return LOCPLC_ROADNM_ADDR;
    }

    public void setLOCPLC_ROADNM_ADDR(String LOCPLC_ROADNM_ADDR) {
        this.LOCPLC_ROADNM_ADDR = LOCPLC_ROADNM_ADDR;
    }

    public String getLICENSG_DE() {
        return LICENSG_DE;
    }

    public void setLICENSG_DE(String LICENSG_DE) {
        this.LICENSG_DE = LICENSG_DE;
    }

    public String getBSN_STATE_NM() {
        return BSN_STATE_NM;
    }

    public void setBSN_STATE_NM(String BSN_STATE_NM) {
        this.BSN_STATE_NM = BSN_STATE_NM;
    }

    public String getCLSBIZ_DE() {
        return CLSBIZ_DE;
    }

    public void setCLSBIZ_DE(String CLSBIZ_DE) {
        this.CLSBIZ_DE = CLSBIZ_DE;
    }

    public String getSUSPNBIZ_BEGIN_DE() {
        return SUSPNBIZ_BEGIN_DE;
    }

    public void setSUSPNBIZ_BEGIN_DE(String SUSPNBIZ_BEGIN_DE) {
        this.SUSPNBIZ_BEGIN_DE = SUSPNBIZ_BEGIN_DE;
    }

    public String getSUSPNBIZ_END_DE() {
        return SUSPNBIZ_END_DE;
    }

    public void setSUSPNBIZ_END_DE(String SUSPNBIZ_END_DE) {
        this.SUSPNBIZ_END_DE = SUSPNBIZ_END_DE;
    }

    public String getREOPENBIZ_DE() {
        return REOPENBIZ_DE;
    }

    public void setREOPENBIZ_DE(String REOPENBIZ_DE) {
        this.REOPENBIZ_DE = REOPENBIZ_DE;
    }

    public int getLOCPLC_AR() {
        return LOCPLC_AR;
    }

    public void setLOCPLC_AR(int LOCPLC_AR) {
        this.LOCPLC_AR = LOCPLC_AR;
    }

    public String getLOCPLC_ZIP_CD() {
        return LOCPLC_ZIP_CD;
    }

    public void setLOCPLC_ZIP_CD(String LOCPLC_ZIP_CD) {
        this.LOCPLC_ZIP_CD = LOCPLC_ZIP_CD;
    }

    public int getGENRL_AMBLNC_CNT() {
        return GENRL_AMBLNC_CNT;
    }

    public void setGENRL_AMBLNC_CNT(int GENRL_AMBLNC_CNT) {
        this.GENRL_AMBLNC_CNT = GENRL_AMBLNC_CNT;
    }

    public int getSPECL_AMBLNC_CNT() {
        return SPECL_AMBLNC_CNT;
    }

    public void setSPECL_AMBLNC_CNT(int SPECL_AMBLNC_CNT) {
        this.SPECL_AMBLNC_CNT = SPECL_AMBLNC_CNT;
    }

    public int getRESCUPSN_CNT() {
        return RESCUPSN_CNT;
    }

    public void setRESCUPSN_CNT(int RESCUPSN_CNT) {
        this.RESCUPSN_CNT = RESCUPSN_CNT;
    }

    public int getSICKBD_CNT() {
        return SICKBD_CNT;
    }

    public void setSICKBD_CNT(int SICKBD_CNT) {
        this.SICKBD_CNT = SICKBD_CNT;
    }

    public String getEASING_MEDCARE_CHARGE_DEPT_NM() {
        return EASING_MEDCARE_CHARGE_DEPT_NM;
    }

    public void setEASING_MEDCARE_CHARGE_DEPT_NM(String EASING_MEDCARE_CHARGE_DEPT_NM) {
        this.EASING_MEDCARE_CHARGE_DEPT_NM = EASING_MEDCARE_CHARGE_DEPT_NM;
    }

    public String getEASING_MEDCARE_APPONT_FORM() {
        return EASING_MEDCARE_APPONT_FORM;
    }

    public void setEASING_MEDCARE_APPONT_FORM(String EASING_MEDCARE_APPONT_FORM) {
        this.EASING_MEDCARE_APPONT_FORM = EASING_MEDCARE_APPONT_FORM;
    }

    public String getMEDCARE_INST_ASORTMT_NM() {
        return MEDCARE_INST_ASORTMT_NM;
    }

    public void setMEDCARE_INST_ASORTMT_NM(String MEDCARE_INST_ASORTMT_NM) {
        this.MEDCARE_INST_ASORTMT_NM = MEDCARE_INST_ASORTMT_NM;
    }

    public int getMEDSTAF_CNT() {
        return MEDSTAF_CNT;
    }

    public void setMEDSTAF_CNT(int MEDSTAF_CNT) {
        this.MEDSTAF_CNT = MEDSTAF_CNT;
    }

    public int getHOSPTLRM_CNT() {
        return HOSPTLRM_CNT;
    }

    public void setHOSPTLRM_CNT(int HOSPTLRM_CNT) {
        this.HOSPTLRM_CNT = HOSPTLRM_CNT;
    }

    public String getTREAT_SBJECT_CD_INFO() {
        return TREAT_SBJECT_CD_INFO;
    }

    public void setTREAT_SBJECT_CD_INFO(String TREAT_SBJECT_CD_INFO) {
        this.TREAT_SBJECT_CD_INFO = TREAT_SBJECT_CD_INFO;
    }

    public String getTREAT_SBJECT_CONT() {
        return TREAT_SBJECT_CONT;
    }

    public void setTREAT_SBJECT_CONT(String TREAT_SBJECT_CONT) {
        this.TREAT_SBJECT_CONT = TREAT_SBJECT_CONT;
    }

    public float getTOT_AR() {
        return TOT_AR;
    }

    public void setTOT_AR(float TOT_AR) {
        this.TOT_AR = TOT_AR;
    }

    public int getTOT_PSN_CNT() {
        return TOT_PSN_CNT;
    }

    public void setTOT_PSN_CNT(int TOT_PSN_CNT) {
        this.TOT_PSN_CNT = TOT_PSN_CNT;
    }

    public String getFIRST_APPONT_DE() {
        return FIRST_APPONT_DE;
    }

    public void setFIRST_APPONT_DE(String FIRST_APPONT_DE) {
        this.FIRST_APPONT_DE = FIRST_APPONT_DE;
    }

    public int getPERMISN_SICKBD_CNT() {
        return PERMISN_SICKBD_CNT;
    }

    public void setPERMISN_SICKBD_CNT(int PERMISN_SICKBD_CNT) {
        this.PERMISN_SICKBD_CNT = PERMISN_SICKBD_CNT;
    }

    public int getWGS84_LOGT() {
        return WGS84_LOGT;
    }

    public void setWGS84_LOGT(int WGS84_LOGT) {
        this.WGS84_LOGT = WGS84_LOGT;
    }

    public int getWGS84_LAT() {
        return WGS84_LAT;
    }

    public void setWGS84_LAT(int WGS84_LAT) {
        this.WGS84_LAT = WGS84_LAT;
    }

    public int getX_CRDNT() {
        return X_CRDNT;
    }

    public void setX_CRDNT(int X_CRDNT) {
        this.X_CRDNT = X_CRDNT;
    }

    public int getY_CRDNT() {
        return Y_CRDNT;
    }

    public void setY_CRDNT(int Y_CRDNT) {
        this.Y_CRDNT = Y_CRDNT;
    }

    public String getDATA_COLCT_DE() {
        return DATA_COLCT_DE;
    }

    public void setDATA_COLCT_DE(String DATA_COLCT_DE) {
        this.DATA_COLCT_DE = DATA_COLCT_DE;
    }

    public String getETL_LDADNG_DTM() {
        return ETL_LDADNG_DTM;
    }

    public void setETL_LDADNG_DTM(String ETL_LDADNG_DTM) {
        this.ETL_LDADNG_DTM = ETL_LDADNG_DTM;
    }

    public String getLAT() {
        return LAT;
    }

    public void setLAT(String LAT) {
        this.LAT = LAT;
    }

    public String getLNG() {
        return LNG;
    }

    public void setLNG(String LNG) {
        this.LNG = LNG;
    }
}
