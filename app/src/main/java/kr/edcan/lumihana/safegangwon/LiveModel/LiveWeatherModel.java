package kr.edcan.lumihana.safegangwon.LiveModel;

/**
 * Created by kimok_000 on 2016-09-28.
 */
public class LiveWeatherModel extends ParentModel {
    private String alertYn;
    private String stromYn;
    private String tmax;
    private String tmin;
    private String nameSky;

    public LiveWeatherModel(String alertYn, String stromYn, String tmax, String tmin, String nameSky) {
        super.typeOfModel = TYPE_WEATHER;
        this.alertYn = alertYn;
        this.stromYn = stromYn;
        this.tmax = tmax;
        this.tmin = tmin;
        this.nameSky = nameSky;
    }

    public void setAlertYn(String alertYn) {
        this.alertYn = alertYn;
    }

    public void setStromYn(String stromYn) {
        this.stromYn = stromYn;
    }

    public void setTmax(String tmax) {
        this.tmax = tmax;
    }

    public void setTmin(String tmin) {
        this.tmin = tmin;
    }

    public void setNameSky(String nameSky) {
        this.nameSky = nameSky;
    }

    public String getAlertYn() {
        return alertYn;
    }

    public String getStromYn() {
        return stromYn;
    }

    public String getTmax() {
        return tmax;
    }

    public String getTmin() {
        return tmin;
    }

    public String getNameSky() {
        return nameSky;
    }
}
