package kr.edcan.lumihana.safegangwon.LiveModel;

/**
 * Created by kimok_000 on 2016-09-28.
 */
public class LiveWaterSupplyModel extends ParentModel{
    private String bizplc_nm;
    private String locplc_lotno_addr;

    public LiveWaterSupplyModel(String bizplc_nm, String locplc_lotno_addr) {
        super.typeOfModel = ParentModel.TYPE_WATER_SUPPLY;
        this.bizplc_nm = bizplc_nm;
        this.locplc_lotno_addr = locplc_lotno_addr;
    }

    public String getBizplc_nm() {
        return bizplc_nm;
    }

    public String getLocplc_lotno_addr() {
        return locplc_lotno_addr;
    }

    public void setBizplc_nm(String bizplc_nm) {
        this.bizplc_nm = bizplc_nm;
    }

    public void setLocplc_lotno_addr(String locplc_lotno_addr) {
        this.locplc_lotno_addr = locplc_lotno_addr;
    }
}
