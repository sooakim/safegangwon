package kr.edcan.lumihana.safegangwon.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import kr.edcan.lumihana.safegangwon.LiveModel.ParentModel;

/**
 * Created by kimok_000 on 2016-09-23.
 */
public class EmergencyNoticeModel extends ParentModel {
    @SerializedName("MisfortuneSituationNoticeMsg")
    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        @SerializedName("head")
        private List<HeadBean> head;
        @SerializedName("row")
        private List<RowBean> row;

        public List<HeadBean> getHead() {
            return head;
        }

        public void setHead(List<HeadBean> head) {
            this.head = head;
        }

        public List<RowBean> getRow() {
            return row;
        }

        public void setRow(List<RowBean> row) {
            this.row = row;
        }

        public static class HeadBean {
            @SerializedName("list_total_count")
            private int list_total_count;
            @SerializedName("RESULT")
            private RESULTBean RESULT;

            public int getList_total_count() {
                return list_total_count;
            }

            public RESULTBean getRESULT() {
                return RESULT;
            }

            public void setList_total_count(int list_total_count) {
                this.list_total_count = list_total_count;
            }

            public void setRESULT(RESULTBean RESULT) {
                this.RESULT = RESULT;
            }

            public static class RESULTBean {
                @SerializedName("code")
                private String code;
                @SerializedName("message")
                private String message;

                public String getCode() {
                    return code;
                }

                public String getMessage() {
                    return message;
                }

                public void setCode(String code) {
                    this.code = code;
                }

                public void setMessage(String message) {
                    this.message = message;
                }
            }
        }

        public static class RowBean {
            @SerializedName("msg_id")
            private long msg_id;
            @SerializedName("msg_seq")
            private int msg_seq;
            @SerializedName("clmy_pttn_cd")
            private int clmy_pttn_cd;
            @SerializedName("clmy_pttn_nm")
            private String clmy_pttn_nm;
            @SerializedName("titl")
            private String titl;
            @SerializedName("cnts1")
            private String cnts1;
            @SerializedName("inpt_date")
            private String inpt_date;

            public long getMsg_id() {
                return msg_id;
            }

            public int getMsg_seq() {
                return msg_seq;
            }

            public int getClmy_pttn_cd() {
                return clmy_pttn_cd;
            }

            public String getClmy_pttn_nm() {
                return clmy_pttn_nm;
            }

            public String getTitl() {
                return titl;
            }

            public String getCnts1() {
                return cnts1;
            }

            public String getInpt_date() {
                return inpt_date;
            }

            public void setMsg_id(long msg_id) {
                this.msg_id = msg_id;
            }

            public void setMsg_seq(int msg_seq) {
                this.msg_seq = msg_seq;
            }

            public void setClmy_pttn_cd(int clmy_pttn_cd) {
                this.clmy_pttn_cd = clmy_pttn_cd;
            }

            public void setClmy_pttn_nm(String clmy_pttn_nm) {
                this.clmy_pttn_nm = clmy_pttn_nm;
            }

            public void setTitl(String titl) {
                this.titl = titl;
            }

            public void setCnts1(String cnts1) {
                this.cnts1 = cnts1;
            }

            public void setInpt_date(String inpt_date) {
                this.inpt_date = inpt_date;
            }
        }
    }
}
