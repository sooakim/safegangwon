package kr.edcan.lumihana.safegangwon.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;
import kr.edcan.lumihana.safegangwon.Adapter.DataAdapter;
import kr.edcan.lumihana.safegangwon.Model.DataModel;
import kr.edcan.lumihana.safegangwon.R;
import kr.edcan.lumihana.safegangwon.RealmModel.EarthModel;
import kr.edcan.lumihana.safegangwon.Util.NetworkManager;

public class EarthActivity extends AppCompatActivity implements OnMapReadyCallback {
    private static final String POINT_NAME = "지진 계측소";
    private GoogleMap earth_map;
    private Toolbar toolbar;
    private ArrayList<DataModel> arrayList;
    private RecyclerView recycler_data;
    private RelativeLayout relative_map;
    private LinearLayoutManager linearLayoutManager;
    private DataAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_earth);

        relative_map = (RelativeLayout) findViewById(R.id.earth_relative_map);
        initToolbar();
        initMap();
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.earth_toolbar);
        toolbar.setTitle("지진 계측소");
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_36px);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.earth_map);
        mapFragment.getMapAsync(this);
    }

    private void updateRecycler() {
        adapter.notifyDataSetChanged();
    }

    private void initRecycler() {
        arrayList = new ArrayList<>();

        linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        adapter = new DataAdapter(getApplicationContext(), arrayList);

        recycler_data = (RecyclerView) findViewById(R.id.earth_recycler_data);
        recycler_data.setLayoutManager(linearLayoutManager);
        recycler_data.setAdapter(adapter);
    }

    private void fillRecycler() {
        final Intent intent = getIntent();
        final int _id = intent.getIntExtra("_id", -1);

        if (_id >= 0) {
            Realm realm = Realm.getDefaultInstance();
            RealmResults<EarthModel> results = realm.where(EarthModel.class).equalTo("_id", _id).findAll();

            EarthModel model;
            if (results.size() <= 0) model = null;
            else model = results.get(0);

            if (model != null) {
                final String sido = getModelValue(model.getSIDO());
                final String gugun_nm = getModelValue(model.getGUGUN_NM());
                final String obs_nm = getModelValue(model.getOBS_NM());
                final String lng = getModelValue(model.getLNG());
                final String lat = getModelValue(model.getLAT());
                final String obs_org_nm = getModelValue(model.getOBS_ORG_NM());
                final String obs_org = getModelValue(model.getOBS_ORG());
                final String obs_sdate = getModelValue(model.getOBS_SDATE());
                final String obs_edate = getModelValue(model.getOBS_EDATE());
                final String obs_type = getModelValue(model.getOBS_TYPE());
                final String obs_status = getModelValue(model.getOBS_STATUS());
                final String locplc_lotno_addr = getModelValue(model.getLOCPLC_LOTNO_ADDR());
                final String obs_etc = getModelValue(model.getOBS_ETC());
                final String obs_equp = getModelValue(model.getOBS_EQUP());
                final String obs_contents = getModelValue(model.getOBS_CONTENTS());
                final String obs_time = getModelValue(model.getOBS_TIME());
                final String data_base_dt = getModelValue(model.getDATA_BASE_DT());

                if (!sido.equals("")) arrayList.add(new DataModel("시 / 도", sido));
                if (!gugun_nm.equals("")) arrayList.add(new DataModel("군 / 구", gugun_nm));
                if (!obs_nm.equals("")) arrayList.add(new DataModel("관측소명", obs_nm));
                if (!lat.equals("") || !lng.equals(""))
                    pointInMap(Double.parseDouble(lat), Double.parseDouble(lng));
                else {
                    relative_map.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), "지도 정보 없음", Toast.LENGTH_SHORT).show();
                }
                if (!obs_org_nm.equals("")) arrayList.add(new DataModel("관측 기관", obs_org_nm));
                if (!obs_org.equals("")) arrayList.add(new DataModel("운영 기관", obs_org));
                if (!obs_sdate.equals("")) arrayList.add(new DataModel("관측 시작", obs_sdate));
                if (!obs_edate.equals("")) arrayList.add(new DataModel("관측 종료", obs_edate));
                if (!obs_type.equals("")) arrayList.add(new DataModel("관측소 유형", obs_type));
                if (!obs_status.equals("")) arrayList.add(new DataModel("운영 상태", obs_status));
                if (!locplc_lotno_addr.equals(""))
                    arrayList.add(new DataModel("소재지 지번 주소", locplc_lotno_addr));
                if (!obs_etc.equals("")) arrayList.add(new DataModel("특이 사항", obs_etc));
                if (!obs_equp.equals("")) arrayList.add(new DataModel("관측 장비", obs_equp));
                if (!obs_contents.equals("")) arrayList.add(new DataModel("관측 내용", obs_contents));
                if (!obs_time.equals("")) arrayList.add(new DataModel("관측 시간", obs_time));
                if (!data_base_dt.equals(""))
                    arrayList.add(new DataModel("데이터 기준 일자", data_base_dt));

                updateRecycler();
            } else {

            }
        } else {

        }
    }

    private String getModelValue(String original) {
        if (original == null) return "";
        else return original.toString().trim();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        earth_map = googleMap;

        initRecycler();
        fillRecycler();
    }

    private void pointInMap(double lat, double lng) {
        if (!(125.0 <= lng && lng <= 132.0) && !(32 <= lat && lat <= 39)) {
            relative_map.setVisibility(View.GONE);  //잘못된 좌표
            Toast.makeText(getApplicationContext(), "지도를 표시할 수 없음", Toast.LENGTH_SHORT).show();
        }
        LatLng point = new LatLng(lat, lng);
        earth_map.addMarker(new MarkerOptions().position(point).title(POINT_NAME));
        earth_map.moveCamera(CameraUpdateFactory.newLatLngZoom(point, 16));
    }

    private boolean checkNetwork() {
        if (NetworkManager.getConnectivityStatus(getApplicationContext()) == NetworkManager.TYPE_NOT_CONNECTED) {
            RelativeLayout map = (RelativeLayout) findViewById(R.id.earth_relative_map);

            Snackbar.make(map, "네트워크에 연결해주세요.", Snackbar.LENGTH_LONG)
                    .setAction("연결", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final Intent intent = new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    }).show();
            return false;
        } else return true;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        checkNetwork();
    }

    @Override
    protected void onResume() {
        super.onResume();
        onStart();
    }
}
