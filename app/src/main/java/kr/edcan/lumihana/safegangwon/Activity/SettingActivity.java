package kr.edcan.lumihana.safegangwon.Activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import kr.edcan.lumihana.safegangwon.R;
import kr.edcan.lumihana.safegangwon.Service.DataAutoSyncService;

public class SettingActivity extends AppCompatActivity {
    private static final long TIME_SECOND = 1000;
    private static final long TIME_UPSCALE = 60;
    private RecyclerView recycler_list;
    private LinearLayoutManager linearLayoutManager;
    private Switch switch_gps, switch_sync;
    private TextView text_use, text_bluetooth;
    private Toolbar toolbar;
    private Spinner spinner_sigungu, spinner_sync;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);

        initSharedPreferences();
        setToolBar();
        setSpinner();

        switch_gps = (Switch) findViewById(R.id.setting_switch_gps);
        switch_gps.setChecked(sharedPreferences.getBoolean("gps", false));
        switch_gps.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                editor.putBoolean("gps", isChecked);
                editor.commit();
            }
        });

        switch_sync = (Switch) findViewById(R.id.setting_switch_sync);
        switch_sync.setChecked(sharedPreferences.getBoolean("sync", false));
        switch_sync.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                editor.putBoolean("sync", isChecked);
                editor.commit();
            }
        });

        text_bluetooth = (TextView) findViewById(R.id.setting_text_bluetooth);
        text_bluetooth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "이 기능은 준비중입니다...", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initSharedPreferences() {
        sharedPreferences = getSharedPreferences("Setting", MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    private void setSpinner() {
        final String[] options = getResources().getStringArray(R.array.spinnerSigungu);
        final String[] times = getResources().getStringArray(R.array.syncTime);
        spinner_sigungu = (Spinner) findViewById(R.id.setting_spinner_sigungu);
        spinner_sync = (Spinner) findViewById(R.id.setting_spinner_sync);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, options);
        spinner_sigungu.setAdapter(adapter);
        spinner_sigungu.setSelection(findDefaultValue(options));
        spinner_sigungu.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final String region = options[position];
                editor.putString("region", region);
                editor.commit();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        Intent intent = new Intent(this, DataAutoSyncService.class);
        final PendingIntent sender = PendingIntent.getBroadcast(this, 0, intent, 0);

        ArrayAdapter<String> syncAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, times);
        spinner_sync.setAdapter(syncAdapter);
        spinner_sync.setSelection(findDefaultTime(times));
        spinner_sync.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final String time = options[position];
                editor.putString("time", time);
                editor.commit();

                long milltime = 60000;
                switch (position) {
                    case 0:
                        milltime = TIME_SECOND * TIME_UPSCALE;
                        break;
                    case 1:
                        milltime = TIME_SECOND * TIME_UPSCALE * 15;
                        break;
                    case 2:
                        milltime = TIME_SECOND * TIME_UPSCALE * 30;
                        break;
                    case 3:
                        milltime = TIME_SECOND * TIME_UPSCALE * TIME_UPSCALE;
                        break;
                    case 4:
                        milltime = TIME_SECOND * TIME_UPSCALE * TIME_UPSCALE * 3;
                        break;
                    case 5:
                        milltime = TIME_SECOND * TIME_UPSCALE * TIME_UPSCALE * 6;
                        break;
                    case 6:
                        milltime = TIME_SECOND * TIME_UPSCALE * TIME_UPSCALE * 12;
                        break;
                    case 7:
                        milltime = TIME_SECOND * TIME_UPSCALE * TIME_UPSCALE * 24;
                        break;
                    case 8:
                        milltime = TIME_SECOND * TIME_UPSCALE * TIME_UPSCALE * 24 * 3;
                        break;
                    case 9:
                        milltime = TIME_SECOND * TIME_UPSCALE * TIME_UPSCALE * 24 * 7;
                        break;
                }

                AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
                am.setInexactRepeating(AlarmManager.RTC, System.currentTimeMillis(), milltime, sender);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private int findDefaultTime(String[] times) {
        for (int i = 0; i < times.length; i++) {
            if (times[i].equals(sharedPreferences.getString("time", ""))) return i;
        }
        return 0;
    }

    private int findDefaultValue(String[] options) {
        for (int i = 0; i < options.length; i++) {
            if (options[i].equals(sharedPreferences.getString("region", ""))) return i;
        }
        return 0;
    }

    private void setToolBar() {
        toolbar = (Toolbar) findViewById(R.id.setting_toolbar);
        toolbar.setTitle("설정");
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_36px);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
