package kr.edcan.lumihana.safegangwon.Util;

import android.content.Context;
import android.util.Log;

import kr.edcan.lumihana.safegangwon.Model.WeatherModel;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by kimok_000 on 2016-08-21.
 */
public class WeatherManager {
    private static final String CLASS_NAME = "WeatherManager";
    public static final int VERSION = 1;
    public static final String SKY_CLEAR = "SKY_A01";
    public static final String SKY_CLOUD_LOW = "SKY_A02";
    public static final String SKY_CLOUD_HIGH = "SKY_A03";
    public static final String SKY_CLOUD_AND_RAIN = "SKY_A04";
    public static final String SKY_CLOUD_AND_SNOW = "SKY_A05";
    public static final String SKY_CLOUD_AND_RAIN_OR_SNOW = "SKY_A06";
    public static final String SKY_CLOUDY = "SKY_A07";
    public static final String SKY_CLOUDY_AND_RAIN = "SKY_A08";
    public static final String SKY_CLOUDY_AND_SNOW = "SKY_A09";
    public static final String SKY_CLOUDY_AND_RAIN_OR_SNOW = "SKY_A10";
    public static final String SKY_CLOUDY_AND_THUNDER = "SKY_A11";
    public static final String SKY_THUNDER_AND_RAIN = "SKY_A12";
    public static final String SKY_THUNDER_AND_SNOW = "SKY_A13";
    public static final String SKY_THUNDER_AND_RAIN_OR_SNOW = "SKY_A14";

    private int autoUpdateCount = 0;
    private WeatherManager weatherManager = this;
    private Context context;
    private Retrofit retrofit;
    private IWeatherService weatherService;
    private Call<WeatherModel> weather;
    private OnWeatherChangedListener onWeatherChangedListener;
    private int version = VERSION;
    private String appKey = "";
    private boolean enableAutoUpdate = false;
//    private Thread autoUpdate = new Thread(new Runnable() {
//        @Override
//        public void run() {
//            while (true) {
//                if (enableAutoUpdate) {
//                    weatherManager.onWeatherChangedListener.autoWeatherUpdate(weatherManager);
//                    Log.e(CLASS_NAME, "AutoUpdate running.." + autoUpdateCount++);
//                }
//                try {
//                    Thread.sleep(60000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
//    });

    public WeatherManager(Context context, int version, String appKey) {
        if (appKey == null) return;
        this.context = context;
        this.version = version;
        this.appKey = appKey;

        initRetrofit();
//        autoUpdate.start();
    }

    private void initRetrofit() {
        retrofit = new Retrofit.Builder()
                .baseUrl("http://apis.skplanetx.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        weatherService = retrofit.create(IWeatherService.class);
    }

    public void setAutoUpdate(boolean enableAutoUpdate) {
        this.enableAutoUpdate = enableAutoUpdate;
    }

    public void setOnWeatherChangedListener(OnWeatherChangedListener onWeatherChangedListener) {
        this.onWeatherChangedListener = onWeatherChangedListener;
    }

    public void getWeather(String city, String county, String village) {
        weather = weatherService.weatherWithAddress(appKey, city, county, village, version);
        weather.enqueue(new Callback<WeatherModel>() {
            @Override
            public void onResponse(Response<WeatherModel> response, Retrofit retrofit) {
                if (response.code() == 200) {
                    onWeatherChangedListener.onChanged(response.body());
                    onWeatherChangedListener.weatherWithAddress(response.body());
                } else Log.e(CLASS_NAME, "weather server return !200");
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e(CLASS_NAME, "Failed to get weatherInfo");
                Log.e(CLASS_NAME, t.getMessage() + "");
            }
        });
    }

    public void getWeather(String lat, String lon) {
        weather = weatherService.weatherWithLocation(appKey, lat, lon, version);
        weather.enqueue(new Callback<WeatherModel>() {
            @Override
            public void onResponse(Response<WeatherModel> response, Retrofit retrofit) {
                if (response.code() == 200) {
                    onWeatherChangedListener.onChanged(response.body());
                    onWeatherChangedListener.weatherWithLocation(response.body());
                } else Log.e(CLASS_NAME, "weather server return !200");
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e(CLASS_NAME, "Failed to get weatherInfo");
                Log.e(CLASS_NAME, t.getMessage());
            }
        });
    }

    public void getWeather(int stnid) {
        weather = weatherService.weatherWithId(appKey, stnid, version);
        weather.enqueue(new Callback<WeatherModel>() {
            @Override
            public void onResponse(Response<WeatherModel> response, Retrofit retrofit) {
                if (response.code() == 200) {
                    onWeatherChangedListener.onChanged(response.body());
                    onWeatherChangedListener.weatherWithId(response.body());
                } else Log.e(CLASS_NAME, "weather server return !200");
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e(CLASS_NAME, "Failed to get weatherInfo");
                Log.e(CLASS_NAME, t.getMessage());
            }
        });
    }

    public int getVersion() {
        return version;
    }

    public String getAppKey() {
        return appKey;
    }

    public interface OnWeatherChangedListener {
        void weatherWithAddress(WeatherModel weather);

        void weatherWithLocation(WeatherModel weather);

        void weatherWithId(WeatherModel weather);

        void onChanged(WeatherModel weather);

//        void autoWeatherUpdate(WeatherManager weatherManager);
    }
}
