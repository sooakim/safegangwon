package kr.edcan.lumihana.safegangwon.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import kr.edcan.lumihana.safegangwon.LiveModel.ParentModel;

/**
 * Created by kimok_000 on 2016-09-23.
 */
public class CivilDefenseModel extends ParentModel {

    /**
     * list_total_count : 980
     * RESULT : {"CODE":"INFO-000","MESSAGE":"정상 처리되었습니다."}
     * row : [{"NO":89,"SIGUN_CD":"","SIGUN_NM":"","BIZPLC_NM":"삼천리3차아파트","LOCPLC_LOTNO_ADDR":"강원도 원주시 학성동 1031번지","LOCPLC_ROADNM_ADDR":"","LICENSG_DE":"20060919","BSN_STATE_NM":"운영중","CLSBIZ_DE":"","SUSPNBIZ_BEGIN_DE":"","SUSPNBIZ_END_DE":"","REOPENBIZ_DE":"","LOCPLC_AR":4036,"LOCPLC_ZIP_CD":"","FACLT_BULDNG_NM":"삼천리3차아파트","RELEASE_DE":"","EMGNCY_FACLT_DIV_NM":"민방위대피시설","WGS84_LOGT":0,"WGS84_LAT":0,"X_CRDNT":0,"Y_CRDNT":0,"DATA_COLCT_DE":"20151205","ETL_LDADNG_DTM":"","LAT":"","LNG":""},{"NO":90,"SIGUN_CD":"","SIGUN_NM":"","BIZPLC_NM":"삼천리1차아파트","LOCPLC_LOTNO_ADDR":"강원도 원주시 학성동 1032번지 6호","LOCPLC_ROADNM_ADDR":"","LICENSG_DE":"20060919","BSN_STATE_NM":"운영중","CLSBIZ_DE":"","SUSPNBIZ_BEGIN_DE":"","SUSPNBIZ_END_DE":"","REOPENBIZ_DE":"","LOCPLC_AR":606,"LOCPLC_ZIP_CD":"","FACLT_BULDNG_NM":"삼천리1차아파트","RELEASE_DE":"","EMGNCY_FACLT_DIV_NM":"민방위대피시설","WGS84_LOGT":0,"WGS84_LAT":0,"X_CRDNT":0,"Y_CRDNT":0,"DATA_COLCT_DE":"20151205","ETL_LDADNG_DTM":"","LAT":"","LNG":""},{"NO":91,"SIGUN_CD":"","SIGUN_NM":"","BIZPLC_NM":"청솔5차아파트","LOCPLC_LOTNO_ADDR":"강원도 원주시 관설동 1774번지 3호","LOCPLC_ROADNM_ADDR":"","LICENSG_DE":"20060915","BSN_STATE_NM":"운영중","CLSBIZ_DE":"","SUSPNBIZ_BEGIN_DE":"","SUSPNBIZ_END_DE":"","REOPENBIZ_DE":"","LOCPLC_AR":4221,"LOCPLC_ZIP_CD":"","FACLT_BULDNG_NM":"청솔5차아파트","RELEASE_DE":"","EMGNCY_FACLT_DIV_NM":"민방위대피시설","WGS84_LOGT":0,"WGS84_LAT":0,"X_CRDNT":0,"Y_CRDNT":0,"DATA_COLCT_DE":"20151205","ETL_LDADNG_DTM":"","LAT":"","LNG":""},{"NO":92,"SIGUN_CD":"","SIGUN_NM":"","BIZPLC_NM":"코아루아파트","LOCPLC_LOTNO_ADDR":"강원도 원주시 관설동 1774번지 4호","LOCPLC_ROADNM_ADDR":"","LICENSG_DE":"20060915","BSN_STATE_NM":"운영중","CLSBIZ_DE":"","SUSPNBIZ_BEGIN_DE":"","SUSPNBIZ_END_DE":"","REOPENBIZ_DE":"","LOCPLC_AR":4876,"LOCPLC_ZIP_CD":"","FACLT_BULDNG_NM":"코아루아파트","RELEASE_DE":"","EMGNCY_FACLT_DIV_NM":"민방위대피시설","WGS84_LOGT":0,"WGS84_LAT":0,"X_CRDNT":0,"Y_CRDNT":0,"DATA_COLCT_DE":"20151205","ETL_LDADNG_DTM":"","LAT":"","LNG":""},{"NO":93,"SIGUN_CD":"","SIGUN_NM":"","BIZPLC_NM":"문막 현진아파트","LOCPLC_LOTNO_ADDR":"강원도 원주시 문막읍 동화리 1543번지","LOCPLC_ROADNM_ADDR":"","LICENSG_DE":"20060630","BSN_STATE_NM":"운영중","CLSBIZ_DE":"","SUSPNBIZ_BEGIN_DE":"","SUSPNBIZ_END_DE":"","REOPENBIZ_DE":"","LOCPLC_AR":3239,"LOCPLC_ZIP_CD":"","FACLT_BULDNG_NM":"문막 현진아파트","RELEASE_DE":"","EMGNCY_FACLT_DIV_NM":"민방위대피시설","WGS84_LOGT":0,"WGS84_LAT":0,"X_CRDNT":0,"Y_CRDNT":0,"DATA_COLCT_DE":"20151205","ETL_LDADNG_DTM":"","LAT":"","LNG":""}]
     */

    @SerializedName("localdata-health_medica-civil_defense_evacuate_facility")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private int list_total_count;
        /**
         * CODE : INFO-000
         * MESSAGE : 정상 처리되었습니다.
         */

        @SerializedName("RESULT")
        private RESULTBean RESULT;
        /**
         * NO : 89
         * SIGUN_CD :
         * SIGUN_NM :
         * BIZPLC_NM : 삼천리3차아파트
         * LOCPLC_LOTNO_ADDR : 강원도 원주시 학성동 1031번지
         * LOCPLC_ROADNM_ADDR :
         * LICENSG_DE : 20060919
         * BSN_STATE_NM : 운영중
         * CLSBIZ_DE :
         * SUSPNBIZ_BEGIN_DE :
         * SUSPNBIZ_END_DE :
         * REOPENBIZ_DE :
         * LOCPLC_AR : 4036
         * LOCPLC_ZIP_CD :
         * FACLT_BULDNG_NM : 삼천리3차아파트
         * RELEASE_DE :
         * EMGNCY_FACLT_DIV_NM : 민방위대피시설
         * WGS84_LOGT : 0
         * WGS84_LAT : 0
         * X_CRDNT : 0
         * Y_CRDNT : 0
         * DATA_COLCT_DE : 20151205
         * ETL_LDADNG_DTM :
         * LAT :
         * LNG :
         */

        @SerializedName("row")
        private List<RowBean> row;

        public int getList_total_count() {
            return list_total_count;
        }

        public void setList_total_count(int list_total_count) {
            this.list_total_count = list_total_count;
        }

        public RESULTBean getRESULT() {
            return RESULT;
        }

        public void setRESULT(RESULTBean RESULT) {
            this.RESULT = RESULT;
        }

        public List<RowBean> getRow() {
            return row;
        }

        public void setRow(List<RowBean> row) {
            this.row = row;
        }

        public static class RESULTBean {
            @SerializedName("CODE")
            private String CODE;
            @SerializedName("MESSAGE")
            private String MESSAGE;

            public String getCODE() {
                return CODE;
            }

            public void setCODE(String CODE) {
                this.CODE = CODE;
            }

            public String getMESSAGE() {
                return MESSAGE;
            }

            public void setMESSAGE(String MESSAGE) {
                this.MESSAGE = MESSAGE;
            }
        }

        public static class RowBean {
            @SerializedName("NO")
            private int NO;
            @SerializedName("SIGUN_CD")
            private String SIGUN_CD;
            @SerializedName("SIGUN_NM")
            private String SIGUN_NM;
            @SerializedName("BIZPLC_NM")
            private String BIZPLC_NM;
            @SerializedName("LOCPLC_LOTNO_ADDR")
            private String LOCPLC_LOTNO_ADDR;
            @SerializedName("LOCPLC_ROADNM_ADDR")
            private String LOCPLC_ROADNM_ADDR;
            @SerializedName("LICENSG_DE")
            private String LICENSG_DE;
            @SerializedName("BSN_STATE_NM")
            private String BSN_STATE_NM;
            @SerializedName("CLSBIZ_DE")
            private String CLSBIZ_DE;
            @SerializedName("SUSPNBIZ_BEGIN_DE")
            private String SUSPNBIZ_BEGIN_DE;
            @SerializedName("SUSPNBIZ_END_DE")
            private String SUSPNBIZ_END_DE;
            @SerializedName("REOPENBIZ_DE")
            private String REOPENBIZ_DE;
            @SerializedName("LOCPLC_AR")
            private int LOCPLC_AR;
            @SerializedName("LOCPLC_ZIP_CD")
            private String LOCPLC_ZIP_CD;
            @SerializedName("FACLT_BULDNG_NM")
            private String FACLT_BULDNG_NM;
            @SerializedName("RELEASE_DE")
            private String RELEASE_DE;
            @SerializedName("EMGNCY_FACLT_DIV_NM")
            private String EMGNCY_FACLT_DIV_NM;
            @SerializedName("WGS84_LOGT")
            private int WGS84_LOGT;
            @SerializedName("WGS84_LAT")
            private int WGS84_LAT;
            @SerializedName("X_CRDNT")
            private int X_CRDNT;
            @SerializedName("Y_CRDNT")
            private int Y_CRDNT;
            @SerializedName("DATA_COLCT_DE")
            private String DATA_COLCT_DE;
            @SerializedName("ETL_LDADNG_DTM")
            private String ETL_LDADNG_DTM;
            @SerializedName("LAT")
            private String LAT;
            @SerializedName("LNG")
            private String LNG;

            public int getNO() {
                return NO;
            }

            public void setNO(int NO) {
                this.NO = NO;
            }

            public String getSIGUN_CD() {
                return SIGUN_CD;
            }

            public void setSIGUN_CD(String SIGUN_CD) {
                this.SIGUN_CD = SIGUN_CD;
            }

            public String getSIGUN_NM() {
                return SIGUN_NM;
            }

            public void setSIGUN_NM(String SIGUN_NM) {
                this.SIGUN_NM = SIGUN_NM;
            }

            public String getBIZPLC_NM() {
                return BIZPLC_NM;
            }

            public void setBIZPLC_NM(String BIZPLC_NM) {
                this.BIZPLC_NM = BIZPLC_NM;
            }

            public String getLOCPLC_LOTNO_ADDR() {
                return LOCPLC_LOTNO_ADDR;
            }

            public void setLOCPLC_LOTNO_ADDR(String LOCPLC_LOTNO_ADDR) {
                this.LOCPLC_LOTNO_ADDR = LOCPLC_LOTNO_ADDR;
            }

            public String getLOCPLC_ROADNM_ADDR() {
                return LOCPLC_ROADNM_ADDR;
            }

            public void setLOCPLC_ROADNM_ADDR(String LOCPLC_ROADNM_ADDR) {
                this.LOCPLC_ROADNM_ADDR = LOCPLC_ROADNM_ADDR;
            }

            public String getLICENSG_DE() {
                return LICENSG_DE;
            }

            public void setLICENSG_DE(String LICENSG_DE) {
                this.LICENSG_DE = LICENSG_DE;
            }

            public String getBSN_STATE_NM() {
                return BSN_STATE_NM;
            }

            public void setBSN_STATE_NM(String BSN_STATE_NM) {
                this.BSN_STATE_NM = BSN_STATE_NM;
            }

            public String getCLSBIZ_DE() {
                return CLSBIZ_DE;
            }

            public void setCLSBIZ_DE(String CLSBIZ_DE) {
                this.CLSBIZ_DE = CLSBIZ_DE;
            }

            public String getSUSPNBIZ_BEGIN_DE() {
                return SUSPNBIZ_BEGIN_DE;
            }

            public void setSUSPNBIZ_BEGIN_DE(String SUSPNBIZ_BEGIN_DE) {
                this.SUSPNBIZ_BEGIN_DE = SUSPNBIZ_BEGIN_DE;
            }

            public String getSUSPNBIZ_END_DE() {
                return SUSPNBIZ_END_DE;
            }

            public void setSUSPNBIZ_END_DE(String SUSPNBIZ_END_DE) {
                this.SUSPNBIZ_END_DE = SUSPNBIZ_END_DE;
            }

            public String getREOPENBIZ_DE() {
                return REOPENBIZ_DE;
            }

            public void setREOPENBIZ_DE(String REOPENBIZ_DE) {
                this.REOPENBIZ_DE = REOPENBIZ_DE;
            }

            public int getLOCPLC_AR() {
                return LOCPLC_AR;
            }

            public void setLOCPLC_AR(int LOCPLC_AR) {
                this.LOCPLC_AR = LOCPLC_AR;
            }

            public String getLOCPLC_ZIP_CD() {
                return LOCPLC_ZIP_CD;
            }

            public void setLOCPLC_ZIP_CD(String LOCPLC_ZIP_CD) {
                this.LOCPLC_ZIP_CD = LOCPLC_ZIP_CD;
            }

            public String getFACLT_BULDNG_NM() {
                return FACLT_BULDNG_NM;
            }

            public void setFACLT_BULDNG_NM(String FACLT_BULDNG_NM) {
                this.FACLT_BULDNG_NM = FACLT_BULDNG_NM;
            }

            public String getRELEASE_DE() {
                return RELEASE_DE;
            }

            public void setRELEASE_DE(String RELEASE_DE) {
                this.RELEASE_DE = RELEASE_DE;
            }

            public String getEMGNCY_FACLT_DIV_NM() {
                return EMGNCY_FACLT_DIV_NM;
            }

            public void setEMGNCY_FACLT_DIV_NM(String EMGNCY_FACLT_DIV_NM) {
                this.EMGNCY_FACLT_DIV_NM = EMGNCY_FACLT_DIV_NM;
            }

            public int getWGS84_LOGT() {
                return WGS84_LOGT;
            }

            public void setWGS84_LOGT(int WGS84_LOGT) {
                this.WGS84_LOGT = WGS84_LOGT;
            }

            public int getWGS84_LAT() {
                return WGS84_LAT;
            }

            public void setWGS84_LAT(int WGS84_LAT) {
                this.WGS84_LAT = WGS84_LAT;
            }

            public int getX_CRDNT() {
                return X_CRDNT;
            }

            public void setX_CRDNT(int X_CRDNT) {
                this.X_CRDNT = X_CRDNT;
            }

            public int getY_CRDNT() {
                return Y_CRDNT;
            }

            public void setY_CRDNT(int Y_CRDNT) {
                this.Y_CRDNT = Y_CRDNT;
            }

            public String getDATA_COLCT_DE() {
                return DATA_COLCT_DE;
            }

            public void setDATA_COLCT_DE(String DATA_COLCT_DE) {
                this.DATA_COLCT_DE = DATA_COLCT_DE;
            }

            public String getETL_LDADNG_DTM() {
                return ETL_LDADNG_DTM;
            }

            public void setETL_LDADNG_DTM(String ETL_LDADNG_DTM) {
                this.ETL_LDADNG_DTM = ETL_LDADNG_DTM;
            }

            public String getLAT() {
                return LAT;
            }

            public void setLAT(String LAT) {
                this.LAT = LAT;
            }

            public String getLNG() {
                return LNG;
            }

            public void setLNG(String LNG) {
                this.LNG = LNG;
            }
        }
    }
}
