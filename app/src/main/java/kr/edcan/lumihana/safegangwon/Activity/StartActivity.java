package kr.edcan.lumihana.safegangwon.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by kimok_000 on 2016-09-18.
 */
public class StartActivity extends AppCompatActivity {
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor spEditor;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        startActivity(new Intent(getApplicationContext(), SignInActivity.class));
        startActivity(new Intent(getApplicationContext(), IntroActivity.class));
        finish();
    }



    @Override
    protected void onResume() {
        super.onResume();
    }
}
