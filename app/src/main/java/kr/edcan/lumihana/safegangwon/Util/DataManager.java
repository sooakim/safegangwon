package kr.edcan.lumihana.safegangwon.Util;

import android.content.Context;
import android.util.Log;

import kr.edcan.lumihana.safegangwon.Model.EmergencyNoticeModel;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by kimok_000 on 2016-09-23.
 */
public class DataManager {
    private static final String CLASS_NAME = "DataManager";
    private String apiKey;
    private Context context;
    private Retrofit retrofit;
    private IDataService dataService;
    private Call<EmergencyNoticeModel> emergencyNotice;
    private OnEmergencyNoticeChangedListener onEmergencyNoticeChangedListener;

    public static final String TYPE_XML = "xml";
    public static final String TYPE_JSON = "json";

    public DataManager(Context context, String apiKey) {
        this.context = context;
        this.apiKey = apiKey;

        initRetrofit();
    }


    private void initRetrofit() {
        retrofit = new Retrofit.Builder()
                .baseUrl("http://data.mpss.go.kr/openapi/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        dataService = retrofit.create(IDataService.class);
    }

    public void setOnEmergencyNoticeChangedListener(OnEmergencyNoticeChangedListener onEmergencyNoticeChangedListener) {
        this.onEmergencyNoticeChangedListener = onEmergencyNoticeChangedListener;
    }

    public void getEmergencyNotice(int startIndex, int endIndex) {
        emergencyNotice = dataService.emergencyNotice(apiKey, TYPE_JSON, startIndex, endIndex);
        emergencyNotice.enqueue(new Callback<EmergencyNoticeModel>() {
            @Override
            public void onResponse(Response<EmergencyNoticeModel> response, Retrofit retrofit) {
                if (response.code() == 200) {
                    onEmergencyNoticeChangedListener.emergenctNoticeChanged(response.body());
                } else Log.e(CLASS_NAME, "MPSS Server return !200");
            }

            @Override
            public void onFailure(Throwable t) {
                Log.e(CLASS_NAME, "Failed to get emergencyInfo");
                Log.e(CLASS_NAME, t.getMessage() + "");
            }
        });
    }

    public interface OnEmergencyNoticeChangedListener {
        void emergenctNoticeChanged(EmergencyNoticeModel emergencyNoticeModel);
    }
}
