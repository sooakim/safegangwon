package kr.edcan.lumihana.safegangwon.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import java.util.ArrayList;

import kr.edcan.lumihana.safegangwon.Activity.ListActivity;
import kr.edcan.lumihana.safegangwon.LiveModel.LiveCivilDefenseModel;
import kr.edcan.lumihana.safegangwon.LiveModel.LiveEarthModel;
import kr.edcan.lumihana.safegangwon.LiveModel.LiveEmergencyCenterModel;
import kr.edcan.lumihana.safegangwon.LiveModel.LiveEmergencyNoticeModel;
import kr.edcan.lumihana.safegangwon.LiveModel.LiveGeneralHospitalModel;
import kr.edcan.lumihana.safegangwon.LiveModel.LiveHospitalModel;
import kr.edcan.lumihana.safegangwon.LiveModel.LiveSnowModel;
import kr.edcan.lumihana.safegangwon.LiveModel.LiveWaterSupplyModel;
import kr.edcan.lumihana.safegangwon.LiveModel.LiveWeatherModel;
import kr.edcan.lumihana.safegangwon.LiveModel.ParentModel;
import kr.edcan.lumihana.safegangwon.R;

/**
 * Created by kimok_000 on 2016-09-20.
 */
public class LiveAdapter extends RecyclerView.Adapter<LiveAdapter.ViewHolder> {
    private Context context;
    private ArrayList<ParentModel> arrayList;

    public LiveAdapter(Context context, ArrayList<ParentModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public LiveAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_live, parent, false);
        view.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ParentModel parentModel = arrayList.get(position);

        switch (parentModel.getType()) {
            case ParentModel.TYPE_WEATHER: {
                final LiveWeatherModel liveWeatherModel = (LiveWeatherModel) parentModel;
                holder.frame_etc.setVisibility(View.GONE);
                holder.frame_weather.setVisibility(View.VISIBLE);

                holder.text_weather.setText(liveWeatherModel.getNameSky());
                String warn = "";
                if (liveWeatherModel.getAlertYn().equals("Y")) warn += "특보 있음";
                else warn += "특보 없음";
                if (liveWeatherModel.getStromYn().equals("Y")) warn += " / 태풍";
                else warn += "";
                holder.text_warn.setText(warn);

                final String temperature = "최고 " + liveWeatherModel.getTmax() + "도 / 최저 " + liveWeatherModel.getTmin() + "도";
                holder.text_temperature.setText(temperature);

                holder.frame_weather.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Intent intent = new Intent(context, ListActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                .putExtra("CLASS_TYPE", ParentModel.TYPE_WEATHER);
                        context.startActivity(intent);
                    }
                });
                break;
            }
            case ParentModel.TYPE_EMERGENCY_NOTICE: {
                final String type = "재난 대응 상황";
                final LiveEmergencyNoticeModel liveEmergencyNoticeModel = (LiveEmergencyNoticeModel) parentModel;
                holder.text_type.setText(type);
                holder.text_title.setText(liveEmergencyNoticeModel.getClmy_pttn_nm());
                holder.text_content.setText(liveEmergencyNoticeModel.getTitl());

                holder.frame_etc.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Intent intent = new Intent(context, ListActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                .putExtra("CLASS_TYPE", parentModel.getType())
                                .putExtra("what", type);
                        context.startActivity(intent);
                    }
                });
                break;
            }
            case ParentModel.TYPE_SNOW: {
                final String type = "적설 감시";
                final LiveSnowModel liveSnowModel = (LiveSnowModel) parentModel;
                holder.text_type.setText(type);
                holder.text_title.setText(liveSnowModel.getCaname());
                holder.text_content.setText(liveSnowModel.getAddr());

                holder.frame_etc.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Intent intent = new Intent(context, ListActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                .putExtra("CLASS_TYPE", parentModel.getType())
                                .putExtra("what", type);

                        context.startActivity(intent);
                    }
                });
                break;
            }
            case ParentModel.TYPE_EARTH: {
                final String type = "지진 계측소";
                final LiveEarthModel liveEarthModel = (LiveEarthModel) parentModel;
                holder.text_type.setText(type);
                holder.text_title.setText(liveEarthModel.getObs_nm());
                holder.text_content.setText(liveEarthModel.getLocplc_lonto_addr());

                holder.frame_etc.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Intent intent = new Intent(context, ListActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                .putExtra("CLASS_TYPE", parentModel.getType())
                                .putExtra("what", type);

                        context.startActivity(intent);
                    }
                });
                break;
            }
            case ParentModel.TYPE_CIVIL_DEFENSE: {
                final String type = "민방위 대피소";
                final LiveCivilDefenseModel liveCivilDefenseModel = (LiveCivilDefenseModel) parentModel;
                holder.text_type.setText(type);
                holder.text_title.setText(liveCivilDefenseModel.getBizplc_nm());
                holder.text_content.setText(liveCivilDefenseModel.getLocplc_lonto_addr());

                holder.frame_etc.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Intent intent = new Intent(context, ListActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                .putExtra("CLASS_TYPE", parentModel.getType())
                                .putExtra("what", type);

                        context.startActivity(intent);
                    }
                });
                break;
            }
            case ParentModel.TYPE_WATER_SUPPLY: {
                final String type = "급수 시설";
                final LiveWaterSupplyModel liveWaterSupplyModel = (LiveWaterSupplyModel) parentModel;
                holder.text_type.setText(type);
                holder.text_title.setText(liveWaterSupplyModel.getBizplc_nm());
                holder.text_content.setText(liveWaterSupplyModel.getLocplc_lotno_addr());

                holder.frame_etc.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Intent intent = new Intent(context, ListActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                .putExtra("CLASS_TYPE", parentModel.getType())
                                .putExtra("what", type);

                        context.startActivity(intent);
                    }
                });
                break;
            }
            case ParentModel.TYPE_EMERGENCY_CENTER: {
                final String type = "응급 센터";
                final LiveEmergencyCenterModel liveEmergencyCenterModel = (LiveEmergencyCenterModel) parentModel;
                holder.text_type.setText(type);
                holder.text_title.setText(liveEmergencyCenterModel.getBizplc_nm());
                holder.text_content.setText(liveEmergencyCenterModel.getLocplc_lotno_addr());

                holder.frame_etc.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Intent intent = new Intent(context, ListActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                .putExtra("CLASS_TYPE", parentModel.getType())
                                .putExtra("what", type);

                        context.startActivity(intent);
                    }
                });
                break;
            }
            case ParentModel.TYPE_GENERAL_HOSPITAL: {
                final String type = "종합 병원";
                final LiveGeneralHospitalModel liveGeneralHospitalModel = (LiveGeneralHospitalModel) parentModel;
                holder.text_type.setText(type);
                holder.text_title.setText(liveGeneralHospitalModel.getBizplc_nm());
                holder.text_content.setText(liveGeneralHospitalModel.getLocplc_lonto_addr());

                holder.frame_etc.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Intent intent = new Intent(context, ListActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                .putExtra("CLASS_TYPE", parentModel.getType())
                                .putExtra("what", type);

                        context.startActivity(intent);
                    }
                });
                break;
            }
            case ParentModel.TYPE_HOSPITAL: {
                final String type = "병원";
                final LiveHospitalModel liveHospitalModel = (LiveHospitalModel) parentModel;
                holder.text_type.setText(type);
                holder.text_title.setText(liveHospitalModel.getBizplc_nm());
                holder.text_content.setText(liveHospitalModel.getLocplc_lonto_addr());

                holder.frame_etc.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final Intent intent = new Intent(context, ListActivity.class)
                                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                                .putExtra("CLASS_TYPE", parentModel.getType())
                                .putExtra("what", type);

                        context.startActivity(intent);
                    }
                });
                break;
            }
            default: {
                Log.e("LiveAdapter", "Unkown Data received!!");
                break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return this.arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private FrameLayout frame_weather, frame_etc;
        private TextView text_type, text_title, text_content,   //etc
                text_weather, text_warn, text_temperature;    //weather

        public ViewHolder(View itemView) {
            super(itemView);

            frame_weather = (FrameLayout) itemView.findViewById(R.id.live_frame_weather);
            text_weather = (TextView) itemView.findViewById(R.id.live_text_weather);
            text_warn = (TextView) itemView.findViewById(R.id.live_text_warn);
            text_temperature = (TextView) itemView.findViewById(R.id.live_text_temperature);

            frame_etc = (FrameLayout) itemView.findViewById(R.id.live_frame_etc);
            text_type = (TextView) itemView.findViewById(R.id.live_text_type);
            text_title = (TextView) itemView.findViewById(R.id.live_text_title);
            text_content = (TextView) itemView.findViewById(R.id.live_text_content);
        }
    }
}
