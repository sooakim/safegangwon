package kr.edcan.lumihana.safegangwon.Model;

/**
 * Created by kimok_000 on 2016-09-25.
 */
public class DataModel {
    private String title;
    private String content;

    public DataModel() {
    }

    public DataModel(String title, String content) {
        this.title = title;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
