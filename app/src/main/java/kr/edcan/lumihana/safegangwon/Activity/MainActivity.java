package kr.edcan.lumihana.safegangwon.Activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;
import kr.edcan.lumihana.safegangwon.Adapter.LiveAdapter;
import kr.edcan.lumihana.safegangwon.LiveModel.LiveCivilDefenseModel;
import kr.edcan.lumihana.safegangwon.LiveModel.LiveEarthModel;
import kr.edcan.lumihana.safegangwon.LiveModel.LiveEmergencyCenterModel;
import kr.edcan.lumihana.safegangwon.LiveModel.LiveEmergencyNoticeModel;
import kr.edcan.lumihana.safegangwon.LiveModel.LiveGeneralHospitalModel;
import kr.edcan.lumihana.safegangwon.LiveModel.LiveHospitalModel;
import kr.edcan.lumihana.safegangwon.LiveModel.LiveSnowModel;
import kr.edcan.lumihana.safegangwon.LiveModel.LiveWaterSupplyModel;
import kr.edcan.lumihana.safegangwon.LiveModel.LiveWeatherModel;
import kr.edcan.lumihana.safegangwon.LiveModel.ParentModel;
import kr.edcan.lumihana.safegangwon.Model.GPSModel;
import kr.edcan.lumihana.safegangwon.R;
import kr.edcan.lumihana.safegangwon.RealmModel.CivilDefenseModel;
import kr.edcan.lumihana.safegangwon.RealmModel.EarthModel;
import kr.edcan.lumihana.safegangwon.RealmModel.EmergencyCenterModel;
import kr.edcan.lumihana.safegangwon.RealmModel.EmergencyNoticeModel;
import kr.edcan.lumihana.safegangwon.RealmModel.GeneralHospitalModel;
import kr.edcan.lumihana.safegangwon.RealmModel.HospitalModel;
import kr.edcan.lumihana.safegangwon.RealmModel.SnowModel;
import kr.edcan.lumihana.safegangwon.RealmModel.WaterSupplyModel;
import kr.edcan.lumihana.safegangwon.RealmModel.WeatherModel;
import kr.edcan.lumihana.safegangwon.Util.DBManager;
import kr.edcan.lumihana.safegangwon.Util.GPSManager;
import kr.edcan.lumihana.safegangwon.Util.GangwonManager;

public class MainActivity extends AppCompatActivity implements GPSManager.OnLocationChangedListener {
    private Toolbar toolbar;
    private RecyclerView recycler_live;
    private SwipeRefreshLayout refresh_live;
    private LinearLayoutManager linearLayoutManager;
    private ArrayList<ParentModel> arrayList;
    private LiveAdapter liveAdapter;
    private DBManager dbManager;
    private GPSManager gpsManager;
    private SharedPreferences sharedPreferences;
    private String sigungu = "양양군";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initSharedPreferences();
        setToolBar();
        setRecycler();
        setRefresh();
        initFireMessage();
        initDBManager();
        getData();
        loadFromDB();
    }


    private void initSharedPreferences() {
        sharedPreferences = getSharedPreferences("Setting", MODE_PRIVATE);
        sigungu = sharedPreferences.getString("region", "춘천시");
    }

    private void loadFromDB() {
        arrayList.clear();
        Realm realm = Realm.getDefaultInstance();

        //Weather Connect
        RealmResults<WeatherModel> weatherResults = realm.where(WeatherModel.class).findAll();
        if (weatherResults.size() <= 0) Log.e("Main", "Weather 로딩 실패");
        else {
            final WeatherModel model = weatherResults.get(0);
            final LiveWeatherModel liveModel = new LiveWeatherModel(model.getAlertYn(), model.getStormYn(), model.getTmaxTemperature(), model.getTminTemperature(), model.getNameSky());
            arrayList.add(liveModel);
            liveAdapter.notifyItemInserted(arrayList.size());
        }

        //EmergencyNotice Connect
        RealmResults<EmergencyNoticeModel> emergencyNoticeModels = realm.where(EmergencyNoticeModel.class).findAll().sort("inpt_date", Sort.DESCENDING);
        if (emergencyNoticeModels.size() <= 0) Log.e("Main", "EmergencyNotice 로딩 실패");
        else {
            final EmergencyNoticeModel model = emergencyNoticeModels.get(0);
            final LiveEmergencyNoticeModel liveModel = new LiveEmergencyNoticeModel(model.getClmy_pttn_nm(), model.getTitl());
            arrayList.add(liveModel);
            liveAdapter.notifyItemInserted(arrayList.size());
        }

        //Snow Connect
        RealmResults<SnowModel> snowResults = realm.where(SnowModel.class).contains("ADDR", sigungu).findAll();
        if (snowResults.size() <= 0) {
            Log.e("Main", "Snow 근처 데이터 못 찾음");
            RealmResults<SnowModel> snowResults2 = realm.where(SnowModel.class).findAll();
            if (snowResults2.size() <= 0) Log.e("Main", "Snow 로딩 실패");
            else {
                final SnowModel model = snowResults2.get(0);
                final LiveSnowModel liveModel = new LiveSnowModel(model.getCANAME(), model.getADDR());
                arrayList.add(liveModel);
                liveAdapter.notifyItemInserted(arrayList.size());
            }
        } else {
            final SnowModel model = snowResults.get(0);
            final LiveSnowModel liveModel = new LiveSnowModel(model.getCANAME(), model.getADDR());
            arrayList.add(liveModel);
            liveAdapter.notifyItemInserted(arrayList.size());
        }

        //Earh Connect
        RealmResults<EarthModel> earthResults = realm.where(EarthModel.class).contains("LOCPLC_LOTNO_ADDR", sigungu).findAll();
        if (earthResults.size() <= 0) {
            Log.e("Main", "Earth 근처 데이터 못 찾음");
            RealmResults<EarthModel> earthResults2 = realm.where(EarthModel.class).findAll();
            if (earthResults2.size() <= 0) Log.e("Main", "Earth 로딩 실패");
            else {
                final EarthModel model = earthResults2.get(0);
                final LiveEarthModel liveModel = new LiveEarthModel(model.getOBS_NM(), model.getLOCPLC_LOTNO_ADDR());
                arrayList.add(liveModel);
                liveAdapter.notifyItemInserted(arrayList.size());
            }
        } else {
            final EarthModel model = earthResults.get(0);
            final LiveEarthModel liveModel = new LiveEarthModel(model.getOBS_NM(), model.getLOCPLC_LOTNO_ADDR());
            arrayList.add(liveModel);
            liveAdapter.notifyItemInserted(arrayList.size());
        }

        //Civil Connect
        RealmResults<CivilDefenseModel> civilDefenseResults = realm.where(CivilDefenseModel.class).contains("LOCPLC_LOTNO_ADDR", sigungu).findAll();
        if (civilDefenseResults.size() <= 0) {
            Log.e("Main", "CivilDefense 근처 데이터 못 찾음");
            RealmResults<CivilDefenseModel> civilDefenseResults2 = realm.where(CivilDefenseModel.class).findAll();
            if (civilDefenseResults2.size() <= 0) Log.e("Main", "CivilDefense 로딩 실패");
            else {
                final CivilDefenseModel model = civilDefenseResults2.get(0);
                final LiveCivilDefenseModel liveModel = new LiveCivilDefenseModel(model.getBIZPLC_NM(), model.getLOCPLC_LOTNO_ADDR());
                arrayList.add(liveModel);
                liveAdapter.notifyItemInserted(arrayList.size());
            }
        } else {
            final CivilDefenseModel model = civilDefenseResults.get(0);
            final LiveCivilDefenseModel liveModel = new LiveCivilDefenseModel(model.getBIZPLC_NM(), model.getLOCPLC_LOTNO_ADDR());
            arrayList.add(liveModel);
            liveAdapter.notifyItemInserted(arrayList.size());
        }

        //WaterSupply Connect
        RealmResults<WaterSupplyModel> waterSupplyResults = realm.where(WaterSupplyModel.class).contains("LOCPLC_LOTNO_ADDR", sigungu).findAll();
        if (waterSupplyResults.size() <= 0) {
            Log.e("Main", "WaterSupply 근처 데이터 못 찾음");
            RealmResults<WaterSupplyModel> waterSupplyResults2 = realm.where(WaterSupplyModel.class).findAll();
            if (waterSupplyResults2.size() <= 0) Log.e("Main", "WaterSupply 로딩 실패");
            else {
                final WaterSupplyModel model = waterSupplyResults2.get(0);
                final LiveWaterSupplyModel liveModel = new LiveWaterSupplyModel(model.getBIZPLC_NM(), model.getLOCPLC_LOTNO_ADDR());
                arrayList.add(liveModel);
                liveAdapter.notifyItemInserted(arrayList.size());
            }
        } else {
            final WaterSupplyModel model = waterSupplyResults.get(0);
            final LiveWaterSupplyModel liveModel = new LiveWaterSupplyModel(model.getBIZPLC_NM(), model.getLOCPLC_LOTNO_ADDR());
            arrayList.add(liveModel);
            liveAdapter.notifyItemInserted(arrayList.size());
        }

        //EmergencyCenter Connect
        RealmResults<EmergencyCenterModel> emergencyCenterResults = realm.where(EmergencyCenterModel.class).contains("LOCPLC_LOTNO_ADDR", sigungu).findAll();
        if (emergencyCenterResults.size() <= 0) {
            Log.e("Main", "EmergencyCenter 근처 데이터 못 찾음");
            RealmResults<EmergencyCenterModel> emergencyCenterResults2 = realm.where(EmergencyCenterModel.class).findAll();
            if (emergencyCenterResults2.size() <= 0) Log.e("Main", "EmergencyCenter 로딩 실패");
            else {
                final EmergencyCenterModel model = emergencyCenterResults2.get(0);
                final LiveEmergencyCenterModel liveModel = new LiveEmergencyCenterModel(model.getBIZPLC_NM(), model.getLOCPLC_LOTNO_ADDR());
                arrayList.add(liveModel);
                liveAdapter.notifyItemInserted(arrayList.size());
            }
        } else {
            final EmergencyCenterModel model = emergencyCenterResults.get(0);
            final LiveEmergencyCenterModel liveModel = new LiveEmergencyCenterModel(model.getBIZPLC_NM(), model.getLOCPLC_LOTNO_ADDR());
            arrayList.add(liveModel);
            liveAdapter.notifyItemInserted(arrayList.size());
        }

        //GeneralHospital Connect
        RealmResults<GeneralHospitalModel> generalHospitalResults = realm.where(GeneralHospitalModel.class).contains("LOCPLC_LOTNO_ADDR", sigungu).findAll();
        if (generalHospitalResults.size() <= 0) {
            Log.e("Main", "GeneralHospital 근처 데이터 못 찾음");
            RealmResults<GeneralHospitalModel> generalHospitalResults2 = realm.where(GeneralHospitalModel.class).findAll();
            if (generalHospitalResults2.size() <= 0) Log.e("Main", "GeneralHospital 로딩 실패");
            else {
                final GeneralHospitalModel model = generalHospitalResults2.get(0);
                final LiveGeneralHospitalModel liveModel = new LiveGeneralHospitalModel(model.getBIZPLC_NM(), model.getLOCPLC_LOTNO_ADDR());
                arrayList.add(liveModel);
                liveAdapter.notifyItemInserted(arrayList.size());
            }
        } else {
            final GeneralHospitalModel model = generalHospitalResults.get(0);
            final LiveGeneralHospitalModel liveModel = new LiveGeneralHospitalModel(model.getBIZPLC_NM(), model.getLOCPLC_LOTNO_ADDR());
            arrayList.add(liveModel);
            liveAdapter.notifyItemInserted(arrayList.size());
        }

        //Hospital Connect
        RealmResults<HospitalModel> hospitalResults = realm.where(HospitalModel.class).contains("LOCPLC_LOTNO_ADDR", sigungu).findAll();
        if (hospitalResults.size() <= 0) {
            Log.e("Main", "Hospital 근처 데이터 못 찾음");
            RealmResults<HospitalModel> hospitalResults2 = realm.where(HospitalModel.class).findAll();
            if (hospitalResults2.size() <= 0) Log.e("Main", "Hospital 로딩 실패");
            else {
                final HospitalModel model = hospitalResults2.get(0);
                final LiveHospitalModel liveModel = new LiveHospitalModel(model.getBIZPLC_NM(), model.getLOCPLC_LOTNO_ADDR());
                arrayList.add(liveModel);
                liveAdapter.notifyItemInserted(arrayList.size());
            }
        } else {
            final HospitalModel model = hospitalResults.get(0);
            final LiveHospitalModel liveModel = new LiveHospitalModel(model.getBIZPLC_NM(), model.getLOCPLC_LOTNO_ADDR());
            arrayList.add(liveModel);
            liveAdapter.notifyItemInserted(arrayList.size());
        }
        liveAdapter.notifyDataSetChanged();
        realm.close();
    }

    private void initFireMessage() {
        FirebaseMessaging.getInstance().subscribeToTopic("news");
        FirebaseInstanceId.getInstance().getToken();
    }

    private void initDBManager() {
        dbManager = new DBManager(getApplicationContext());
    }

    private void setToolBar() {
        toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        toolbar.setTitle(R.string.app_name);
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        setSupportActionBar(toolbar);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.setting: {
                final Intent intent = new Intent(getApplicationContext(), SettingActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
            }
            case R.id.post: {
                final Intent intent = new Intent(getApplicationContext(), PostListActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private void setRecycler() {
        arrayList = new ArrayList<>();
        recycler_live = (RecyclerView) findViewById(R.id.main_recycler_live);
        recycler_live.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycler_live.setLayoutManager(linearLayoutManager);

        liveAdapter = new LiveAdapter(getApplicationContext(), arrayList);
        recycler_live.setAdapter(liveAdapter);
    }

    private void setRefresh() {
        refresh_live = (SwipeRefreshLayout) findViewById(R.id.main_refresh_live);
        refresh_live.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                arrayList.clear();
                liveAdapter.notifyDataSetChanged();
                loadFromDB();
                refresh_live.setRefreshing(false);
            }
        });
    }

    private void getData() {
        dbManager.getSnow(1, 100);
        dbManager.getEarth(GangwonManager.SERVICE_EARTH_YANGYANG, 1, 100);
        dbManager.getCivilDefense(1, 93);
        dbManager.getEmergencyCenter(1, 100);
        dbManager.getHospital(1, 100);
        dbManager.getWaterSupply(1, 100);
        dbManager.getWeatherWithAddress("강원", "속초시", "금호동");
        dbManager.getEmergencyNotice(1, 100);
        dbManager.getGeneralHospital(1, 100);
    }

    @Override
    protected void onResume() {
        super.onResume();
        sigungu = sharedPreferences.getString("region", "춘천시");
        loadFromDB();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        sigungu = sharedPreferences.getString("region", "춘천시");
        loadFromDB();
    }

    @Override
    public void onLocationChanged(GPSModel gpsModel) {
    }
}

