package kr.edcan.lumihana.safegangwon.Util;

import com.squareup.okhttp.ResponseBody;

import retrofit.Call;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by kimok_000 on 2016-09-23.
 */
public interface IFireMessageService {
    @POST("/registertoken")
    @FormUrlEncoded
    Call<ResponseBody> registerToken(@Field("token") String token, @Field("id") String id);
}
