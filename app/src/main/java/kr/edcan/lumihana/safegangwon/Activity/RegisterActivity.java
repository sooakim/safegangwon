package kr.edcan.lumihana.safegangwon.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import kr.edcan.lumihana.safegangwon.R;
import kr.edcan.lumihana.safegangwon.Util.NetworkManager;

public class RegisterActivity extends AppCompatActivity {
    private static final String CLASS_NAME = "RegisterActivity";
    private FrameLayout frame_root;
    private FloatingActionButton floating_next;
    private EditText edit_email, edit_password, edit_repassword;
    private FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        init();
        initFirebase();
        checkNetwork();
    }

    private void initFirebase() {
        firebaseAuth = FirebaseAuth.getInstance();
    }

    private void onRegister(final String email, final String password, final String repassword) {
        if (email.toString().trim().equals("") || password.toString().trim().equals("") || repassword.toString().trim().equals("")) {
            Snackbar.make(frame_root, "제대로 입력했는지 다시 확인해주세요", Snackbar.LENGTH_SHORT).show();
            return;
        } else if (!password.toString().trim().equals(repassword.toString().trim())) {
            Snackbar.make(frame_root, "재확인과 비밀번호의 입력값이 일치하지 않습니다.", Snackbar.LENGTH_SHORT).show();
            return;
        }

        firebaseAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            final FirebaseUser uesr = task.getResult().getUser();
                            final Intent intent = new Intent();
                            intent.putExtra("Email", email);
                            intent.putExtra("Password", password);
                            setResult(RESULT_OK, intent);
                            Log.e(CLASS_NAME, "register success");
                            finish();
                        } else {
                            Snackbar.make(frame_root, task.getException().getLocalizedMessage(), Snackbar.LENGTH_SHORT).show();
                            setResult(RESULT_CANCELED);
                            Log.e(CLASS_NAME, "register failed");
                        }
                    }
                });
    }

    private void init() {
        frame_root = (FrameLayout) findViewById(R.id.register_frame_root);
        floating_next = (FloatingActionButton) findViewById(R.id.register_floating_next);
        edit_email = (EditText) findViewById(R.id.register_edit_email);
        edit_password = (EditText) findViewById(R.id.register_edit_password);
        edit_repassword = (EditText) findViewById(R.id.register_edit_repassword);

        floating_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email = edit_email.getText().toString();
                final String password = edit_password.getText().toString();
                final String repassword = edit_repassword.getText().toString();

                onRegister(email, password, repassword);
            }
        });
    }

    private boolean checkNetwork() {
        if (NetworkManager.getConnectivityStatus(getApplicationContext()) == NetworkManager.TYPE_NOT_CONNECTED) {
            floating_next.setEnabled(false);

            Snackbar.make(frame_root, "네트워크 연결을 확인하세요.", Snackbar.LENGTH_LONG)
                    .setAction("연결", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final Intent intent = new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    })
                    .show();
            return false;
        } else {
            floating_next.setEnabled(true);
            return true;
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        checkNetwork();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkNetwork();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        firebaseAuth = null;
    }
}
