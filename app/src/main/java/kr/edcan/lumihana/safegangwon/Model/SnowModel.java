package kr.edcan.lumihana.safegangwon.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import kr.edcan.lumihana.safegangwon.LiveModel.ParentModel;

/**
 * Created by kimok_000 on 2016-09-22.
 */
public class SnowModel extends ParentModel {

    /**
     * list_total_count : 32
     * RESULT : {"CODE":"INFO-000","MESSAGE":"정상 처리되었습니다."}
     * row : [{"SIDO":"???","SIGUNGU":"평창군","CANAME":"평창 봉평면 구영동2터널","PROD":"웹게이트\t\t","MODELN":"B101[Webeye 1.X]","CTYPE":"적설감시","ADDR":"평창군 봉평면 면온리 산195-2\t","STREAMADDR":"http://www.safety.go.kr/video/view460.jsp?srcTarget=9&cctvCode=11PC_18&cctvId=42002"},{"SIDO":"???","SIGUNGU":"홍천군","CANAME":"홍천 내면 율전리 행치령","PROD":"웹게이트\t\t","MODELN":"B101[Webeye 1.X]","CTYPE":"적설감시","ADDR":"홍천군 서석면 수하리 산27-33\t","STREAMADDR":"http://www.safety.go.kr/video/view460.jsp?srcTarget=10&cctvCode=08HC_14&cctvId=42002"},{"SIDO":"???","SIGUNGU":"홍천군","CANAME":"홍천 서면 굴업리 백양치","PROD":"웹게이트\t\t","MODELN":"B101[Webeye 1.X]","CTYPE":"적설감시","ADDR":"홍천군 남면 화전리 산223-2\t\t","STREAMADDR":"http://www.safety.go.kr/video/view460.jsp?srcTarget=11&cctvCode=08HC_11&cctvId=42002"},{"SIDO":"???","SIGUNGU":"인제군","CANAME":"인제 북면 월학리 칠성고개","PROD":"삼성테크윈\t","MODELN":"SPE-400[Default]","CTYPE":"적설감시","ADDR":"인제군 북면 월학리 351-4\t\t\t","STREAMADDR":"http://www.safety.go.kr/video/view460.jsp?srcTarget=13&cctvCode=16IJ_27&cctvId=42002"},{"SIDO":"???","SIGUNGU":"원주시","CANAME":"원주 신림면 황둔리 신림터널","PROD":"삼성테크윈\t","MODELN":"SPE-400[Default]","CTYPE":"적설감시","ADDR":"원주시 신림면 황둔리 1813-5\t\t","STREAMADDR":"http://www.safety.go.kr/video/view460.jsp?srcTarget=14&cctvCode=02WJ_3&cctvId=42002"}]
     */

    @SerializedName("gwcgcom-disaster_snow_image")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        @SerializedName("list_total_count")
        private int list_total_count;
        /**
         * CODE : INFO-000
         * MESSAGE : 정상 처리되었습니다.
         */

        @SerializedName("RESULT")
        private RESULTBean RESULT;
        /**
         * SIDO : ???
         * SIGUNGU : 평창군
         * CANAME : 평창 봉평면 구영동2터널
         * PROD : 웹게이트
         * MODELN : B101[Webeye 1.X]
         * CTYPE : 적설감시
         * ADDR : 평창군 봉평면 면온리 산195-2
         * STREAMADDR : http://www.safety.go.kr/video/view460.jsp?srcTarget=9&cctvCode=11PC_18&cctvId=42002
         */

        @SerializedName("row")
        private List<RowBean> row;

        public int getList_total_count() {
            return list_total_count;
        }

        public void setList_total_count(int list_total_count) {
            this.list_total_count = list_total_count;
        }

        public RESULTBean getRESULT() {
            return RESULT;
        }

        public void setRESULT(RESULTBean RESULT) {
            this.RESULT = RESULT;
        }

        public List<RowBean> getRow() {
            return row;
        }

        public void setRow(List<RowBean> row) {
            this.row = row;
        }

        public static class RESULTBean {
            @SerializedName("CODE")
            private String CODE;
            @SerializedName("MESSAGE")
            private String MESSAGE;

            public String getCODE() {
                return CODE;
            }

            public void setCODE(String CODE) {
                this.CODE = CODE;
            }

            public String getMESSAGE() {
                return MESSAGE;
            }

            public void setMESSAGE(String MESSAGE) {
                this.MESSAGE = MESSAGE;
            }
        }

        public static class RowBean {
            @SerializedName("SIDO")
            private String SIDO;
            @SerializedName("SIGUNGU")
            private String SIGUNGU;
            @SerializedName("CANAME")
            private String CANAME;
            @SerializedName("PROD")
            private String PROD;
            @SerializedName("MODELN")
            private String MODELN;
            @SerializedName("CTYPE")
            private String CTYPE;
            @SerializedName("ADDR")
            private String ADDR;
            @SerializedName("STREAMADDR")
            private String STREAMADDR;

            public String getSIDO() {
                return SIDO;
            }

            public void setSIDO(String SIDO) {
                this.SIDO = SIDO;
            }

            public String getSIGUNGU() {
                return SIGUNGU;
            }

            public void setSIGUNGU(String SIGUNGU) {
                this.SIGUNGU = SIGUNGU;
            }

            public String getCANAME() {
                return CANAME;
            }

            public void setCANAME(String CANAME) {
                this.CANAME = CANAME;
            }

            public String getPROD() {
                return PROD;
            }

            public void setPROD(String PROD) {
                this.PROD = PROD;
            }

            public String getMODELN() {
                return MODELN;
            }

            public void setMODELN(String MODELN) {
                this.MODELN = MODELN;
            }

            public String getCTYPE() {
                return CTYPE;
            }

            public void setCTYPE(String CTYPE) {
                this.CTYPE = CTYPE;
            }

            public String getADDR() {
                return ADDR;
            }

            public void setADDR(String ADDR) {
                this.ADDR = ADDR;
            }

            public String getSTREAMADDR() {
                return STREAMADDR;
            }

            public void setSTREAMADDR(String STREAMADDR) {
                this.STREAMADDR = STREAMADDR;
            }
        }
    }
}
