package kr.edcan.lumihana.safegangwon.Activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import kr.edcan.lumihana.safegangwon.R;

/**
 * Created by kimok_000 on 2016-09-30.
 */
public class SettingFragment extends android.support.v4.app.Fragment {
    private Spinner spinner_sigungu;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public SettingFragment() {
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        spinner_sigungu = (Spinner) view.findViewById(R.id.fragment_setting_spinner_sigungu);

        sharedPreferences = getActivity().getSharedPreferences("Setting", getActivity().MODE_PRIVATE);
        editor = sharedPreferences.edit();

        final String[] options = getResources().getStringArray(R.array.spinnerSigungu);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line, options);
        spinner_sigungu.setAdapter(adapter);
        spinner_sigungu.setSelection(findDefaultValue(options));
        spinner_sigungu.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                final String option = options[position];
                editor.putString("region", option);
                editor.commit();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return view;
    }

    private int findDefaultValue(String[] options){
        for(int i = 0; i < options.length; i++){
            if(options[i].equals(sharedPreferences.getString("region",""))) return i;
        }
        return 0;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }
}
