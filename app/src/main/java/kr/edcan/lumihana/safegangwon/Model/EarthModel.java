package kr.edcan.lumihana.safegangwon.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import kr.edcan.lumihana.safegangwon.LiveModel.ParentModel;

/**
 * Created by kimok_000 on 2016-09-23.
 */
public class EarthModel extends ParentModel {

    /**
     * list_total_count : 24
     * RESULT : {"CODE":"INFO-000","MESSAGE":"정상 처리되었습니다."}
     * row : [{"SIDO":"강원도","GUGUN_NM":"원주","OBS_NM":"강원도 원주시청","LNG":"37.3419","LAT":"127.9196","OBS_ORG_NM":"국민안전처","OBS_ORG":"국민안전처","OBS_SDATE":"2013-12-21","OBS_EDATE":"","OBS_TYPE":"지진관측소","OBS_STATUS":"운영","LOCPLC_LOTNO_ADDR":"강원도 원주시 시청로1","OBS_ETC":"","OBS_EQUP":"기록계,가속도계","OBS_CONTENTS":"","OBS_TIME":"","DATA_BASE_DT":"2015-11-10"},{"SIDO":"강원도","GUGUN_NM":"강릉","OBS_NM":"강원도 강릉시청","LNG":"128.875","LAT":"37.752","OBS_ORG_NM":"강원도 강릉시청","OBS_ORG":"강원도 강릉시청","OBS_SDATE":"2014-12-24","OBS_EDATE":"","OBS_TYPE":"지진관측소","OBS_STATUS":"운영","LOCPLC_LOTNO_ADDR":"강원도 강릉시 강릉대로33 강릉시청","OBS_ETC":"","OBS_EQUP":"가속도계","OBS_CONTENTS":"","OBS_TIME":"","DATA_BASE_DT":"2015-09-02"},{"SIDO":"강원도","GUGUN_NM":"동해","OBS_NM":"동해시청(지하3축센서)","LNG":"129.1143001","LAT":"37.5248001","OBS_ORG_NM":"강원도 동해시청","OBS_ORG":"강원도 동해시청","OBS_SDATE":"2016-01-01","OBS_EDATE":"","OBS_TYPE":"지자체 재난상황실","OBS_STATUS":"미운영","LOCPLC_LOTNO_ADDR":"강원도 동해시 천곡동 806","OBS_ETC":"계약중","OBS_EQUP":"가속도계","OBS_CONTENTS":"지진신호 3축(x,y,z)","OBS_TIME":"","DATA_BASE_DT":"2015-11-10"},{"SIDO":"강원도","GUGUN_NM":"동해","OBS_NM":"동해시청(최상층2축센서)","LNG":"129.1143001","LAT":"37.5248001","OBS_ORG_NM":"강원도 동해시청","OBS_ORG":"강원도 동해시청","OBS_SDATE":"2016-01-01","OBS_EDATE":"","OBS_TYPE":"지자체 재난상황실","OBS_STATUS":"미운영","LOCPLC_LOTNO_ADDR":"강원도 동해시 천곡동 806","OBS_ETC":"계약중","OBS_EQUP":"가속도계","OBS_CONTENTS":"지진신호 2축(남-북, 동-서)","OBS_TIME":"","DATA_BASE_DT":"2015-11-11"},{"SIDO":"강원도","GUGUN_NM":"동해","OBS_NM":"동해시청(최상층1축센서)","LNG":"129.1139001","LAT":"37.5247001","OBS_ORG_NM":"강원도 동해시청","OBS_ORG":"강원도 동해시청","OBS_SDATE":"2016-01-01","OBS_EDATE":"","OBS_TYPE":"지자체 재난상황실","OBS_STATUS":"미운영","LOCPLC_LOTNO_ADDR":"강원도 동해시 천곡동 806","OBS_ETC":"계약중","OBS_EQUP":"가속도계","OBS_CONTENTS":"지진신호 1축(수직U-D)","OBS_TIME":"","DATA_BASE_DT":"2015-11-12"}]
     */

    @SerializedName("gwcgcom-earthquake_monitor-yangyang")
    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        @SerializedName("list_total_count")
        private int list_total_count;
        /**
         * CODE : INFO-000
         * MESSAGE : 정상 처리되었습니다.
         */

        @SerializedName("RESULT")
        private RESULTBean RESULT;
        /**
         * SIDO : 강원도
         * GUGUN_NM : 원주
         * OBS_NM : 강원도 원주시청
         * LNG : 37.3419
         * LAT : 127.9196
         * OBS_ORG_NM : 국민안전처
         * OBS_ORG : 국민안전처
         * OBS_SDATE : 2013-12-21
         * OBS_EDATE :
         * OBS_TYPE : 지진관측소
         * OBS_STATUS : 운영
         * LOCPLC_LOTNO_ADDR : 강원도 원주시 시청로1
         * OBS_ETC :
         * OBS_EQUP : 기록계,가속도계
         * OBS_CONTENTS :
         * OBS_TIME :
         * DATA_BASE_DT : 2015-11-10
         */

        @SerializedName("row")
        private List<RowBean> row;

        public int getList_total_count() {
            return list_total_count;
        }

        public void setList_total_count(int list_total_count) {
            this.list_total_count = list_total_count;
        }

        public RESULTBean getRESULT() {
            return RESULT;
        }

        public void setRESULT(RESULTBean RESULT) {
            this.RESULT = RESULT;
        }

        public List<RowBean> getRow() {
            return row;
        }

        public void setRow(List<RowBean> row) {
            this.row = row;
        }

        public static class RESULTBean {
            @SerializedName("CODE")
            private String CODE;
            @SerializedName("MESSAGE")
            private String MESSAGE;

            public String getCODE() {
                return CODE;
            }

            public void setCODE(String CODE) {
                this.CODE = CODE;
            }

            public String getMESSAGE() {
                return MESSAGE;
            }

            public void setMESSAGE(String MESSAGE) {
                this.MESSAGE = MESSAGE;
            }
        }

        public static class RowBean {
            @SerializedName("SIDO")
            private String SIDO;
            @SerializedName("GUGUN_NM")
            private String GUGUN_NM;
            @SerializedName("OBS_NM")
            private String OBS_NM;
            @SerializedName("LNG")
            private String LNG;
            @SerializedName("LAT")
            private String LAT;
            @SerializedName("OBS_ORG_NM")
            private String OBS_ORG_NM;
            @SerializedName("OBS_ORG")
            private String OBS_ORG;
            @SerializedName("OBS_SDATE")
            private String OBS_SDATE;
            @SerializedName("OBS_EDATE")
            private String OBS_EDATE;
            @SerializedName("OBS_TYPE")
            private String OBS_TYPE;
            @SerializedName("OBS_STATUS")
            private String OBS_STATUS;
            @SerializedName("LOCPLC_LOTNO_ADDR")
            private String LOCPLC_LOTNO_ADDR;
            @SerializedName("OBS_ETC")
            private String OBS_ETC;
            @SerializedName("OBS_EQUP")
            private String OBS_EQUP;
            @SerializedName("OBS_CONTENTS")
            private String OBS_CONTENTS;
            @SerializedName("OBS_TIME")
            private String OBS_TIME;
            @SerializedName("DATA_BASE_DT")
            private String DATA_BASE_DT;

            public String getSIDO() {
                return SIDO;
            }

            public void setSIDO(String SIDO) {
                this.SIDO = SIDO;
            }

            public String getGUGUN_NM() {
                return GUGUN_NM;
            }

            public void setGUGUN_NM(String GUGUN_NM) {
                this.GUGUN_NM = GUGUN_NM;
            }

            public String getOBS_NM() {
                return OBS_NM;
            }

            public void setOBS_NM(String OBS_NM) {
                this.OBS_NM = OBS_NM;
            }

            public String getLNG() {
                return LNG;
            }

            public void setLNG(String LNG) {
                this.LNG = LNG;
            }

            public String getLAT() {
                return LAT;
            }

            public void setLAT(String LAT) {
                this.LAT = LAT;
            }

            public String getOBS_ORG_NM() {
                return OBS_ORG_NM;
            }

            public void setOBS_ORG_NM(String OBS_ORG_NM) {
                this.OBS_ORG_NM = OBS_ORG_NM;
            }

            public String getOBS_ORG() {
                return OBS_ORG;
            }

            public void setOBS_ORG(String OBS_ORG) {
                this.OBS_ORG = OBS_ORG;
            }

            public String getOBS_SDATE() {
                return OBS_SDATE;
            }

            public void setOBS_SDATE(String OBS_SDATE) {
                this.OBS_SDATE = OBS_SDATE;
            }

            public String getOBS_EDATE() {
                return OBS_EDATE;
            }

            public void setOBS_EDATE(String OBS_EDATE) {
                this.OBS_EDATE = OBS_EDATE;
            }

            public String getOBS_TYPE() {
                return OBS_TYPE;
            }

            public void setOBS_TYPE(String OBS_TYPE) {
                this.OBS_TYPE = OBS_TYPE;
            }

            public String getOBS_STATUS() {
                return OBS_STATUS;
            }

            public void setOBS_STATUS(String OBS_STATUS) {
                this.OBS_STATUS = OBS_STATUS;
            }

            public String getLOCPLC_LOTNO_ADDR() {
                return LOCPLC_LOTNO_ADDR;
            }

            public void setLOCPLC_LOTNO_ADDR(String LOCPLC_LOTNO_ADDR) {
                this.LOCPLC_LOTNO_ADDR = LOCPLC_LOTNO_ADDR;
            }

            public String getOBS_ETC() {
                return OBS_ETC;
            }

            public void setOBS_ETC(String OBS_ETC) {
                this.OBS_ETC = OBS_ETC;
            }

            public String getOBS_EQUP() {
                return OBS_EQUP;
            }

            public void setOBS_EQUP(String OBS_EQUP) {
                this.OBS_EQUP = OBS_EQUP;
            }

            public String getOBS_CONTENTS() {
                return OBS_CONTENTS;
            }

            public void setOBS_CONTENTS(String OBS_CONTENTS) {
                this.OBS_CONTENTS = OBS_CONTENTS;
            }

            public String getOBS_TIME() {
                return OBS_TIME;
            }

            public void setOBS_TIME(String OBS_TIME) {
                this.OBS_TIME = OBS_TIME;
            }

            public String getDATA_BASE_DT() {
                return DATA_BASE_DT;
            }

            public void setDATA_BASE_DT(String DATA_BASE_DT) {
                this.DATA_BASE_DT = DATA_BASE_DT;
            }
        }
    }
}
