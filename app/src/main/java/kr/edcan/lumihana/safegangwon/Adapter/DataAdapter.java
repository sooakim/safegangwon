package kr.edcan.lumihana.safegangwon.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import kr.edcan.lumihana.safegangwon.Model.DataModel;
import kr.edcan.lumihana.safegangwon.R;

/**
 * Created by kimok_000 on 2016-09-25.
 */
public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    private Context context;
    private ArrayList<DataModel> arrayList;

    public DataAdapter(Context context, ArrayList<DataModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_data, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final DataModel dataModel = arrayList.get(position);

        holder.text_title.setText(dataModel.getTitle().toString().trim() + "");
        holder.text_content.setText(dataModel.getContent().toString().trim() + "");
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView text_title;
        private TextView text_content;

        public ViewHolder(View itemView) {
            super(itemView);

            text_title = (TextView) itemView.findViewById(R.id.data_text_title);
            text_content = (TextView) itemView.findViewById(R.id.data_text_content);
        }
    }
}
