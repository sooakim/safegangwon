package kr.edcan.lumihana.safegangwon.RealmModel;

import io.realm.RealmModel;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by kimok_000 on 2016-09-24.
 */
@RealmClass
public class EarthModel extends RealmObject implements RealmModel {
    @PrimaryKey
    private int _id;

    private String SIDO;
    private String GUGUN_NM;
    private String OBS_NM;
    private String LNG;
    private String LAT;
    private String OBS_ORG_NM;
    private String OBS_ORG;
    private String OBS_SDATE;
    private String OBS_EDATE;
    private String OBS_TYPE;
    private String OBS_STATUS;
    private String LOCPLC_LOTNO_ADDR;
    private String OBS_ETC;
    private String OBS_EQUP;
    private String OBS_CONTENTS;
    private String OBS_TIME;
    private String DATA_BASE_DT;

    public EarthModel(){}

    public EarthModel(int _id, String SIDO, String GUGUN_NM, String OBS_NM, String LNG, String LAT, String OBS_ORG_NM, String OBS_ORG, String OBS_SDATE, String OBS_EDATE, String OBS_TYPE, String OBS_STATUS, String LOCPLC_LOTNO_ADDR, String OBS_ETC, String OBS_EQUP, String OBS_CONTENTS, String OBS_TIME, String DATA_BASE_DT) {
        this._id = _id;
        this.SIDO = SIDO;
        this.GUGUN_NM = GUGUN_NM;
        this.OBS_NM = OBS_NM;
        this.LNG = LNG;
        this.LAT = LAT;
        this.OBS_ORG_NM = OBS_ORG_NM;
        this.OBS_ORG = OBS_ORG;
        this.OBS_SDATE = OBS_SDATE;
        this.OBS_EDATE = OBS_EDATE;
        this.OBS_TYPE = OBS_TYPE;
        this.OBS_STATUS = OBS_STATUS;
        this.LOCPLC_LOTNO_ADDR = LOCPLC_LOTNO_ADDR;
        this.OBS_ETC = OBS_ETC;
        this.OBS_EQUP = OBS_EQUP;
        this.OBS_CONTENTS = OBS_CONTENTS;
        this.OBS_TIME = OBS_TIME;
        this.DATA_BASE_DT = DATA_BASE_DT;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getSIDO() {
        return SIDO;
    }

    public void setSIDO(String SIDO) {
        this.SIDO = SIDO;
    }

    public String getGUGUN_NM() {
        return GUGUN_NM;
    }

    public void setGUGUN_NM(String GUGUN_NM) {
        this.GUGUN_NM = GUGUN_NM;
    }

    public String getOBS_NM() {
        return OBS_NM;
    }

    public void setOBS_NM(String OBS_NM) {
        this.OBS_NM = OBS_NM;
    }

    public String getLNG() {
        return LNG;
    }

    public void setLNG(String LNG) {
        this.LNG = LNG;
    }

    public String getLAT() {
        return LAT;
    }

    public void setLAT(String LAT) {
        this.LAT = LAT;
    }

    public String getOBS_ORG_NM() {
        return OBS_ORG_NM;
    }

    public void setOBS_ORG_NM(String OBS_ORG_NM) {
        this.OBS_ORG_NM = OBS_ORG_NM;
    }

    public String getOBS_ORG() {
        return OBS_ORG;
    }

    public void setOBS_ORG(String OBS_ORG) {
        this.OBS_ORG = OBS_ORG;
    }

    public String getOBS_SDATE() {
        return OBS_SDATE;
    }

    public void setOBS_SDATE(String OBS_SDATE) {
        this.OBS_SDATE = OBS_SDATE;
    }

    public String getOBS_EDATE() {
        return OBS_EDATE;
    }

    public void setOBS_EDATE(String OBS_EDATE) {
        this.OBS_EDATE = OBS_EDATE;
    }

    public String getOBS_TYPE() {
        return OBS_TYPE;
    }

    public void setOBS_TYPE(String OBS_TYPE) {
        this.OBS_TYPE = OBS_TYPE;
    }

    public String getOBS_STATUS() {
        return OBS_STATUS;
    }

    public void setOBS_STATUS(String OBS_STATUS) {
        this.OBS_STATUS = OBS_STATUS;
    }

    public String getLOCPLC_LOTNO_ADDR() {
        return LOCPLC_LOTNO_ADDR;
    }

    public void setLOCPLC_LOTNO_ADDR(String LOCPLC_LOTNO_ADDR) {
        this.LOCPLC_LOTNO_ADDR = LOCPLC_LOTNO_ADDR;
    }

    public String getOBS_ETC() {
        return OBS_ETC;
    }

    public void setOBS_ETC(String OBS_ETC) {
        this.OBS_ETC = OBS_ETC;
    }

    public String getOBS_EQUP() {
        return OBS_EQUP;
    }

    public void setOBS_EQUP(String OBS_EQUP) {
        this.OBS_EQUP = OBS_EQUP;
    }

    public String getOBS_CONTENTS() {
        return OBS_CONTENTS;
    }

    public void setOBS_CONTENTS(String OBS_CONTENTS) {
        this.OBS_CONTENTS = OBS_CONTENTS;
    }

    public String getOBS_TIME() {
        return OBS_TIME;
    }

    public void setOBS_TIME(String OBS_TIME) {
        this.OBS_TIME = OBS_TIME;
    }

    public String getDATA_BASE_DT() {
        return DATA_BASE_DT;
    }

    public void setDATA_BASE_DT(String DATA_BASE_DT) {
        this.DATA_BASE_DT = DATA_BASE_DT;
    }
}
