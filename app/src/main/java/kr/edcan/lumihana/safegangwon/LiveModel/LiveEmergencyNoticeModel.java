package kr.edcan.lumihana.safegangwon.LiveModel;

/**
 * Created by kimok_000 on 2016-09-28.
 */
public class LiveEmergencyNoticeModel extends ParentModel {
    private String clmy_pttn_nm;
    private String titl;

    public LiveEmergencyNoticeModel(String clmy_pttn_nm, String titl) {
        super.typeOfModel = ParentModel.TYPE_EMERGENCY_NOTICE;
        this.clmy_pttn_nm = clmy_pttn_nm;
        this.titl = titl;
    }

    public String getClmy_pttn_nm() {
        return clmy_pttn_nm;
    }

    public String getTitl() {
        return titl;
    }

    public void setClmy_pttn_nm(String clmy_pttn_nm) {
        this.clmy_pttn_nm = clmy_pttn_nm;
    }

    public void setTitl(String titl) {
        this.titl = titl;
    }
}
