package kr.edcan.lumihana.safegangwon.RealmModel;

import io.realm.RealmModel;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by kimok_000 on 2016-09-24.
 */
@RealmClass
public class WeatherModel extends RealmObject implements RealmModel {
    @PrimaryKey
    private int _id;

    private String alertYn;
    private String stormYn;

    private String humidity;
    private String lightning;
    private String timeObservation;

    private String nameStation;
    private String idStation;
    private String typeStation;
    private String latitudeStation;
    private String longitudeStation;

    private String wdirWind;
    private String wspdWind;

    private String typePrecipitation;
    private String sinceOntimePrecipitation;

    private String nameSky;
    private String codeSky;

    private String sinceOntimeRain;
    private String sinceMidnightRain;
    private String last10minRain;
    private String last15minRain;
    private String last30minRain;
    private String last1hourRain;
    private String last6hourRain;
    private String last12hourRain;
    private String last24hourRain;

    private String surfacePressure;
    private String seaLevelPressure;

    private String tcTemperature;
    private String tmaxTemperature;
    private String tminTemperature;

    public WeatherModel(){}

    public WeatherModel(int _id, String alertYn, String stormYn, String humidity, String lightning, String timeObservation, String nameStation, String idStation, String typeStation, String latitudeStation, String longitudeStation, String wdirWind, String wspdWind, String typePrecipitation, String sinceOntimePrecipitation, String nameSky, String codeSky, String sinceOntimeRain, String sinceMidnightRain, String last10minRain, String last15minRain, String last30minRain, String last1hourRain, String last6hourRain, String last12hourRain, String last24hourRain, String surfacePressure, String seaLevelPressure, String tcTemperature, String tmaxTemperature, String tminTemperature) {
        this._id = _id;
        this.alertYn = alertYn;
        this.stormYn = stormYn;
        this.humidity = humidity;
        this.lightning = lightning;
        this.timeObservation = timeObservation;
        this.nameStation = nameStation;
        this.idStation = idStation;
        this.typeStation = typeStation;
        this.latitudeStation = latitudeStation;
        this.longitudeStation = longitudeStation;
        this.wdirWind = wdirWind;
        this.wspdWind = wspdWind;
        this.typePrecipitation = typePrecipitation;
        this.sinceOntimePrecipitation = sinceOntimePrecipitation;
        this.nameSky = nameSky;
        this.codeSky = codeSky;
        this.sinceOntimeRain = sinceOntimeRain;
        this.sinceMidnightRain = sinceMidnightRain;
        this.last10minRain = last10minRain;
        this.last15minRain = last15minRain;
        this.last30minRain = last30minRain;
        this.last1hourRain = last1hourRain;
        this.last6hourRain = last6hourRain;
        this.last12hourRain = last12hourRain;
        this.last24hourRain = last24hourRain;
        this.surfacePressure = surfacePressure;
        this.seaLevelPressure = seaLevelPressure;
        this.tcTemperature = tcTemperature;
        this.tmaxTemperature = tmaxTemperature;
        this.tminTemperature = tminTemperature;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public void setNameStation(String nameStation) {
        this.nameStation = nameStation;
    }

    public void setIdStation(String idStation) {
        this.idStation = idStation;
    }

    public void setTypeStation(String typeStation) {
        this.typeStation = typeStation;
    }

    public void setLatitudeStation(String latitudeStation) {
        this.latitudeStation = latitudeStation;
    }

    public void setLongitudeStation(String longitudeStation) {
        this.longitudeStation = longitudeStation;
    }

    public void setWdirWind(String wdirWind) {
        this.wdirWind = wdirWind;
    }

    public void setWspdWind(String wspdWind) {
        this.wspdWind = wspdWind;
    }

    public void setTypePrecipitation(String typePrecipitation) {
        this.typePrecipitation = typePrecipitation;
    }

    public void setSinceOntimePrecipitation(String sinceOntimePrecipitation) {
        this.sinceOntimePrecipitation = sinceOntimePrecipitation;
    }

    public void setNameSky(String nameSky) {
        this.nameSky = nameSky;
    }

    public void setCodeSky(String codeSky) {
        this.codeSky = codeSky;
    }

    public void setSinceOntimeRain(String sinceOntimeRain) {
        this.sinceOntimeRain = sinceOntimeRain;
    }

    public void setSinceMidnightRain(String sinceMidnightRain) {
        this.sinceMidnightRain = sinceMidnightRain;
    }

    public void setLast10minRain(String last10minRain) {
        this.last10minRain = last10minRain;
    }

    public void setLast15minRain(String last15minRain) {
        this.last15minRain = last15minRain;
    }

    public void setLast30minRain(String last30minRain) {
        this.last30minRain = last30minRain;
    }

    public void setLast1hourRain(String last1hourRain) {
        this.last1hourRain = last1hourRain;
    }

    public void setLast6hourRain(String last6hourRain) {
        this.last6hourRain = last6hourRain;
    }

    public void setLast12hourRain(String last12hourRain) {
        this.last12hourRain = last12hourRain;
    }

    public void setLast24hourRain(String last24hourRain) {
        this.last24hourRain = last24hourRain;
    }

    public void setSurfacePressure(String surfacePressure) {
        this.surfacePressure = surfacePressure;
    }

    public void setSeaLevelPressure(String seaLevelPressure) {
        this.seaLevelPressure = seaLevelPressure;
    }

    public void setTcTemperature(String tcTemperature) {
        this.tcTemperature = tcTemperature;
    }

    public void setTmaxTemperature(String tmaxTemperature) {
        this.tmaxTemperature = tmaxTemperature;
    }

    public void setTminTemperature(String tminTemperature) {
        this.tminTemperature = tminTemperature;
    }

    public String getNameStation() {
        return nameStation;
    }

    public String getIdStation() {
        return idStation;
    }

    public String getTypeStation() {
        return typeStation;
    }

    public String getLatitudeStation() {
        return latitudeStation;
    }

    public String getLongitudeStation() {
        return longitudeStation;
    }

    public String getWdirWind() {
        return wdirWind;
    }

    public String getWspdWind() {
        return wspdWind;
    }

    public String getTypePrecipitation() {
        return typePrecipitation;
    }

    public String getSinceOntimePrecipitation() {
        return sinceOntimePrecipitation;
    }

    public String getNameSky() {
        return nameSky;
    }

    public String getCodeSky() {
        return codeSky;
    }

    public String getSinceOntimeRain() {
        return sinceOntimeRain;
    }

    public String getSinceMidnightRain() {
        return sinceMidnightRain;
    }

    public String getLast10minRain() {
        return last10minRain;
    }

    public String getLast15minRain() {
        return last15minRain;
    }

    public String getLast30minRain() {
        return last30minRain;
    }

    public String getLast1hourRain() {
        return last1hourRain;
    }

    public String getLast6hourRain() {
        return last6hourRain;
    }

    public String getLast12hourRain() {
        return last12hourRain;
    }

    public String getLast24hourRain() {
        return last24hourRain;
    }

    public String getSurfacePressure() {
        return surfacePressure;
    }

    public String getSeaLevelPressure() {
        return seaLevelPressure;
    }

    public String getTcTemperature() {
        return tcTemperature;
    }

    public String getTmaxTemperature() {
        return tmaxTemperature;
    }

    public String getTminTemperature() {
        return tminTemperature;
    }

    public String getHumidity() {
        return humidity;
    }

    public String getLightning() {
        return lightning;
    }

    public String getTimeObservation() {
        return timeObservation;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public void setLightning(String lightning) {
        this.lightning = lightning;
    }

    public void setTimeObservation(String timeObservation) {
        this.timeObservation = timeObservation;
    }

    public String getAlertYn() {
        return alertYn;
    }

    public void setAlertYn(String alertYn) {
        this.alertYn = alertYn;
    }

    public String getStormYn() {
        return stormYn;
    }

    public void setStormYn(String stormYn) {
        this.stormYn = stormYn;
    }

}
