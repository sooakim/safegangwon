package kr.edcan.lumihana.safegangwon.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.webkit.WebView;

import io.realm.Realm;
import io.realm.RealmResults;
import kr.edcan.lumihana.safegangwon.R;
import kr.edcan.lumihana.safegangwon.RealmModel.EmergencyNoticeModel;

public class EmergencyNoticeActivity extends AppCompatActivity {
    private Toolbar toolbar;
    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_notice);

        initWebView();
    }

    private void initWebView() {
        final Intent intent = getIntent();
        final int _id = intent.getIntExtra("_id", -1);

        if (_id >= 0) {
            Realm realm = Realm.getDefaultInstance();
            RealmResults<EmergencyNoticeModel> results = realm.where(EmergencyNoticeModel.class).equalTo("_id", _id).findAll();

            EmergencyNoticeModel model;
            if (results.size() <= 0) model = null;
            else model = results.get(0);

            if (model != null) {
                final long msg_id = model.getMsg_id();
                final int msg_seq = model.getMsg_seq();
                final int clmy_pttn_cd = model.getClmy_pttn_cd();
                final String clmy_pttn_nm = getModelValue(model.getClmy_pttn_nm());
                final String titl = getModelValue(model.getTitl());
                final String cnts1 = getModelValue(model.getCnts1());
                final String inpt_date = getModelValue(model.getInpt_date());

                final String title = "[" + clmy_pttn_nm + "] " + titl;
                if(!title.equals("")) initToolbar(title);
                else initToolbar("Null");
                if (!cnts1.equals("")) setWebView(cnts1);
            } else {

            }
        } else {

        }

    }

    private String getModelValue(String original) {
        if (original == null) return "";
        else return original.toString().trim();
    }


    private void setWebView(String html) {
        webView = (WebView) findViewById(R.id.emergencynotice_webview_doc);
        webView.loadData(html,  "text/html; charset=UTF-8", null);
    }

    private void initToolbar(String title) {
        toolbar = (Toolbar) findViewById(R.id.emergencynotice_toolbar);
        toolbar.setTitle(title);
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_36px);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
