package kr.edcan.lumihana.safegangwon.Util;

import android.content.Context;
import android.location.Address;
import android.location.Location;

import com.entire.sammalik.samlocationandgeocoding.SamLocationRequestService;

import kr.edcan.lumihana.safegangwon.Model.GPSModel;

/**
 * Created by kimok_000 on 2016-09-23.
 */
public class GPSManager {
    private Context context;
    private OnLocationChangedListener onLocationChangedListener;
    private SamLocationRequestService samLocationRequestService;

    public GPSManager(Context context) {
        this.context = context;

        samLocationRequestService = new SamLocationRequestService(context);
    }

    public void setOnLocationChangedListener(OnLocationChangedListener onLocationChangedListener) {
        this.onLocationChangedListener = onLocationChangedListener;
    }

    public void getLocation() {
        samLocationRequestService.executeService(new SamLocationRequestService.SamLocationListener() {
            @Override
            public void onLocationUpdate(Location location, Address address) {
                onLocationChangedListener.onLocationChanged(new GPSModel(location, address));
            }
        });
    }

    public interface OnLocationChangedListener {
        void onLocationChanged(GPSModel gpsModel);
    }
}
