package kr.edcan.lumihana.safegangwon.RealmModel;

import io.realm.RealmModel;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

/**
 * Created by kimok_000 on 2016-09-24.
 */
@RealmClass
public class CivilDefenseModel extends RealmObject implements RealmModel {
    @PrimaryKey
    private int _id;

    private int NO;
    private String SIGUN_CD;
    private String SIGUN_NM;
    private String BIZPLC_NM;
    private String LOCPLC_LOTNO_ADDR;
    private String LOCPLC_ROADNM_ADDR;
    private String LICENSG_DE;
    private String BSN_STATE_NM;
    private String CLSBIZ_DE;
    private String SUSPNBIZ_BEGIN_DE;
    private String SUSPNBIZ_END_DE;
    private String REOPENBIZ_DE;
    private int LOCPLC_AR;
    private String LOCPLC_ZIP_CD;
    private String FACLT_BULDNG_NM;
    private String RELEASE_DE;
    private String EMGNCY_FACLT_DIV_NM;
    private int WGS84_LOGT;
    private int WGS84_LAT;
    private int X_CRDNT;
    private int Y_CRDNT;
    private String DATA_COLCT_DE;
    private String ETL_LDADNG_DTM;
    private String LAT;
    private String LNG;

    public CivilDefenseModel(){}

    public CivilDefenseModel(int _id, int NO, String SIGUN_CD, String SIGUN_NM, String BIZPLC_NM, String LOCPLC_LOTNO_ADDR, String LOCPLC_ROADNM_ADDR, String LICENSG_DE, String BSN_STATE_NM, String CLSBIZ_DE, String SUSPNBIZ_BEGIN_DE, String SUSPNBIZ_END_DE, String REOPENBIZ_DE, int LOCPLC_AR, String LOCPLC_ZIP_CD, String FACLT_BULDNG_NM, String RELEASE_DE, String EMGNCY_FACLT_DIV_NM, int WGS84_LOGT, int WGS84_LAT, int x_CRDNT, int y_CRDNT, String DATA_COLCT_DE, String ETL_LDADNG_DTM, String LAT, String LNG) {
        this._id = _id;
        this.NO = NO;
        this.SIGUN_CD = SIGUN_CD;
        this.SIGUN_NM = SIGUN_NM;
        this.BIZPLC_NM = BIZPLC_NM;
        this.LOCPLC_LOTNO_ADDR = LOCPLC_LOTNO_ADDR;
        this.LOCPLC_ROADNM_ADDR = LOCPLC_ROADNM_ADDR;
        this.LICENSG_DE = LICENSG_DE;
        this.BSN_STATE_NM = BSN_STATE_NM;
        this.CLSBIZ_DE = CLSBIZ_DE;
        this.SUSPNBIZ_BEGIN_DE = SUSPNBIZ_BEGIN_DE;
        this.SUSPNBIZ_END_DE = SUSPNBIZ_END_DE;
        this.REOPENBIZ_DE = REOPENBIZ_DE;
        this.LOCPLC_AR = LOCPLC_AR;
        this.LOCPLC_ZIP_CD = LOCPLC_ZIP_CD;
        this.FACLT_BULDNG_NM = FACLT_BULDNG_NM;
        this.RELEASE_DE = RELEASE_DE;
        this.EMGNCY_FACLT_DIV_NM = EMGNCY_FACLT_DIV_NM;
        this.WGS84_LOGT = WGS84_LOGT;
        this.WGS84_LAT = WGS84_LAT;
        X_CRDNT = x_CRDNT;
        Y_CRDNT = y_CRDNT;
        this.DATA_COLCT_DE = DATA_COLCT_DE;
        this.ETL_LDADNG_DTM = ETL_LDADNG_DTM;
        this.LAT = LAT;
        this.LNG = LNG;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public int getNO() {
        return NO;
    }

    public void setNO(int NO) {
        this.NO = NO;
    }

    public String getSIGUN_CD() {
        return SIGUN_CD;
    }

    public void setSIGUN_CD(String SIGUN_CD) {
        this.SIGUN_CD = SIGUN_CD;
    }

    public String getSIGUN_NM() {
        return SIGUN_NM;
    }

    public void setSIGUN_NM(String SIGUN_NM) {
        this.SIGUN_NM = SIGUN_NM;
    }

    public String getBIZPLC_NM() {
        return BIZPLC_NM;
    }

    public void setBIZPLC_NM(String BIZPLC_NM) {
        this.BIZPLC_NM = BIZPLC_NM;
    }

    public String getLOCPLC_LOTNO_ADDR() {
        return LOCPLC_LOTNO_ADDR;
    }

    public void setLOCPLC_LOTNO_ADDR(String LOCPLC_LOTNO_ADDR) {
        this.LOCPLC_LOTNO_ADDR = LOCPLC_LOTNO_ADDR;
    }

    public String getLOCPLC_ROADNM_ADDR() {
        return LOCPLC_ROADNM_ADDR;
    }

    public void setLOCPLC_ROADNM_ADDR(String LOCPLC_ROADNM_ADDR) {
        this.LOCPLC_ROADNM_ADDR = LOCPLC_ROADNM_ADDR;
    }

    public String getLICENSG_DE() {
        return LICENSG_DE;
    }

    public void setLICENSG_DE(String LICENSG_DE) {
        this.LICENSG_DE = LICENSG_DE;
    }

    public String getBSN_STATE_NM() {
        return BSN_STATE_NM;
    }

    public void setBSN_STATE_NM(String BSN_STATE_NM) {
        this.BSN_STATE_NM = BSN_STATE_NM;
    }

    public String getCLSBIZ_DE() {
        return CLSBIZ_DE;
    }

    public void setCLSBIZ_DE(String CLSBIZ_DE) {
        this.CLSBIZ_DE = CLSBIZ_DE;
    }

    public String getSUSPNBIZ_BEGIN_DE() {
        return SUSPNBIZ_BEGIN_DE;
    }

    public void setSUSPNBIZ_BEGIN_DE(String SUSPNBIZ_BEGIN_DE) {
        this.SUSPNBIZ_BEGIN_DE = SUSPNBIZ_BEGIN_DE;
    }

    public String getSUSPNBIZ_END_DE() {
        return SUSPNBIZ_END_DE;
    }

    public void setSUSPNBIZ_END_DE(String SUSPNBIZ_END_DE) {
        this.SUSPNBIZ_END_DE = SUSPNBIZ_END_DE;
    }

    public String getREOPENBIZ_DE() {
        return REOPENBIZ_DE;
    }

    public void setREOPENBIZ_DE(String REOPENBIZ_DE) {
        this.REOPENBIZ_DE = REOPENBIZ_DE;
    }

    public int getLOCPLC_AR() {
        return LOCPLC_AR;
    }

    public void setLOCPLC_AR(int LOCPLC_AR) {
        this.LOCPLC_AR = LOCPLC_AR;
    }

    public String getLOCPLC_ZIP_CD() {
        return LOCPLC_ZIP_CD;
    }

    public void setLOCPLC_ZIP_CD(String LOCPLC_ZIP_CD) {
        this.LOCPLC_ZIP_CD = LOCPLC_ZIP_CD;
    }

    public String getFACLT_BULDNG_NM() {
        return FACLT_BULDNG_NM;
    }

    public void setFACLT_BULDNG_NM(String FACLT_BULDNG_NM) {
        this.FACLT_BULDNG_NM = FACLT_BULDNG_NM;
    }

    public String getRELEASE_DE() {
        return RELEASE_DE;
    }

    public void setRELEASE_DE(String RELEASE_DE) {
        this.RELEASE_DE = RELEASE_DE;
    }

    public String getEMGNCY_FACLT_DIV_NM() {
        return EMGNCY_FACLT_DIV_NM;
    }

    public void setEMGNCY_FACLT_DIV_NM(String EMGNCY_FACLT_DIV_NM) {
        this.EMGNCY_FACLT_DIV_NM = EMGNCY_FACLT_DIV_NM;
    }

    public int getWGS84_LOGT() {
        return WGS84_LOGT;
    }

    public void setWGS84_LOGT(int WGS84_LOGT) {
        this.WGS84_LOGT = WGS84_LOGT;
    }

    public int getWGS84_LAT() {
        return WGS84_LAT;
    }

    public void setWGS84_LAT(int WGS84_LAT) {
        this.WGS84_LAT = WGS84_LAT;
    }

    public int getX_CRDNT() {
        return X_CRDNT;
    }

    public void setX_CRDNT(int X_CRDNT) {
        this.X_CRDNT = X_CRDNT;
    }

    public int getY_CRDNT() {
        return Y_CRDNT;
    }

    public void setY_CRDNT(int Y_CRDNT) {
        this.Y_CRDNT = Y_CRDNT;
    }

    public String getDATA_COLCT_DE() {
        return DATA_COLCT_DE;
    }

    public void setDATA_COLCT_DE(String DATA_COLCT_DE) {
        this.DATA_COLCT_DE = DATA_COLCT_DE;
    }

    public String getETL_LDADNG_DTM() {
        return ETL_LDADNG_DTM;
    }

    public void setETL_LDADNG_DTM(String ETL_LDADNG_DTM) {
        this.ETL_LDADNG_DTM = ETL_LDADNG_DTM;
    }

    public String getLAT() {
        return LAT;
    }

    public void setLAT(String LAT) {
        this.LAT = LAT;
    }

    public String getLNG() {
        return LNG;
    }

    public void setLNG(String LNG) {
        this.LNG = LNG;
    }
}
