package kr.edcan.lumihana.safegangwon.Model;

/**
 * Created by kimok_000 on 2016-09-26.
 */
public class ListDataModel {
    private int _id;
    private int type;
    private String number;
    private String title;
    private String content;
    private String date;

    public ListDataModel(int _id, int type ,String number, String title, String content, String date) {
        this.type = type;
        this._id = _id;
        this.number = number;
        this.title = title;
        this.content = content;
        this.date = date;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getNumber() {
        return number;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public String getDate() {
        return date;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
