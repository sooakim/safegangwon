package kr.edcan.lumihana.safegangwon.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.IOException;
import java.util.UUID;

import kr.edcan.lumihana.safegangwon.Model.PostModel;
import kr.edcan.lumihana.safegangwon.R;
import kr.edcan.lumihana.safegangwon.Util.NetworkManager;

public class PostActivity extends AppCompatActivity {
    private static final String SHAREDPREFERENCE_USER = "User";
    private static final String CLASS_NAME = "PostActivity";
    private FrameLayout root;
    private TextView text_filename;
    private EditText edit_title, edit_content;
    private ImageView image;
    private FirebaseDatabase firebaseDatabase;
    private DatabaseReference databaseReference;
    private FirebaseStorage firebaseStorage;
    private StorageReference storageReference;
    private FloatingActionButton floating_post;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private String email, uuid;
    private Uri imageUri;
    private ProgressDialog progressDialog;
    private boolean isLaunched = false;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        initSharedPreferences();
        init();
        userCheck();
        checkNetwork();
        initFirebase();
        initToolbar();
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.post_toolbar);
        toolbar.setTitle("글 작성");
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_36px);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                break;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        userCheck();
        checkNetwork();
    }

    @Override
    protected void onResume() {
        super.onResume();
        userCheck();
        checkNetwork();
    }

    private boolean userCheck() {
        email = sharedPreferences.getString("Email", "");
        uuid = sharedPreferences.getString("Uid", "");

        if (email.equals("") || uuid.equals("")) {
            if (isLaunched) finish();
            final Intent intent = new Intent(getApplicationContext(), SignInActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            isLaunched = true;
            Toast.makeText(getApplicationContext(), "이 기능을 이용하려면 로그인이 필요합니다.", Toast.LENGTH_SHORT).show();
            finish();
            return true;
        } else return false;
    }

    private boolean checkNetwork() {
        if (NetworkManager.getConnectivityStatus(getApplicationContext()) == NetworkManager.TYPE_NOT_CONNECTED) {

            Snackbar.make(root, "네트워크에 연결이 필요합니다.", Snackbar.LENGTH_LONG)
                    .setAction("연결", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            final Intent intent = new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    })
                    .show();
            return false;
        } else return true;
    }

    private void initSharedPreferences() {
        sharedPreferences = getSharedPreferences(SHAREDPREFERENCE_USER, MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    private void initFirebase() {
        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference();
        firebaseStorage = FirebaseStorage.getInstance();
        storageReference = firebaseStorage.getReference();
    }

    private void init() {
        root = (FrameLayout) findViewById(R.id.post_root);
        edit_title = (EditText) findViewById(R.id.post_edit_title);
        edit_content = (EditText) findViewById(R.id.post_edit_content);
        text_filename = (TextView) findViewById(R.id.post_text_filename);
        image = (ImageView) findViewById(R.id.post_image);
        floating_post = (FloatingActionButton) findViewById(R.id.post_floating_post);

        floating_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String title = edit_title.getText().toString().trim();
                final String content = edit_content.getText().toString().trim();

                onPost(title, content);
            }
        });

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(Intent.ACTION_PICK)
                        .setType(MediaStore.Images.Media.CONTENT_TYPE)
                        .setData(MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, 2);
            }
        });
    }

    private void onPost(String title, String content) {
        if (title.equals("") || content.equals("")) {
            Snackbar.make(root, "내용을 확인해주세요", Snackbar.LENGTH_SHORT).show();
            return;
        }

        if (checkNetwork()) {
            final PostModel model = new PostModel(email, uuid, title, content, "", 0);
            uploadImage(databaseReference, model, imageUri);
        }
    }

    private void uploadImage(final DatabaseReference databaseIstance, final PostModel postModel, final Uri imageUri) {
        if (databaseIstance == null) return;
        if (postModel == null) return;

        progressDialog = new ProgressDialog(PostActivity.this);
        progressDialog.setTitle("게시글 업로드");
        progressDialog.setMessage("잠시만 기다려주세요...");
        progressDialog.show();

        if (imageUri == null) uploadPost(databaseIstance, postModel);
        else {
            storageReference.child("posts").child(UUID.randomUUID().toString()).putFile(imageUri)
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            final Uri image = taskSnapshot.getDownloadUrl();
                            postModel.setImage(image.toString());
                            uploadPost(databaseIstance, postModel);
                        }
                    })
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Snackbar.make(root, "업로드 실패", Snackbar.LENGTH_SHORT).show();
                            Log.e(CLASS_NAME, e.getLocalizedMessage());
                        }
                    });
        }
    }

    private void uploadPost(DatabaseReference databaseInstance, PostModel postModel) {
        if (databaseInstance == null) return;

        postModel.setTime(System.currentTimeMillis());
        databaseInstance.child("posts").push().setValue(postModel)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            progressDialog.dismiss();
                            Toast.makeText(getApplicationContext(), "게시 성공", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            progressDialog.dismiss();
                            Snackbar.make(root, task.getException().getLocalizedMessage() + "", Snackbar.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            try {
                final String name = getImageNameToUri(data.getData());
                imageUri = data.getData();
                final Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());

                text_filename.setText(name + "");
                image.setImageBitmap(bitmap);
            } catch (IOException e) {
                Log.e(CLASS_NAME, e.getLocalizedMessage());
            }
        }
    }

    private String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        startManagingCursor(cursor);
        int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(columnIndex);
    }

    private String getImageNameToUri(Uri data) {
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = managedQuery(data, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

        cursor.moveToFirst();

        String imgPath = cursor.getString(column_index);
        String imgName = imgPath.substring(imgPath.lastIndexOf("/") + 1);

        return imgName;
    }
}
