package kr.edcan.lumihana.safegangwon.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import kr.edcan.lumihana.safegangwon.Activity.ImageActivity;
import kr.edcan.lumihana.safegangwon.Model.PostModel;
import kr.edcan.lumihana.safegangwon.R;

/**
 * Created by kimok_000 on 2016-09-26.
 */
public class PostAdapter extends RecyclerView.Adapter<PostAdapter.ViewHolder> {
    private Context context;
    private ArrayList<PostModel> arrayList;

    public PostAdapter(Context context, ArrayList<PostModel> arrayList) {
        this.context = context;
        this.arrayList = arrayList;

    }

    @Override
    public PostAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_post, parent, false);
        view.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PostAdapter.ViewHolder holder, int position) {
        final PostModel model = arrayList.get(position);


        holder.text_title.setText(model.getTitle());
        holder.text_content.setText(model.getContent());
        holder.text_email.setText("작성자 : " + model.getEmail());
        if (safeString(model.getImage()).toString().trim().equals(""))
            holder.text_image.setVisibility(View.GONE);
        else holder.text_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Intent intent = new Intent(context, ImageActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("title", model.getTitle());
                intent.putExtra("image", model.getImage());
                context.startActivity(intent);
            }
        });
    }

    private String safeString(String str){
        if(str == null) return "";
        else return str;
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView text_title, text_content, text_email, text_image;

        public ViewHolder(View itemView) {
            super(itemView);

            text_title = (TextView) itemView.findViewById(R.id.post_text_title);
            text_content = (TextView) itemView.findViewById(R.id.post_text_content);
            text_email = (TextView) itemView.findViewById(R.id.post_text_email);
            text_image = (TextView) itemView.findViewById(R.id.post_text_image);
        }
    }
}
