package kr.edcan.lumihana.safegangwon.Activity;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.github.paolorotolo.appintro.AppIntro2;
import com.github.paolorotolo.appintro.AppIntro2Fragment;

import kr.edcan.lumihana.safegangwon.R;

/**
 * Created by kimok_000 on 2016-09-30.
 */
public class IntroActivity extends AppIntro2 {
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    @Override
    public void init(@Nullable Bundle savedInstanceState) {
        super.init(savedInstanceState);

        sharedPreferences = getSharedPreferences("Setting", MODE_PRIVATE);
        editor = sharedPreferences.edit();
        if (sharedPreferences.getBoolean("isFirst", false)) {
            final Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            startActivity(intent);
            finish();
        }

        addSlide(AppIntro2Fragment.newInstance("세이프 강원", "세이프 강원에 오신 것을 환영합니다.", R.drawable.logo, getResources().getColor(R.color.colorPrimary)));
        addSlide(AppIntro2Fragment.newInstance("라이브 위젯", "메인 화면의 라이브 위젯 기능은 설정 지역의 실시간 재난 정보와 대피/병원 시설 정보를 한 눈에 볼 수 있게 해줍니다.", R.drawable.help_live, getResources().getColor(R.color.colorPrimary)));
        addSlide(AppIntro2Fragment.newInstance("리스트", "리스트 기능은 정보를 검색하고 상세 정보를 살펴볼 수 있습니다.", R.drawable.help_list, getResources().getColor(R.color.colorPrimary)));
        addSlide(AppIntro2Fragment.newInstance("상세 보기", "상세 보기 기능은 리스트에서 선택한 정보에 대한 상세 정보를 표시합니다.", R.drawable.help_more, getResources().getColor(R.color.colorPrimary)));
        addSlide(AppIntro2Fragment.newInstance("오토 싱크", "오토 싱크 기능은 네트워크가 끊긴 상태에서도 재난 정보를 볼 수 있도록 데이터를 동기화 합니다.", R.drawable.help_sync, getResources().getColor(R.color.colorPrimary)));
        addSlide(AppIntro2Fragment.newInstance("게시판", "게시판에 재난 정보를 사용자들과 공유할 수 있습니다.", R.drawable.help_gesi, getResources().getColor(R.color.colorPrimary)));
        addSlide(AppIntro2Fragment.newInstance("권한 설정 안내", "이 앱을 이용하려면 다음 권한을 허용해야 합니다.(6.0버젼 이상)", R.drawable.ic_https_white_48px, getResources().getColor(R.color.colorPrimary)));
        addSlide(new SettingFragment());

        showSkipButton(false);
        showDoneButton(true);
        setProgressButtonEnabled(true);

        askForPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, 7);
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);

        Toast.makeText(getApplicationContext(), "데이터 로딩중입니다. 잠시후 아래로 밀어 새로고침 해보세요.", Toast.LENGTH_SHORT).show();
        final Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();
        editor.putBoolean("isFirst", true);
        editor.commit();
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
    }
}